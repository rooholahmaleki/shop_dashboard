from cerberus import Validator
import re


class MyValidator(Validator):
    def _validate_type_strdate(self, value):
        """ Enables validation for `strdate` schema attribute.
        :param value: field value.
        """
        check_pattern = re.match("(\d{4})[/.-](\d{2})[/.-](\d{2})[/.T](\d{2})[/.:](\d{2})[/.:](\d{2})[/.](\d{2})$", value)
        if not check_pattern:
            self._error("date", "last update error")
        return True

    def _validate_type_email(self, value):
        """ Enables validation for `email` schema attribute.
        :param value: field value.
        """
        check_pattern = re.match("[^@]+@[^@]+\.[^@]+", value)
        if not check_pattern:
            self._error("email", "email error")
        return True
