from django.conf.urls import url
from rest_framework_jwt.views import refresh_jwt_token

from rest_api.views import (
    user_login,
    confirm_code,
    categories,
    get_all_product,
    get_similar_product,
    edit_profile,
    payment_method,
    create_invoice,
    get_invoices,
    get_invoice,
    canceled_invoice,
    search_product,
    get_product,
    specific_products,
    get_banners,
    about_us,
)

urlpatterns = [
    url(r'^user_login/$', user_login, name='user_login'),
    url(r'^confirm_code/$', confirm_code, name='confirm_code'),
    url(r'^token_refresh/', refresh_jwt_token, name="token_refresh"),
    url(r'^edit_profile/', edit_profile, name="edit_profile"),

    url(r'^categories/$', categories, name='categories'),
    url(r'^get_all_product/$', get_all_product, name='get_all_product'),
    url(r'^search_product/$', search_product, name='search_product'),
    url(r'^get_product/(?P<product_id>\d+)$', get_product, name='get_product'),
    url(r'^get_similar_product/(?P<product_id>\d+)$', get_similar_product, name='get_similar_product'),
    url(r'^specific_products/$', specific_products, name='specific_products'),

    url(r'^get_banners/$', get_banners, name='get_banners'),


    url(r'^payment_method/$', payment_method, name='payment_method'),
    url(r'^create_invoice/$', create_invoice, name='create_invoice'),
    url(r'^get_invoices/$', get_invoices, name='get_invoices'),
    url(r'^get_invoice/(?P<invoice_id>[\w\d-]+)$', get_invoice, name='get_invoice'),
    url(r'^canceled_invoice/(?P<invoice_id>[\w\d-]+)$', canceled_invoice, name='canceled_invoice'),

    url(r'^about_us/$', about_us, name='about_us'),

]
