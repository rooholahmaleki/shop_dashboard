def login_schema():
    return {
        'phone_number': {
            'type': 'string',
            'required': True,
            'maxlength': 11,
            'minlength': 11,
            'empty': False
        },
        'device_id': {
            'type': 'string',
            'required': True,
            'empty': False
        }
    }


def confirm_schema():
    return {
        'phone_number': {
            'type': 'string',
            'required': True,
            'maxlength': 11,
            'minlength': 11,
            'empty': False
        },
        'device_id': {
            'type': 'string',
            'required': True,
            'empty': False
        },
        'code': {
            'type': 'string',
            'required': True,
            'empty': False
        }
    }


def general_schema():
    return {
        'phone_number': {
            'type': 'string',
            'required': True,
            'maxlength': 11,
            'minlength': 11,
            'empty': False
        },
        'device_id': {
            'type': 'string',
            'required': True,
            'empty': False
        },
        'last_update': {
            'type': 'strdate',
            'required': True,
            'empty': False
        }
    }


def similar_product_schema():
    return {
        'phone_number': {
            'type': 'string',
            'required': True,
            'maxlength': 11,
            'minlength': 11,
            'empty': False
        },
        'device_id': {
            'type': 'string',
            'required': True,
            'empty': False
        },
        'product_id': {
            'type': 'string',
            'required': True,
            'empty': False
        }
    }


def edit_profile_schema():
    return {
        'first_name': {
            'type': 'string',
            'required': True,
            'empty': False
        },
        'last_name': {
            'type': 'string',
            'required': True,
            'empty': False
        },
        'latitude': {
            'type': 'string',
            'required': False,
            'empty': True
        },
        'longitude': {
            'type': 'string',
            'required': False,
            'empty': True
        },
        'address': {
            'type': 'string',
            'required': True,
            'empty': False
        },
        'email': {
            'type': 'email',
            'required': False,
            'empty': True
        },
        'device_id': {
            'type': 'string',
            'required': False,
            'empty': True
        }
    }


def create_invoice_schema():
    return {
        'device_id': {
            'type': 'string',
            'required': False,
            'empty': True
        },
        'first_name': {
            'type': 'string',
            'required': True,
            'empty': False
        },
        'last_name': {
            'type': 'string',
            'required': True,
            'empty': False
        },
        'latitude': {
            'type': 'string',
            'required': False,
            'empty': True
        },
        'longitude': {
            'type': 'string',
            'required': False,
            'empty': True
        },
        'address': {
            'type': 'string',
            'required': True,
            'empty': False
        },
        'description': {
            'type': 'string',
            'required': True,
            'empty': True
        },
        'phone_number': {
            'type': 'string',
            'required': True,
            'empty': False
        },
        'payment_method': {
            'type': 'string',
            'allowed': ["1", "2"],
            'required': True,
            'empty': False
        },
        'products': {
            'type': 'string',
            'required': True,
            'empty': False
            # 'minlength': 1,
            # 'schema': {
            #     'type': 'dict',
            #     'required': True,
            #     'schema': {
            #         'id': {
            #             'type': 'string',
            #             'empty': False,
            #             'required': True
            #         },
            #         'amount': {
            #             'type': 'integer',
            #             'required': True,
            #             'empty': False
            #         }
            #     }
            # }
        }
    }
