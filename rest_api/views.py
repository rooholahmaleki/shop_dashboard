# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals

import re
import json

import jdatetime
from django.db import transaction
from django.shortcuts import render
from pymongo import ReturnDocument, DESCENDING, ASCENDING

from core.db_mongo import cursor
from bson.json_util import loads
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes, authentication_classes, renderer_classes
from cerberus import Validator

from factor.static import FACTOR_STATE_MAP
from product.statics import UNIT_TYPE_DICT
from rest_api import schema
from django.conf import settings
from rest_api import message
from django.contrib.auth.models import User as DjangoUser
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from cerberus_custom import MyValidator
from product.controller import CategoryController
from rest_framework_jwt.views import api_settings

import datetime
import requests
import random

from shop_dashboard.settings import logger_api


@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
@transaction.atomic()
def user_login(request):
    _schema = schema.login_schema()
    v = Validator(_schema)
    data = request.data
    if v.validate(data):
        user = cursor.users.find_one({
            "user_type": "USER",
            "phone_number": data["phone_number"]
        })
        date_time_now = datetime.datetime.now()
        code = random.randint(10000, 99999)
        if user:
            update = {
                'modified_date': date_time_now
            }

            modified_date = user.get('modified_date_code')
            if not modified_date:
                update["modified_date_code"] = date_time_now
                modified_date = date_time_now

            different_date = date_time_now - modified_date
            minutes = divmod(different_date.days * 86400 + different_date.seconds, 60)[0]
            if minutes < 120:
                code = user.get('code', code)

            else:
                update["modified_date_code"] = date_time_now

            try:
                user = DjangoUser.objects.get(username=data["phone_number"])
            except:
                user = DjangoUser(username=data["phone_number"], password=code)
                user.save()

            jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
            jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)

            update["code"] = code

            update_or_insert = cursor.users.update_one(
                {"phone_number": data["phone_number"]},
                {
                    "$set": update
                },
            ).modified_count
            _message = "OldUser"

        else:
            user = DjangoUser(username=data["phone_number"], password=code)
            user.save()

            jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
            jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)

            update_or_insert = cursor.users.insert_one({
                "phone_number": data["phone_number"],
                "device_id": data["device_id"],
                "user_type": "USER",
                "code": code,
                "modified_date": date_time_now,
                "modified_date_code": date_time_now,
                "created_date": date_time_now,
            })

            _message = "NewUser"

        if update_or_insert:
            _status = generate_code_for_login(data["phone_number"], code)
            if _status:
                response = {
                    "data": {
                        "code": code,
                        "token": token,
                        "sms_numbers": settings.LINE_NUMBERS
                    },
                    "message": _message,
                    "status": status.HTTP_200_OK
                }
                return Response(response, status=status.HTTP_200_OK)
            return Response(message.SMS_PANEL_NOT_WORKING, status=status.HTTP_204_NO_CONTENT)
        return Response(message.DATABASE_ERROR, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return Response(data=v.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@transaction.atomic()
def confirm_code(request):
    _schema = schema.confirm_schema()
    v = Validator(_schema)
    data = request.data
    if v.validate(data):
        user = cursor.users.find_one({
            "user_type": "USER",
            "phone_number": data["phone_number"],
        })
        if user and str(user.get("code", "")) == data["code"]:
            response = {
                "data": {
                    "register_status": 1,
                    "address": user.get("address", ""),
                    "first_name": user.get("first_name", ""),
                    "last_name": user.get("last_name", ""),
                    "phone_number": data["phone_number"],
                    "latitude": user.get("latitude", ""),
                    "longitude": user.get("longitude", ""),
                },
                "message": "confirmed",
                "status": status.HTTP_200_OK
            }
            return Response(response, status=status.HTTP_200_OK)
        return Response(message.INVALID_CODE, status=status.HTTP_400_BAD_REQUEST)
    return Response(data=v.errors, status=status.HTTP_400_BAD_REQUEST)


def get_token_sms_panel():
    token = requests.post(
        settings.SMS_TOKEN_URL,
        json=settings.SMS_SECRET
    )
    content = loads(token.content)
    if content.get("IsSuccessful", False):
        data = {
            "created_date": datetime.datetime.now(),
            "token_key": content["TokenKey"]
        }
        cursor.tokens.insert_one(data)
        return data
    else:
        return False


def send_sms(phone, text_message):
    _time = datetime.datetime.now() - datetime.timedelta(minutes=30)
    token = cursor.tokens.find_one({"created_date": {"$gte": _time}})
    if not token:
        token = get_token_sms_panel()

    data = {
        "Messages": [text_message],
        "MobileNumbers": [phone],
        "LineNumber": settings.LINE_NUMBERS[0],
        "SendDateTime": "",
        "CanContinueInCaseOfError": "false",
    }

    message_response = loads(requests.post(
        settings.SMS_MESSAGED_URL,
        headers={"x-sms-ir-secure-token": token.get("token_key", "")},
        json=data,
    ).content)
    if message_response.get("IsSuccessful", False):
        return True
    return False


def check_phone_number(phone_number):
    if len(phone_number) == 11:
        return True
    return False


def generate_code_for_login(phone_number, code):
    try:
        text_message = message.SMS_MESSAGE.format(code)
        _status = send_sms(phone_number, text_message)
        if _status:
            return True
        return False
    except:
        return False


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def categories(request):
    data = request.data
    last_update = datetime.datetime.strptime(data["last_update"], str("%Y-%m-%dT%H:%M:%S.%f"))
    _categories = list(cursor.category_final.find({
        "created_date": {"$gte": last_update},
    }).sort("created_date", 1))
    data = []
    if _categories:
        data = _categories[-1]["data"]
    response = {
        "data": data,
        "message": "success",
        "status": status.HTTP_200_OK
    }
    return Response(response, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def search_product(request):
    # logger_api.info("********************************" + str(request.data))
    conditions = request.data
    sort_trans = {
        'most_viewed': ('sell_count', DESCENDING),
        'low_price': ('customer_price', ASCENDING),
        'high_price': ('customer_price', DESCENDING),
        'favorite': ('rate', DESCENDING),
        'newest': ('created_date', DESCENDING),
    }

    criteria = {"trash": {"$ne": True}, "published": True}

    if conditions.get('categories', None):
        _categories = json.loads(conditions["categories"])
        if _categories:
            criteria['categories'] = {"$in": map(int, _categories)}

    if conditions.get('range_price', None):
        range_price = json.loads(conditions["range_price"])
        if range_price:
            criteria['customer_price'] = {
                "$gte": range_price[0],
                "$lte": range_price[1],
            }

    # if conditions.get('brands', None):
    #     criteria['brand'] = {"$in": map(int, conditions['brand'])}

    if int(conditions.get('exist', 0)) == 1:
        criteria['exist'] = int(conditions.get('exist', 1))

    words = conditions.get('words')
    if words:
        words = json.loads(words)
        if words:
            criteria["$or"] = [
                {"product_name_fa": {"$regex": words[0]}},
                {"product_name_en": {"$regex": words[0], "$options": 'i'}}
            ]

    sort_key = sort_trans[conditions.get('order', 'newest')]
    page = int(conditions.get('page', 1))
    pagesize = int(conditions.get('limit', 16))

    # logger_api.info("++++++++++++++++++++++++++" + str(criteria))
    result = change_structure_product(list(
        cursor.product.find(criteria).sort([(sort_key[0], sort_key[1])]).skip(pagesize * (page - 1)).limit(pagesize)
    ))

    response = {
        "data": result,
        "message": "success",
        "status": status.HTTP_200_OK
    }
    return Response(response, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def get_all_product(request):
    data = request.data
    last_update = datetime.datetime.strptime(data["last_update"], str("%Y-%m-%dT%H:%M:%S.%f"))
    _products = list(cursor.product.find({
        "created_date": {"$gte": last_update}
    }).sort("created_date", 1))
    data = []
    if _products:
        data = change_structure_product(_products)
    response = {
        "data": data,
        "message": "success",
        "status": status.HTTP_200_OK
    }

    return Response(response, status=status.HTTP_200_OK)


def change_structure_product(products):
    _list_product = []
    _dict_categories = {}
    _list_category = []
    for product in products:
        _images = []
        _list_category.append(product["categories"][-1])
        for images in product.get("galleries", []):
            for small in images["list_image"]:
                _images.append(small["small"])
        product["galleries"] = _images

    _list_category = CategoryController().get_by_list_id(_list_category)
    for cat in _list_category:
        _dict_categories[cat["id"]] = cat["name"]

    for product in products:
        cat = product["categories"][-1]
        _dict_product = {
            "id": product["product_id"],
            "category_id": cat,
            "category_name": _dict_categories.get(cat, "none"),
            "name_fa": product["product_name_fa"],
            "name_en": product["product_name_en"],
            "short_description": product["product_name_fa"] + " - " + product["product_name_en"],
            "full_description": product.get("general_specifications", ""),
            "picture_urls": product["galleries"],
            "image": product.get("image", ""),
            "exist": True if product["exist"] == 1 else False,
            "price": product.get("customer_price", 0),
            "old_price": product["price"][1] if len(product["price"]) >= 2 else None,
            "published": product["published"],
            "last_update": product["modified_date"],
            "trash": product["trash"],
            "category_published": product["category_published"],
            "unit": product.get('unit', 1),
            "display_capacity": UNIT_TYPE_DICT[int(product.get('unit_type', 1))]
        }
        _list_product.append(_dict_product)
    return _list_product


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_similar_product(request, product_id):
    criteria = {"product_id": product_id}
    _product = cursor.product.find_one(criteria)
    data = []
    if _product:
        data = get_similar_list(_product, 4)

    response = {
        "data": data,
        "message": "success",
        "status": status.HTTP_200_OK
    }
    return Response(data=response, status=status.HTTP_200_OK)


def get_similar_list(product, limit):
    criteria = {
        'product_id': {'$ne': product['product_id']},
        'categories': {'$in': [product['categories'][1]]},
        'customer_price': {
            '$gt': product['customer_price'] * (1 - settings.SIMILAR_THRESHOLD),
            '$lt': product['customer_price'] * (1 + settings.SIMILAR_THRESHOLD)
        },
    }
    criteria.update(settings.PRODUCT_GENERAL_CRITERIA)
    _products = list(cursor.product.find(criteria).limit(limit))
    return change_structure_product(_products)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_product(request, product_id):
    criteria = {"product_id": product_id}
    product = cursor.product.find_one_and_update(
        criteria,
        {'$addToSet': {'most_viewed': request.user.id}},
        return_document=ReturnDocument.AFTER
    )
    data = {}
    if product:
        _images = []
        for images in product.get("galleries", []):
            for small in images["list_image"]:
                _images.append(small["small"])
        product["galleries"] = _images

        cat = CategoryController().get_by_id(product["categories"][-1])

        data = {
            "id": product["product_id"],
            "category_id": cat["id"],
            "category_name": cat["name"],
            "name_fa": product["product_name_fa"],
            "name_en": product["product_name_en"],
            "short_description": product["product_name_fa"] + " - " + product["product_name_en"],
            "full_description": product.get("general_specifications", ""),
            "picture_urls": product["galleries"],
            "image": product.get("image", ""),
            "exist": True if product["exist"] == 1 else False,
            "price": product.get("customer_price", 0),
            "old_price": product["price"][1] if len(product["price"]) >= 2 else None,
            "published": product["published"],
            "last_update": product["modified_date"],
            "trash": product["trash"],
            "category_published": product["category_published"],
            "unit": product.get('unit', 1),
            "display_capacity": UNIT_TYPE_DICT[int(product.get('unit_type', 1))]
        }
        response = {
            "data": data,
            "message": "success",
            "status": status.HTTP_200_OK
        }
        return Response(data=response, status=status.HTTP_200_OK)

    return Response(data=data, status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def edit_profile(request):
    _schema = schema.edit_profile_schema()
    v = MyValidator(_schema)
    data = request.data
    if v.validate(data):
        update = {
            'first_name': data["first_name"],
            'last_name': data["last_name"],
            'address': data["address"],
        }
        if 'email' in data:
            update["email"] = data["email"]

        if 'latitude' in data:
            update['latitude'] = data['latitude']

        if 'longitude' in data:
            update['longitude'] = data['longitude']

        user = cursor.users.find_one_and_update(
            {"user_type": "USER", "phone_number": request.user.username},
            {"$set": update},
            return_document=ReturnDocument.AFTER
        )
        response = {
            "data": {
                "register_status": 1,
                "address": user.get("address", ""),
                "first_name": user.get("first_name", ""),
                "last_name": user.get("last_name", ""),
                "phone_number": request.user.username,
                "latitude": user.get("latitude", ""),
                "longitude": user.get("longitude", ""),
            },
            "message": "user update",
            "status": status.HTTP_200_OK
        }
        return Response(data=response, status=status.HTTP_200_OK)
    return Response(data=v.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def payment_method(request):
    data = [
        {
            "id": 1,
            "title": "پرداخت در محل",
            "description": "پرداخت در هنگام تحویل کالا صورت می گیرد.",
            "active": True,
        },
        {
            "id": 2,
            "title": "پرداخت آنلاین",
            "description": "پرداخت به صورت آنلاین انجام می گیرد.",
            "active": False,
        },
    ]
    response = {
        "data": data,
        "message": "success",
        "status": status.HTTP_200_OK
    }
    return Response(data=response, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def create_invoice(request):
    _schema = schema.create_invoice_schema()
    v = MyValidator(_schema)
    data = request.data
    if v.validate(data):
        products = json.loads(data.get("products", '[]'))
        list_id = [product["id"] for product in products]

        db_products = cursor.product.find({
            "product_id": {"$in": list_id}
        })
        if db_products.count():
            list_product = []
            total_price = 0
            for db_product in db_products:
                for product in products:
                    if product["id"] == db_product["product_id"]:
                        try:
                            price = db_product["price"][0]["customer_price"]
                        except:
                            price = 0

                        amount = product["amount"]
                        _dict = {
                            "product_name_fa": db_product["product_name_fa"],
                            "product_name_en": db_product["product_name_en"],
                            "product_id": db_product["product_id"],
                            "unit": db_product["unit"],
                            "unit_type": UNIT_TYPE_DICT[int(db_product.get('unit_type', 1))],
                            "amount": amount,
                            "price": price,
                            "total_price": price * amount,
                        }
                        list_product.append(_dict)
                        total_price += (price * amount)

            now = datetime.datetime.now()
            jalali_year, jalali_month, jalali_day = jdatetime.GregorianToJalali(
                now.year, now.month, now.day
            ).getJalaliList()

            invoice = {
                "invoice_id": generate_order_id(),
                "user_id": request.user.username,
                "products": list_product,
                "created_date": now,

                "jalali_day": jalali_day,
                "jalali_year": jalali_year,
                "jalali_month": jalali_month,

                "state_text": FACTOR_STATE_MAP["pending"],
                "total_price": total_price,
                "state": "pending",

                'device_id': data.get('device_id'),
                'first_name': data.get('first_name'),
                'last_name': data.get('last_name'),
                'latitude': data.get('latitude'),
                'longitude': data.get('longitude'),
                'address': data.get('address'),
                'description': data.get('description'),
                'phone_number': data.get('phone_number'),
                'payment_method': data.get('payment_method'),
            }

            invoice_id = cursor.invoice.insert_one(invoice)
            if invoice_id:
                invoice.pop("_id")
                response = {
                    "data": invoice,
                    "message": "insert invoice",
                    "status": status.HTTP_200_OK
                }
                return Response(data=response, status=status.HTTP_200_OK)
            return Response(message.DATABASE_ERROR, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response("product ids not valid", status=status.HTTP_400_BAD_REQUEST)
    return Response(data=v.errors, status=status.HTTP_400_BAD_REQUEST)


def get_epoch(dt):
    return (dt - datetime.datetime(1970, 1, 1)).total_seconds()


def generate_order_id():
    now = datetime.datetime.now()
    generated_code = str(int((
                                 get_epoch(now) -
                                 get_epoch(datetime.datetime(now.year, now.month, 1))
                             ) * 37)).zfill(8)

    jalali_year, jalali_month, jalali_day = jdatetime.GregorianToJalali(now.year, now.month, now.day).getJalaliList()
    order_id = 'PSN{}{:02d}{:02d}-{}'.format(jalali_year, jalali_month, jalali_day, generated_code)

    return order_id


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_invoices(request):
    invoices = list(cursor.invoice.find({"user_id": request.user.username}, {"_id": 0}))

    response = {
        "data": invoices,
        "message": "success",
        "status": status.HTTP_200_OK
    }
    return Response(data=response, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_invoice(request, invoice_id):
    invoice = cursor.invoice.find_one(
        {"user_id": request.user.username, "invoice_id": invoice_id},
        {"_id": 0}
    )
    if invoice:
        response = {
            "data": invoice,
            "message": "success",
            "status": status.HTTP_200_OK
        }
        return Response(data=response, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def canceled_invoice(request, invoice_id):
    invoice = cursor.invoice.find_one_and_update(
        {"user_id": request.user.username, "invoice_id": invoice_id},
        {"$set": {"state": "canceled"}},
        return_document=ReturnDocument.AFTER,
    )
    if invoice:
        invoice.pop("_id")
        response = {
            "data": invoice,
            "message": "success",
            "status": status.HTTP_200_OK,
        }
        return Response(data=response, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_banners(request):
    banners = cursor.banner.find()
    list_banners = []
    index = 1
    for banner in banners:
        list_banners.append({
            "id": 1,
            "title": banner["name"],
            "product_id": banner["link"],
            # "description": "پیشنهادات ویژه تابستانه",
            # "active": True,
            "display_order": index,
            # "link": "http://sabzaloo.com/",
            "pic": banner["image"],
        })
        index += 1

    response = {
        "data": list_banners,
        "message": "success",
        "status": status.HTTP_200_OK
    }
    return Response(data=response, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def specific_products(request):
    criteria = {"published": True, "trash": False}
    list_column_specific_products = [
        {
            "title": u"جدیدترین ها",
            "query_type": "newest",
            "products": change_structure_product(
                list(cursor.product.find(criteria).sort("created_date", 1).limit(10))
            )
        },
        {
            "title": u"پربازدیدترین ها",
            "query_type": "most_viewed",
            "products": change_structure_product(
                list(cursor.product.aggregate([
                    {"$unwind": "$most_viewed"},
                    {"$sort": {"size": 1}},
                    {"$limit": 10}]))
            )
        },
        {
            "title": u"پرفروش ترین ها",
            "query_type": "best_sellers",
            "products": change_structure_product(
                list(cursor.product.find(criteria).sort("created_date", 1).limit(10))
            )
        }
    ]
    response = {
        "data": list_column_specific_products,
        "message": "success",
        "status": status.HTTP_200_OK
    }
    return Response(data=response, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes(())
def about_us(request):
    return render(request, 'about_us.html', {})
