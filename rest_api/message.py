# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import status

DATABASE_ERROR = {
    "data": {},
    "message": "data base error",
    "status": status.HTTP_500_INTERNAL_SERVER_ERROR
}

INVALID_DATA = {
    "data": {},
    "message": "invalid data",
    "status": status.HTTP_400_BAD_REQUEST
}

SMS_PANEL_NOT_WORKING = {
    "data": {},
    "message": "sms panel not working",
    "status": status.HTTP_500_INTERNAL_SERVER_ERROR
}

USER_NOT_FOUND = {
    "data": {},
    "message": "user not found",
    "status": status.HTTP_404_NOT_FOUND
}

INVALID_CODE = {
    "message": "invalid code",
    "status": status.HTTP_400_BAD_REQUEST
}

SMS_MESSAGE = "{0} \n کد اختصاصی شما جهت ورود به سامانه پاندا"
