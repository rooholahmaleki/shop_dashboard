# coding: utf-8
import datetime
import calendar

import jdatetime


class Jalali(object):
    date_patterns = (
        "%Y-%m-%dT%H:%M:%S.%f",
        "%Y-%m-%dT%H:%M:%S.%fZ",
    )

    jalali_month = {
        1: 'فروردین',
        2: 'اردیبهشت',
        3: 'خرداد',
        4: 'تیر',
        5: 'مرداد',
        6: 'شهریور',
        7: 'مهر',
        8: 'آبان',
        9: 'آذر',
        10: 'دی',
        11: 'بهمن',
        12: 'اسفند'
    }

    def __init__(self, datetime_value):
        if isinstance(datetime_value, str):
            for pattern in self.date_patterns:
                try:
                    datetime_value = datetime.datetime.strptime(datetime_value, pattern)
                except ValueError:
                    pass

            if isinstance(datetime_value, str):
                raise ValueError(
                    'Couldn\'t match date string {} with specified patterns'.format(datetime_value)
                )

        self.datetime_obj = datetime_value
        self.hour = datetime_value.hour
        self.minute = datetime_value.minute
        self.second = datetime_value.second

        self.g_year = datetime_value.year
        self.g_month = datetime_value.month
        self.g_day = datetime_value.day

        jalali = jdatetime.GregorianToJalali(self.g_year, self.g_month, self.g_day)
        self.j_year, self.j_month, self.j_day = jalali.getJalaliList()

        self.j_month_fa = self.jalali_month[self.j_month]

    @property
    def complete(self):
        return '{} {} {}، ساعت {}:{}'.format(
            self.j_day, self.j_month_fa, self.j_day, self.minute, self.hour
        )

    @property
    def raw(self):
        return {
            'year': self.j_year, 'month': self.j_month, 'day': self.j_day,
            'hour': self.hour, 'minute': self.minute, 'second': self.second,
        }

    @property
    def date_with_space(self):
        return '{} {} {}'.format(self.j_day, self.j_month_fa, self.j_year)

    @property
    def date_with_splash(self):
        return '{}/{}/{}'.format(self.j_day, self.j_month_fa, self.j_year)

    def translate_ago_date(self):
        now = datetime.datetime.now()
        diff = (now - self.datetime_obj)
        sec = int(diff.total_seconds())

        end_month = calendar.monthrange(now.year, now.month)[1]

        if sec <= 59:
            result = int(sec), 'seconds'

        elif 60 <= sec <= 3599:
            result = int(sec / 60), 'minutes'

        elif 3600 <= sec <= 86399:
            result = int(sec / 3600), 'hours'

        elif sec >= 86400:
            if diff.days <= 6:
                result = diff.days, 'days'

            elif 7 <= diff.days <= end_month:
                result = (diff.days / 7), 'weeks'

            elif (end_month + 1) <= diff.days <= 365:
                result = (diff.days / end_month), 'months'

            elif diff.days >= 366:
                result = (diff.days / 365), 'years'

        return result

    @property
    def ago(self):
        _tuple = self.translate_ago_date()
        translate = {
            'seconds': 'ثانیه',
            'minutes': 'دقیقه',
            'hours': 'ساعت',
            'weeks': 'هفته',
            'days': 'روز',
            'months': 'ماه',
            'years': 'سال'
        }

        text = "{0} {1} پیش".format(_tuple[0], translate[_tuple[1]])

        return text
