import zlib
from bson.objectid import ObjectId
from core.db_mongo import cursor


def get_user_data(id_user):
    _id = {'_id': ObjectId(id_user)}
    user = cursor.users.find_one(_id)
    user['id'] = str(user.pop('_id'))
    return user


def deflate(data, compresslevel=9):
    compress = zlib.compressobj(
        compresslevel,
        zlib.DEFLATED,
        -zlib.MAX_WBITS,
        zlib.DEF_MEM_LEVEL,
        0
    )
    deflated = compress.compress(data)
    deflated += compress.flush()
    return deflated


def inflate(data):
    decompress = zlib.decompressobj(-zlib.MAX_WBITS)
    inflated = decompress.decompress(data)
    inflated += decompress.flush()
    return inflated
