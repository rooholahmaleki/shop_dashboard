# coding: utf-8

import random
import datetime
import re
import string
import jdatetime
from jdatetime import JalaliToGregorian


month = {
    1: 'فروردین',
    2: 'اردیبهشت',
    3: 'خرداد',
    4: 'تیر',
    5: 'مرداد',
    6: 'شهریور',
    7: 'مهر',
    8: 'آبان',
    9: 'آذر',
    10: 'دی',
    11: 'بهمن',
    12: 'اسفند'
}


'''
def toG(array):
    date_type = datetime.datetime(*array)
    return date_type


def jToArray(*date):
    gregorian   = JalaliToGregorian(date[0], date[1], date[2])
    array       = gregorian.getGregorianList()
    return array
'''


def jToArray(_date):
    gregorian = JalaliToGregorian(_date.year, _date.month, _date.day)
    array = gregorian.getGregorianList()
    return array


def jalali_to_gregorian(year, month, day):
    gregorian = JalaliToGregorian(year, month, day)
    gyear, gmonth, gday = gregorian.getGregorianList()
    return gyear, gmonth, gday


def rand_x_digits(x, leading_zeroes=True):
    '''
        Return an X digit number, leading_zeroes returns a string,
        otherwise int
    '''
    if not leading_zeroes:
        # wrap with str() for uniform results
        return random.randint(10 ** (x - 1), 10 ** x - 1)
    else:
        if x > 6000:
            return ''.join([str(random.randint(0, 9)) for i in xrange(x)])
        else:
            return '{0:0{x}d}'.format(random.randint(0, 10 ** x - 1), x=x)


def gregorian_to_jalali(value):
    '''
        Convert Gregorian date to Jalali date into Core
    '''
    if isinstance(value, datetime.datetime):
        value = value.isoformat()

    pattern = re.compile('^(\d+-\d+-\d+)[T\s](\d+:\d+:\d+\.?\d*)')
    ins = re.search(pattern, value).groups()
    split_str = ins[0].split('-')
    split = map(int, split_str)
    date = jdatetime.GregorianToJalali(split[0], split[1], split[2])
    jdate = date.getJalaliList()
    return '%s %s %s در ساعت %s' % (jdate[2], month[jdate[1]], jdate[0], str(ins[1][:8]))


def randomword(length):
   return ''.join(random.choice(string.lowercase) for i in range(length))