import logging
import bcrypt

from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist

from users.models import UserBox
from core.db_mongo import cursor

logger = logging.getLogger(__name__)


class ShopBackend(object):
    def authenticate(self, username, password):
        user_find = cursor.users.find_one({
            'username': username,
            'user_type': 'OPERATOR'
        })

        if user_find:
            result_pass = user_find['password'].encode('utf-8')
            if result_pass != bcrypt.hashpw(password, result_pass):
                return None
            try:
                user = User.objects.get(username=username)
            except ObjectDoesNotExist:
                user = User(username=username, password=password)
                user.email = username
                # user.first_name = user_find['personal']['first_name'].encode('utf-8')
                # user.last_name = user_find['personal']['last_name'].encode('utf-8')
                user.save()

                UserBox.objects.create(
                    user=user,
                    group_name=user_find['group_name'],
                    mongo_id=str(user_find['_id'])
                )

            return user
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
