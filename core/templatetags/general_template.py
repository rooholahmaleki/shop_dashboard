#! -*- coding: utf-8 -*-
from __future__ import unicode_literals

import re

from django import template
from bson import ObjectId
from core.db_mongo import cursor, cursor
from core.tools import gregorian_to_jalali, month

import jdatetime

register = template.Library()


@register.filter
def has_perm(user, perm):
    if not hasattr(user, 'userBox'):
        return False
    else:
        return cursor.acl_group.find_one(
            {'group_name': user.userBox.group_name, 'visible_templatetags': perm},
            {'visible_templatetags': 1, '_id': 0}
        )


@register.filter(name='reverse_ranger')
def reverse_ranger(number, _from=1):
    return range(_from, number, -1)


@register.filter(name='ranger')
def ranger(number, _from=1):
    return range(_from, number)


@register.filter(name='jmonth')
def jmonth(value):
    return month[int(value)]


@register.filter(name='jalali')
def jalali(value):
    return gregorian_to_jalali(value)


@register.filter(name='tdjalali')
def tdjalali(value):
    """
        Convert Gregorian date to Jalali date into Template
    """
    pattern = re.compile(r'^(\d+-\d+-\d+)')
    ins = re.search(pattern, value).groups()
    split_str = ins[0].split('-')
    split = map(int, split_str)
    date = jdatetime.GregorianToJalali(split[0], split[1], split[2])
    jdate = date.getJalaliList()
    return 'سال: {0}  -  ماه: {1}  -  روز: {2}'.format(jdate[0], month[jdate[1]], jdate[2])


@register.filter(name='djalali')
def djalali(value):
    """
        Convert Gregorian date to Jalali date into Template
    """
    pattern = re.compile(r'^(\d+-\d+-\d+)')
    ins = re.search(pattern, value).groups()
    split_str = ins[0].split('-')
    split = map(int, split_str)
    date = jdatetime.GregorianToJalali(split[0], split[1], split[2])
    return '{0}/{1}/{2}'.format(date.jyear, date.jmonth, date.jday)


@register.filter(name='tjalali')
def tjalali(value):
    """
        Convert Gregorian date to Jalali date into Template
    """
    pattern = re.compile(r'^(\d+-\d+-\d+)T(\d+:\d+:\d+[\.?\d*|])')
    ins = re.search(pattern, value).groups()
    split_str = ins[0].split('-')
    split = map(int, split_str)
    date = jdatetime.GregorianToJalali(split[0], split[1], split[2])
    jdate = date.getJalaliList()
    return '%s %s %s ساعت %s' % (jdate[2], month[jdate[1]], jdate[0], str(ins[1][:8]))


@register.filter(name='check_like')
def check_like(feedbacks, user):
    if hasattr(user, 'userBox') and user.userBox.mongo_id in feedbacks and feedbacks[user.userBox.mongo_id] is True:
        return 'sh-like-voted'
    else:
        return ''


@register.filter(name='check_dislike')
def check_dislike(feedbacks, user):
    if hasattr(user, 'userBox') and user.userBox.mongo_id in feedbacks and feedbacks[user.userBox.mongo_id] is False:
        return 'sh-dislike-voted'
    else:
        return ''


@register.filter(name='feedback_count')
def feedback_count(feedbacks, _type):
    if feedbacks and isinstance(feedbacks, dict):
        return sum([1 if feedback == _type else 0 for feedback in feedbacks.values()])
    else:
        return 0


@register.filter(name='mongo_id')
def mongo_id(value):
    return str(value['_id'])


@register.filter(name='get_color')
def get_color(report):
    days = {
        "1": "#17a589",
        "2": "#138d75",
        "3": "#28b463",
        "4": "#229954",
        "5": "#2e86c1",
        "6": "#2471a3",
        "7": "#04D215",
        "8": "#884ea0",
        "9": "#0D52D1",
        "10": "#7d3c98",
        "11": "#2e4053",
        "12": "#273746",
        "13": "#d4ac0d",
        "14": "#DDDDDD",
        "15": "#d68910",
        "16": "#707b7c",
        "17": "#ca6f1e",
        "18": "#ba4a00",
        "19": "#cb4335",
        "20": "#a93226",
        "21": "#d0d3d4",
        "22": "#a6acaf",
        "23": "#839192",
        "24": "#707b7c",
        "25": "#229954",
        "26": "#000000",
        "27": "#138d75",
        "28": "#d4ac0d",
        "29": "#a93226",
        "30": "#884ea0",
        "31": "#d4ac0d",
    }

    return days.get(str(report["_id"]))
