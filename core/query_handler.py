import logging

logger = logging.getLogger(__name__)


def modify_update_result(data):
    try:
        data['result'] = data['updatedExisting']
    except KeyError:
        pass

    return data
