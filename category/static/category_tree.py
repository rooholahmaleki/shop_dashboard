# coding: utf-8
from core.db_mongo import cursor
from datetime import datetime


def category_tree():
    all_categories = list(cursor.category.find(
        {'published': True, 'ready_to_delete': False},
        {'name': 1, 'ancestors': 1, 'id': 1, '_id': 0, }
    ).sort('id', 1).sort('order', 1))
    for category in all_categories:
        category['id_parent'] = category['ancestors'][-1]

    category_tree_dict = {}
    parents = [cat for cat in all_categories if cat['ancestors'] == [0]]
    for parent in parents:
        childs = [
            cat for cat in all_categories if parent['id'] in cat['ancestors']]
        tree = {}
        level_depth = 0
        childs_dict = {1: [], 2: [], 3: [], 4: []}

        for child in childs:
            level_depth = min(4, max(level_depth, len(child['ancestors'])))
            try:
                childs_dict[len(child['ancestors'])].append(child)
            except KeyError:
                pass  # max level is category>group>sub>sub>sub

        for i in range(level_depth - 1, 0, -1):
            for cat in childs_dict[i]:
                cat_and_ancestors = cat['ancestors'][:]
                cat_and_ancestors.append(cat['id'])
                for sub_cat in childs_dict[i + 1]:
                    if cat_and_ancestors == sub_cat['ancestors']:
                        try:
                            cat['childs'][sub_cat['id']] = sub_cat
                        except KeyError:
                            cat['childs'] = {}
                            cat['childs'][sub_cat['id']] = sub_cat

        for cat in childs_dict[1]:
            tree[cat['id']] = cat
        parent['childs'] = tree
        category_tree_dict[parent['id']] = parent

    return category_tree_dict


def update_final_all_category():
    all_categories = list(cursor.category.find(
        {'published': True, 'ready_to_delete': False},
        {'name': 1, 'ancestors': 1, 'id': 1, '_id': 0, }
    ).sort('id', 1).sort('order', 1))

    date_time = datetime.now()
    conversion_category = {
        "data": [],
        "created_date": date_time
    }
    count = 1
    for category in all_categories:
        order = 0
        if not category['ancestors']:
            order = count
            count += 1

        conversion_category["data"].append({
            "id": category["id"],
            "name": category["name"],
            "description": None,
            "parent_category_id": category['ancestors'][-1],
            "ancestors": category['ancestors'],
            "display_order": order,
            "picture_url": None,
            "picture_icon": None,
            "show_on_home_page": True if category['ancestors'][0] == 0 else False,
            "published": True,
            "updated_on_utc": str(date_time).replace(" ", "T")
        })

    cursor.category_final.insert_one(conversion_category)
