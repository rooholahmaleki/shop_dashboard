# coding: utf-8
"""
Category and Group "id"s will Start from 1, Subgroup "id"s will start from 400.
finally counter fields for auto incrementing  "category_id" and "subgroup_id" will set in SHOP.counters.
"""
from core.db_mongo import cursor

ID = 1
SUB_ID = 400

CAT_LIST = [
    {
        "name": "محصولات دیجیتال",
        "childs": [
            {
                "name": "موبایل",
                "childs": [
                ]
            },
            {
                "name": "تبلت",
                "childs": [
                ]
            },
            {
                "name": "دوربین عکاسی",
                "childs": [
                ]
            },
            {
                "name": "دوربین فیلم برداری",
                "childs": [
                ]
            },
            {
                "name": "لپ تاپ",
                "childs": [
                ]
            },
            {
                "name": "ماشینهای اداری",
                "childs": [
                    {
                        "name": "پرینتر و اسکنر ",
                        "childs": [
                            {
                                "name": "پرینتر لیزری",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پرینتر جوهر افشان ",
                                "childs": [

                                ]
                            },
                            {
                                "name": "چند کاره",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پرینتر عکس",
                                "childs": [

                                ]
                            },
                            {
                                "name": "لوازم جانبی پرینتر",
                                "childs": [

                                ]
                            },
                            {
                                "name": "اسکنر اداری",
                                "childs": [

                                ]
                            },
                            {
                                "name": "اسکنر خانگی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "اسکنر حرفه ای",
                                "childs": [

                                ]
                            },
                            {
                                "name": "اسکنر قابل حمل",
                                "childs": [

                                ]
                            },
                        ]
                    },
                    {
                        "name": "تلفن و فکس ",
                        "childs": [
                            {
                                "name": "تلفن بی سیم",
                                "childs": [

                                ]
                            },
                            {
                                "name": "تلفن با سیم ",
                                "childs": [

                                ]
                            },
                            {
                                "name": "تلفن سانترال",
                                "childs": [

                                ]
                            },
                            {
                                "name": "فکس ",
                                "childs": [

                                ]
                            },
                            {
                                "name": "لوازم جانبی تلفن و فکس",
                                "childs": [

                                ]
                            },
                        ]
                    },
                    {
                        "name": "لوازم اداری ",
                        "childs": [
                            {
                                "name": "دیتا و ویدئو پروژکتور",
                                "childs": [

                                ]
                            },
                            {
                                "name": " دستگاه کپی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "لیزر پوینتر",
                                "childs": [
                                ]
                            },
                            {
                                "name": " کاغذ خرد کن ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "دستگاه حضور غیاب",
                                "childs": [
                                ]
                            },
                            {
                                "name": "پرزنتر",
                                "childs": [
                                ]
                            },
                            {
                                "name": "تخته هوشمند",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": "لوازم فروشگاهی ",
                        "childs": [
                            {
                                "name": "اسکناس شمار",
                                "childs": [

                                ]
                            },
                            {
                                "name": " تشخیص اصالت اسکناس",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پرفراژ چک",
                                "childs": [
                                ]
                            },
                            {
                                "name": " صندوق فروشگاهی ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "بارکد خوان",
                                "childs": [
                                ]
                            },
                            {
                                "name": "پرینتر لیبل زن حرارتی",
                                "childs": [
                                ]
                            },
                        ]
                    },
                ]
            },
            {
                "name": "کنسول بازی",
                "childs": [
                ]
            },
            {
                "name": "کامپیوتر و قطعات جانبی",
                "childs": [
                    {
                        "name": "قطعات کامپیوتر",
                        "childs": [
                            {
                                "name": "مادر برد ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "پردازنده",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کارت گرافیک ",
                                "childs": [

                                ]
                            },
                            {
                                "name": "نمایشگر(مانیتور)",
                                "childs": [

                                ]
                            },
                            {
                                "name": "هاردیسک ",
                                "childs": [

                                ]
                            },
                            {
                                "name": "منبع تغذیه(پاور)",
                                "childs": [

                                ]
                            },
                            {
                                "name": "درایو نوری",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کیس کامپیوتر",
                                "childs": [

                                ]
                            },
                            {
                                "name": "اسپیکر(بلندگو)",
                                "childs": [

                                ]
                            },
                            {
                                "name": "هدفون و هدست . میکروفن",
                                "childs": [
                                ]
                            },
                            {
                                "name": "کیبور و موس",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": "تجهیزات شبکه و ارتباطات ",
                        "childs": [
                            {
                                "name": "مودم-روتر-ADSL",
                                "childs": [

                                ]
                            },
                            {
                                "name": "روتر و اکسس پوینت ",
                                "childs": [

                                ]
                            },
                            {
                                "name": "مودم همراه 3G -4G",
                                "childs": [

                                ]
                            },
                            {
                                "name": "ذخیره ساز تحت شبکه ",
                                "childs": [

                                ]
                            },
                            {
                                "name": "دوربین تحت شبکه",
                                "childs": [

                                ]
                            },
                            {
                                "name": "سووئیچ  ",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کارت شبکه ",
                                "childs": [

                                ]
                            },
                            {
                                "name": "آداپتور ",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پرینت سرور",
                                "childs": [

                                ]
                            },
                            {
                                "name": "آنتن تقویتی",
                                "childs": [

                                ]
                            },
                        ]
                    },
                    {
                        "name": "تجهیزات ذخیره ساز اطلاعات ",
                        "childs": [
                            {
                                "name": "حافظه SSD  اینترنال",
                                "childs": [

                                ]
                            },
                            {
                                "name": " حافظه SSD  اکسترنال",
                                "childs": [

                                ]
                            },
                            {
                                "name": "هاردیسک اکسترنال",
                                "childs": [
                                ]
                            },
                            {
                                "name": " فلش مموری USB ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "کارت حافظه",
                                "childs": [
                                ]
                            },
                            {
                                "name": "لوح فشرده",
                                "childs": [
                                ]
                            },
                            {
                                "name": "لوازم جانبی تجهیزات ذخیره سازی",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": "لوازم جانبی ",
                        "childs": [
                            {
                                "name": "وب کم",
                                "childs": [

                                ]
                            },
                            {
                                "name": " گسرنده تلویزیون",
                                "childs": [

                                ]
                            },
                            {
                                "name": "یو اس پی هاب USB HUB",
                                "childs": [
                                ]
                            },
                            {
                                "name": " قلم نوری ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "چند راهی برق",
                                "childs": [
                                ]
                            },
                        ]
                    },
                ]
            },
            {
                "name": " لوازم جانبی",
                "childs": [
                    {
                        "name": "موبایل",
                        "childs": [
                            {
                                "name": "کارت حاظه میکرو Micro SD",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کیف و کاور گوشی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "محافظ صفحه نمایش",
                                "childs": [

                                ]
                            },
                            {
                                "name": "هندز فری و هدفون",
                                "childs": [

                                ]
                            },
                            {
                                "name": "باتری گوشی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پاور بانک",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کابل گوشی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "منو پاد و پایه نگهدارنده",
                                "childs": [

                                ]
                            },
                            {
                                "name": "قلم لمسی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کیت تمیز کننده",
                                "childs": [

                                ]
                            },
                            {
                                "name": "شارژر موبایل",
                                "childs": [

                                ]
                            },
                            {
                                "name": "سایر لوازم جانبی موبایل",
                                "childs": [

                                ]
                            },
                        ]
                    },
                    {
                        "name": "تبلت",
                        "childs": [
                            {
                                "name": "کیف و کاور و قاب محافظ تبلت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "محافظ صفحه نمایش",
                                "childs": [

                                ]
                            },
                            {
                                "name": "هدفون و هندز فری",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کارت حافظه",
                                "childs": [

                                ]
                            },
                            {
                                "name": "شارژر تبلت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "استند تبلت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "قلم لمسی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کیت تمیز کننده ",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کابل رابط تبلت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کیبور مخصوص تبلت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "سایر لوازم جانبی",
                                "childs": [

                                ]
                            },
                        ]
                    },
                    {
                        "name": "دوربین عکاسی ",
                        "childs": [
                            {
                                "name": "فلاش",
                                "childs": [

                                ]
                            },
                            {
                                "name": "فیلتر لنز ",
                                "childs": [

                                ]
                            },
                            {
                                "name": "لنز دوربین",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کیف دوربین",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کارت حافظه",
                                "childs": [

                                ]
                            },
                            {
                                "name": "سه پایه و تک پایه",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ریموت کنترل دوربی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "محافظ صفحه نمایش",
                                "childs": [
                                ]
                            },
                            {
                                "name": "کیت تمیز کننده",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": " فیلم برداری ",
                        "childs": [
                            {
                                "name": "لنز",
                                "childs": [

                                ]
                            },
                            {
                                "name": "لوازم جانبی لنز ",
                                "childs": [

                                ]
                            },
                            {
                                "name": "لوازم جانبی فیلم برداری",
                                "childs": [

                                ]
                            },
                            {
                                "name": "لوازم جانبی تجهیزات هوایی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "لوازم جانبی فیلم برداری اکسن کمرا",
                                "childs": [

                                ]
                            },
                            {
                                "name": "سه پایه و نگهدارنده",
                                "childs": [
                                ]
                            },
                            {
                                "name": "کیف",
                                "childs": [
                                ]
                            },
                            {
                                "name": "باطری و شارژر",
                                "childs": [
                                ]
                            },
                            {
                                "name": "صفحه نمایش",
                                "childs": [
                                ]
                            },
                            {
                                "name": "رکوردر",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": "لپ تاب ",
                        "childs": [
                            {
                                "name": "کیف و کاور لپ تاپ",
                                "childs": [

                                ]
                            },
                            {
                                "name": "ماس و ماس پد",
                                "childs": [

                                ]
                            },
                            {
                                "name": "استند و پایه خنک کننده",
                                "childs": [
                                ]
                            },
                            {
                                "name": "فلش مموریUSB ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "هاردیسک اکسترنال",
                                "childs": [
                                ]
                            },
                            {
                                "name": "اسپیکر",
                                "childs": [
                                ]
                            },
                            {
                                "name": "هدفون و هد ست و میکروفن",
                                "childs": [
                                ]
                            },
                            {
                                "name": "درایو نوری",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کارت خوان",
                                "childs": [
                                ]
                            },
                            {
                                "name": "کابل های رابط ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "کیبور",
                                "childs": [
                                ]
                            },
                            {
                                "name": "گیرنده تلویزیون دیجیتال USB",
                                "childs": [
                                ]
                            },
                            {
                                "name": "مودم اکسترنال",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": "کنسول بازی ",
                        "childs": [
                            {
                                "name": "آدابتور",
                                "childs": [

                                ]
                            },
                            {
                                "name": "جوئستیک ",
                                "childs": [

                                ]
                            },
                            {
                                "name": "سایر قطعات",
                                "childs": [
                                ]
                            },
                        ]
                    },
                ]
            },
        ]
    },
    {

        "name": "پوشاک",
        "childs": [
            {
                "name": "بانوان",
                "childs": [
                    {
                        "name": "پیراهن و لباس مجلسی",
                        "childs": [
                            {

                                "name": "تیشرت زنانه",
                                "childs": [

                                ]
                            },
                            {
                                "name": "تاپ",
                                "childs": [

                                ]
                            },
                            {
                                "name": "تونیک و شومیز و بلوز",
                                "childs": [

                                ]
                            },
                            {
                                "name": "شلوار و ساپورت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "دامن و شلوارک",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کت زنانه",
                                "childs": [

                                ]
                            },
                            {
                                "name": "بافت و ژاکت و پلیور",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پالتو و کاپشن و بارانی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "لباس زیر",
                                "childs": [

                                ]
                            },
                            {
                                "name": "چادر و چادر ملی",
                                "childs": [

                                ]
                            },
                        ]
                    },
                    {
                        "name": "کیف و کفش",
                        "childs": [
                            {
                                "name": "کیف و کفش مجلسی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کیف و کفش اداری",
                                "childs": [

                                ]
                            },
                            {
                                "name": " کفش کژوال(راحتی)",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کوله ",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کفش تخت ",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کیف پول و شانه ای",
                                "childs": [

                                ]
                            },
                        ]
                    },
                    {
                        "name": "اکسسوری",
                        "childs": [
                            {
                                "name": "شال و رو سری",
                                "childs": [

                                ]
                            },
                            {
                                "name": "زیور آلات",
                                "childs": [

                                ]
                            },
                            {
                                "name": "جوراب و جوراب شلواری",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پاپوشه زنانه",
                                "childs": [

                                ]
                            },
                            {
                                "name": "دستمال گردن و شال",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کمر بند",
                                "childs": [

                                ]
                            },
                            {
                                "name": "ساعت مچی",
                                "childs": [

                                ]
                            },
                        ]
                    },
                    {
                        "name": "عطر زنانه",
                        "childs": [
                            {
                                "name": "عطر و ادکلن",
                                "childs": [

                                ]
                            },
                            {
                                "name": "اسپری",
                                "childs": [

                                ]
                            },
                            {
                                "name": " عطر هدیه",
                                "childs": [

                                ]
                            },
                        ]
                    },
                ]
            },
            {
                "name": "آقایان",
                "childs": [
                    {
                        "name": "لباس",
                        "childs": [
                            {
                                "name": "کت و شلوار و  پالتو",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پیراهن",
                                "childs": [

                                ]
                            },
                            {
                                "name": "تی شرت و پلوشرت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "شلوار",
                                "childs": [

                                ]
                            },
                            {
                                "name": "بافت و پلیور",
                                "childs": [

                                ]
                            },
                            {
                                "name": "سیوشرت و بافت و کاپشن",
                                "childs": [

                                ]
                            },
                            {
                                "name": "جلیقه و ژیله",
                                "childs": [

                                ]
                            },
                            {
                                "name": "لباس زیر",
                                "childs": [

                                ]
                            },
                        ]
                    },
                    {
                        "name": "کیف و کفش",
                        "childs": [
                            {
                                "name": "کفش",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کیف اداری",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کوله پشتی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "دستی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کیف پول",
                                "childs": [

                                ]
                            },
                        ]
                    },
                    {
                        "name": "اکسسوری مردانه",
                        "childs": [
                            {
                                "name": "عینک آفتابی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "ساعت مچی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "دستمال گردن و پاپیون و کروات",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کلاه دستکش و شال گردن",
                                "childs": [

                                ]
                            },
                            {
                                "name": "انگشتر ",
                                "childs": [

                                ]
                            },
                        ]
                    },
                    {
                        "name": "عطر مردانه",
                        "childs": [
                            {
                                "name": "عطر و ادکلن",
                                "childs": [

                                ]
                            },
                            {
                                "name": "عطر جیبی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "اسپری",
                                "childs": [

                                ]
                            },
                            {
                                "name": "عطر هدیه ",
                                "childs": [

                                ]
                            },
                        ]
                    },
                ]
            },
            {
                "name": "کودکان",
                "childs": [
                    {
                        "name": "دخترانه",
                        "childs": [
                            {
                                "name": "بلوزو پیراهن و تونیک",
                                "childs": [

                                ]
                            },
                            {
                                "name": "تاپ و تیشرت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "شلوار و دامن",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کت، پالتو و کاپشن",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پلیور، ژاکت و سویشرت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "لباس راحتی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کفش",
                                "childs": [

                                ]
                            },
                            {
                                "name": "جوراب و جوراب شلواری",
                                "childs": [

                                ]
                            },
                            {
                                "name": "مانتو وروسری و مقنعه",
                                "childs": [

                                ]
                            }
                        ]
                    },
                    {
                        "name": "پسرانه",
                        "childs": [
                            {
                                "name": "پیراهن و تیشرت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "شلوار و شلوارک",
                                "childs": [

                                ]
                            },
                            {
                                "name": " پالتو و کاپشن",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پلیور، ژاکت و سویشرت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "لباس زیر و راحتی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کفش",
                                "childs": [

                                ]
                            },
                            {
                                "name": "اکسسوری ",
                                "childs": [

                                ]
                            },
                        ]
                    },

                ]
            },
            {
                "name": "خردسالان",
                "childs": [
                    {
                        "name": "لباس",
                        "childs": [

                        ]
                    },
                    {
                        "name": "کفش",
                        "childs": [

                        ]
                    },
                ]
            },
            {
                "name": "مادر و نوزاد",
                "childs": [
                    {
                        "name": "لباس نوزاد و سیسمونی",
                        "childs": [
                            {
                                "name": "ست کامل لباس نوزادی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "ست خواب و پتو",
                                "childs": [

                                ]
                            },
                            {
                                "name": "ایمنی و حفاظت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "حوله",
                                "childs": [

                                ]
                            },
                            {
                                "name": "لباس بارداری و شیر دهی",
                                "childs": [

                                ]
                            }
                        ]
                    },
                ]
            },
            {
                "name": "لباس ورزشی",
                "childs": [
                    {
                        "name": "بانوان",
                        "childs": [
                            {
                                "name": "کفش",
                                "childs": [

                                ]
                            },
                            {
                                "name": "ساک ورزشی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "تجهیزات ورزشی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پوشاک ورزشی",
                                "childs": [

                                ]

                            }
                        ]
                    },
                    {
                        "name": "آقایان",
                        "childs": [
                            {
                                "name": "کفش",
                                "childs": [

                                ]
                            },
                            {
                                "name": "ساک ورزشی",
                                "childs": [

                                ]
                            },
                            {
                                "name": " تجهیزات",
                                "childs": [

                                ]
                            },
                            {
                                "name": "تناسب اندام",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پوشاک ورزشی",
                                "childs": [

                                ]
                            },
                        ]
                    },
                    {
                        "name": "دختر بچه",
                        "childs": [
                            {
                                "name": "کفش",
                                "childs": [

                                ]
                            },
                            {
                                "name": "ساک ورزشی",
                                "childs": [

                                ]
                            },
                            {
                                "name": " تجهیزات",
                                "childs": [

                                ]
                            },
                            {
                                "name": "متعلقات",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پوشاک ورزشی",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": "پسر بچه",
                        "childs": [
                            {
                                "name": "کفش",
                                "childs": [

                                ]
                            },
                            {
                                "name": "ساک ورزشی",
                                "childs": [

                                ]
                            },
                            {
                                "name": " تجهیزات",
                                "childs": [

                                ]
                            },
                            {
                                "name": "متعلقات",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پوشاک ورزشی",
                                "childs": [
                                ]
                            },
                        ]
                    },
                ]
            },
        ]
    },
    {

        "name": "خانه و کاشانه",
        "childs": [
            {
                "name": "آشپز خانه",
                "childs": [
                    {
                        "name": " پخت و پز",
                        "childs": [
                            {
                                "name": "اجاق گاز",
                                "childs": [
                                ]
                            },
                            {
                                "name": "سرخ کن",
                                "childs": [
                                ]
                            },
                            {
                                "name": "پلوپز",
                                "childs": [

                                ]
                            },
                            {
                                "name": "هواپز",
                                "childs": [

                                ]
                            },
                            {
                                "name": "بخار پز",
                                "childs": [

                                ]
                            },
                            {
                                "name": "مایکرویو",
                                "childs": [

                                ]
                            },
                            {
                                "name": "زودپز",
                                "childs": [

                                ]
                            },
                            {
                                "name": "آرام پز",
                                "childs": [

                                ]
                            },
                            {
                                "name": "توستر",
                                "childs": [

                                ]
                            },
                            {
                                "name": "ساندویچ ساز",
                                "childs": [

                                ]
                            },
                            {
                                "name": "گریل و باربی کیو",
                                "childs": [

                                ]
                            },
                            {
                                "name": "سوپ پز",
                                "childs": [

                                ]

                            }
                        ]
                    },
                    {

                        "name": "تهیه غذا",
                        "childs": [
                            {
                                "name": "همزن",
                                "childs": [
                                ]
                            },
                            {
                                "name": "مخلوط کن",
                                "childs": [
                                ]
                            },
                            {
                                "name": "آسیاب",
                                "childs": [

                                ]
                            },
                            {
                                "name": "رنده برقی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "آسیاب",
                                "childs": [

                                ]
                            },
                            {
                                "name": "گوشکوب برقی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "خرد کن",
                                "childs": [

                                ]
                            },
                            {
                                "name": "چرخ گوشت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "چاقو برقی",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {

                        "name": "تهیه نوشیدنی",
                        "childs": [
                            {
                                "name": "آب و مرکبات گیری",
                                "childs": [
                                ]
                            },
                            {
                                "name": "آب میوه گیری",
                                "childs": [
                                ]
                            },
                            {
                                "name": "کتری برقی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "تصفیه آب",
                                "childs": [

                                ]
                            },
                            {
                                "name": "چای ساز",
                                "childs": [

                                ]
                            },
                            {
                                "name": "قهوه ساز",
                                "childs": [

                                ]
                            },
                            {
                                "name": "آب سردکن",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کتری رو گاز",
                                "childs": [

                                ]
                            },
                            {
                                "name": "سماور برقی",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {

                        "name": "یخچال و فریزر",
                        "childs": [
                            {
                                "name": "دوقلو",
                                "childs": [
                                ]
                            },
                            {
                                "name": "دو درب",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ساید بای ساید",
                                "childs": [

                                ]
                            },
                            {
                                "name": "فرانسوی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "فریزر",
                                "childs": [

                                ]
                            },
                            {
                                "name": "یخچال فریزیر کوچک",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {

                        "name": "سرو و پذیرایی",
                        "childs": [
                            {
                                "name": "سرویس غذا خوری",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ظروف سرو و پذیرایی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ظروف یکبار مصرف",
                                "childs": [

                                ]
                            },
                            {
                                "name": "آسیاب نمک و فلفل",
                                "childs": [

                                ]
                            },
                            {
                                "name": "ظروف کریستال",
                                "childs": [

                                ]
                            },
                            {
                                "name": "سرویس قاشق و چنگال و کارد",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پارچ و بطری ئ لیوان",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کلمن و فلاکس",
                                "childs": [

                                ]
                            },
                            {
                                "name": "رو میزیو زیر لیوانی",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {

                        "name": "تهویه هوا",
                        "childs": [
                            {
                                "name": "انواع هود ها",
                                "childs": [
                                ]
                            },
                        ]
                    }
                ]
            },
            {
                "name": "لوازم خانگی",
                "childs": [
                    {
                        "name": "شست و شو",
                        "childs": [
                            {
                                "name": "ماشین ظرف شویی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ماشین لباس شویی تمام اتوماتیک",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ماشین لباسشویی دستی و دوقلو",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کهنه شور",
                                "childs": [

                                ]
                            },
                            {
                                "name": "بخارشور",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کارواش",
                                "childs": [

                                ]
                            },
                            {
                                "name": "خشک کن",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کف شوی",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": "نظافت",
                        "childs": [
                            {
                                "name": "جارو برقی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "جارو برقی صنعتی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "اتو کشی",
                                "childs": [

                                ]
                            },
                        ]
                    },
                ]
            },
            {
                "name": "اتاق خواب",
                "childs": [
                    {
                        "name": "سرویس خواب",
                        "childs": [

                        ]
                    },
                    {
                        "name": "تشک",
                        "childs": [

                        ]
                    },
                    {
                        "name": "پتو",
                        "childs": [

                        ]
                    },
                    {
                        "name": "بالش و رو بالشتی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "وسنک",
                        "childs": [

                        ]
                    },
                    {
                        "name": "کمد  ",
                        "childs": [

                        ]
                    },
                ]
            },
            {
                "name": "صوت و تصویر",
                "childs": [
                    {
                        "name": "تلویزیون",
                        "childs": [

                        ]
                    },
                    {
                        "name": "سینمای خانگی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "گیرنده دیجیتال DVB-T",
                        "childs": [

                        ]
                    },
                    {
                        "name": "پخش کننده موسیقی و ویدئو پرتال",
                        "childs": [

                        ]
                    },
                    {
                        "name": "سیستم شنیداری خانگی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "دیتا - ویدئو پروژکتور",
                        "childs": [

                        ]
                    },
                    {
                        "name": "ضبط کننده صدا",
                        "childs": [

                        ]
                    },
                    {
                        "name": "پخش کننده چند رسانه خانگی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "رادیو",
                        "childs": [

                        ]
                    },
                ]
            },
            {
                "name": "سرمایش و گرمایش",
                "childs": [
                    {
                        "name": " کولر های آبی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "کولر های گازی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "پنکه",
                        "childs": [

                        ]
                    },
                    {
                        "name": "بخاری ",
                        "childs": [

                        ]
                    },
                    {
                        "name": "هیتر",
                        "childs": [

                        ]
                    },
                    {
                        "name": "رادیاتور",
                        "childs": [

                        ]
                    },
                    {
                        "name": "کرسی",
                        "childs": [

                        ]
                    },
                ]
            },
            {

                "name": "دکوری",
                "childs": [
                    {
                        "name": " صنایع دستی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "فرش و تابلو فرش ",
                        "childs": [

                        ]
                    },
                    {
                        "name": "مبلمان ",
                        "childs": [

                        ]
                    },
                    {
                        "name": "آباژور ",
                        "childs": [

                        ]
                    },
                    {
                        "name": "لوست",
                        "childs": [

                        ]
                    },
                    {
                        "name": "مجسمه و قاب عکس ",
                        "childs": [

                        ]
                    },
                    {
                        "name": "کالاها لوکس",
                        "childs": [
                        ]
                    },
                    {
                        "name": "گل های تزئینی",
                        "childs": [

                        ]
                    },
                ]
            },
        ]
    },
    {

        "name": "زیبایی و سلامت",
        "childs": [
            {
                "name": "زیبایی",
                "childs": [
                    {
                        "name": " آرایشی",
                        "childs": [
                            {
                                "name": "آرایش صورت ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "آرایش لب ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "آرایش چشمو ابرو ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "آرایش مو",
                                "childs": [
                                ]
                            },
                            {
                                "name": "آرایش ناخن ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ابزار آرایشی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "پاک کننده های آرایشی",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": " مراقبتی",
                        "childs": [
                            {
                                "name": "مراقبت از صورت",
                                "childs": [
                                ]
                            },
                            {
                                "name": "مراقبت از چشم",
                                "childs": [
                                ]
                            },
                            {
                                "name": "مراقبت از لب",
                                "childs": [
                                ]
                            },
                            {
                                "name": "مراقبت از ناخن",
                                "childs": [
                                ]
                            },
                            {
                                "name": "مراقبت از بدن",
                                "childs": [
                                ]
                            },
                            {
                                "name": "مراقبت از مو",
                                "childs": [
                                ]
                            },
                            {
                                "name": "مراقبت مادر و فرزند",
                                "childs": [
                                ]
                            },
                        ]
                    },
                ]
            },
            {
                "name": "سلامت",
                "childs": [
                    {
                        "name": "ماساژور",
                        "childs": [
                        ]
                    },
                    {
                        "name": "فشار سنج",
                        "childs": [
                        ]
                    },
                    {
                        "name": "ساعت هوشمند و دستبند سلامت",
                        "childs": [
                        ]
                    },
                    {
                        "name": "ترازو",
                        "childs": [
                        ]
                    },
                    {
                        "name": "نمایشگر ضربان قلب",
                        "childs": [
                        ]
                    },
                    {
                        "name": "دما سنج",
                        "childs": [
                        ]
                    },
                    {
                        "name": "بخور و تصفیه هوا",
                        "childs": [
                        ]
                    },
                    {
                        "name": "درمانگرهای موضعی",
                        "childs": [
                        ]
                    },
                ]
            },
            {
                "name": "بهداشت شخصی",
                "childs": [
                    {
                        "name": "لوازم اصلاح",
                        "childs": [
                        ]
                    },
                    {
                        "name": "شامپو و نرمکننده",
                        "childs": [
                        ]
                    },
                    {
                        "name": "انواع لوسیون ها",
                        "childs": [
                        ]
                    },
                    {
                        "name": "بهداشت دهان و دندان",
                        "childs": [
                        ]
                    },
                    {
                        "name": "ضد عفونی کننده ها",
                        "childs": [
                        ]
                    },
                    {
                        "name": "لوازم استحمام",
                        "childs": [
                        ]
                    },
                    {
                        "name": "انواع پودر های شوینده",
                        "childs": [

                        ]
                    },
                ]
            },
        ]
    },
    {

        "name": "بازی و سرگرمی",
        "childs": [
            {
                "name": "لگو و ساختنی",
                "childs": [
                    {
                        "name": "رده سنی",
                        "childs": [
                            {
                                "name": "سه ماه تا دو سال",
                                "childs": [
                                ]
                            },
                            {
                                "name": "دو سال تا چهار سال",
                                "childs": [
                                ]
                            },
                            {
                                "name": "چهار سال تا شش سال",
                                "childs": [
                                ]
                            },
                            {
                                "name": "شش سال تا هشت سال",
                                "childs": [
                                ]
                            },
                            {
                                "name": "هشت سال تا دوازده سال",
                                "childs": [
                                ]
                            },
                            {
                                "name": "دوازده سال تا پانزده سال",
                                "childs": [
                                ]
                            },
                            {
                                "name": "پانزده سال تا بزرگسال",
                                "childs": [
                                ]
                            },
                            {
                                "name": "بزرگسال",
                                "childs": [
                                ]
                            },
                        ]
                    },
                ]
            },
            {
                "name": "عروسک",
                "childs": [
                    {
                        "name": "عروسک های شخصیت ایرانی",
                        "childs": [
                        ]
                    },
                    {
                        "name": "عروسک های شخصیت های خارجی",
                        "childs": [
                        ]
                    },
                    {
                        "name": "عروسک موزیکار",
                        "childs": [
                        ]
                    },
                    {
                        "name": "عروسک های نمایشی",
                        "childs": [
                        ]
                    },
                ]
            },
            {
                "name": "سرگرمی ",
                "childs": [
                    {
                        "name": "انواع اسباب بازی های کنترلی",
                        "childs": [
                        ]
                    },
                    {
                        "name": "اسباب بازی های پلیسی",
                        "childs": [
                        ]
                    },
                    {
                        "name": "شعبده بازی",
                        "childs": []
                    },
                    {
                        "name": "شعبده بازی",
                        "childs": [
                        ]
                    },
                    {
                        "name": "شوکی و برقگیر",
                        "childs": [
                        ]
                    },
                    {
                        "name": "ابزار تغییر چهره",
                        "childs": [
                        ]
                    },
                    {
                        "name": "انواع ماسک ها",
                        "childs": [
                        ]
                    },
                ]
            },
            {
                "name": "فکری",
                "childs": [
                    {
                        "name": "ویژگی",
                        "childs": [
                            {
                                "name": "تقویت خلاقیت",
                                "childs": [
                                ]
                            },
                            {
                                "name": "تقویت حافظه",
                                "childs": [
                                ]
                            },
                            {
                                "name": "قدرت حل مسئله",
                                "childs": [
                                ]
                            },
                            {
                                "name": "خانوادگی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "استراتژی و برنامه ریزی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "درک و شناخت",
                                "childs": [
                                ]
                            },
                            {
                                "name": "مهارت های حرکتی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "قدرت تجسم",
                                "childs": [
                                ]
                            },
                            {
                                "name": "تمرکز و دقت",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": "رده سنی",
                        "childs": [
                            {
                                "name": "دو تا پنج سال",
                                "childs": [
                                ]
                            },
                            {
                                "name": "پنج تا هشت سال",
                                "childs": [
                                ]
                            },
                            {
                                "name": "هشت سال به بالا",
                                "childs": [
                                ]
                            },
                            {
                                "name": "بازی های حرفه ای",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": "طبقه بندی بر اساس تعداد بازیکن ",
                        "childs": [
                            {
                                "name": "یکنفره",
                                "childs": [
                                ]
                            },
                            {
                                "name": "دو یا چند نفره",
                                "childs": [
                                ]
                            },
                        ]
                    },
                ]
            },
            {
                "name": "آموزشی",
                "childs": [
                    {
                        "name": "رده سنی",
                        "childs": [
                            {
                                "name": "سه ماه تا دو سال",
                                "childs": [
                                ]
                            },
                            {
                                "name": "دو سال تا چهار سال",
                                "childs": [
                                ]
                            },
                            {
                                "name": "چهار سال تا شش سال",
                                "childs": [
                                ]
                            },
                            {
                                "name": "شش سال تا هشت سال",
                                "childs": [
                                ]
                            },
                            {
                                "name": "هشت سال تا دوازده سال",
                                "childs": [
                                ]
                            },
                            {
                                "name": "دوازده سال تا پانزده سال",
                                "childs": [
                                ]
                            },
                            {
                                "name": "پانزده سال تا بزرگسال",
                                "childs": [
                                ]
                            },
                            {
                                "name": "بزرگسال",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": "نوع آموزشی",
                        "childs": [
                            {
                                "name": "آوایی و گفتاری",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ادراکی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "قدرت حل مسئله",
                                "childs": [
                                ]
                            },
                            {
                                "name": "شنیداری",
                                "childs": [
                                ]
                            },
                            {
                                "name": "استراتژی و برنامه ریزی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "مهارت های حرکتی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "مهارت های اجتماعی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "قدرت تجسم",
                                "childs": [
                                ]
                            },
                            {
                                "name": "حافظه ای",
                                "childs": [
                                ]
                            },
                        ]
                    },
                ]
            },
            {
                "name": "ورزشی",
                "childs": [
                    {
                        "name": "انواع دوچرخه",
                        "childs": [
                        ]
                    },
                    {
                        "name": "اسکوتر",
                        "childs": [
                        ]
                    },
                    {
                        "name": "اسکیت و لوازم جانبی",
                        "childs": [
                        ]
                    },
                    {
                        "name": "میز و لوازم بیایارد",
                        "childs": [
                        ]
                    },
                    {
                        "name": "میز و لوازم تنیس",
                        "childs": [
                        ]
                    },
                    {
                        "name": "راکت و لوازم بدمینتون",
                        "childs": [
                        ]
                    },
                    {
                        "name": "اسکوآش و لوازم جانبی",
                        "childs": [
                        ]
                    },
                    {
                        "name": "لوازم ورزشی رزمی",
                        "childs": [
                        ]
                    },
                    {
                        "name": "لوازم شنا",
                        "childs": [
                        ]
                    },
                    {
                        "name": "فوتبال دستی",
                        "childs": [
                        ]
                    },
                    {
                        "name": "لوازم اسکی",
                        "childs": [
                        ]
                    },
                    {
                        "name": "دارت",
                        "childs": [
                        ]
                    },
                    {
                        "name": "تیر و کمان ",
                        "childs": [
                        ]
                    },
                    {
                        "name": "تجهیزات کوه نوردی",
                        "childs": [
                        ]
                    },
                    {
                        "name": "تجهیزات شمشیر زنی ",
                        "childs": [
                        ]
                    },
                ]
            },
        ]
    },
    {
        "name": "فرهنگ و هنر",
        "childs": [
            {
                "name": "کتاب",
                "childs": [
                    {
                        "name": "علمی",
                        "childs": [
                            {
                                "name": "اقتصاد",
                                "childs": [
                                ]
                            },
                            {
                                "name": "پزشکی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "مدیریت",
                                "childes": [
                                ]
                            },
                            {
                                "name": "هوا و فضا",
                                "childs": [
                                ]
                            },
                            {
                                "name": "فیزیک",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": "هنر",
                        "childs": [
                            {
                                "name": "گرافیک",
                                "childs": [
                                ]
                            },
                            {
                                "name": "عکاسی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "مهماری",
                                "childs": [
                                ]
                            },
                            {
                                "name": "داستان نویسیا",
                                "childs": [
                                ]
                            },
                            {
                                "name": "آرایشگری",
                                "childs": [
                                ]
                            },
                            {
                                "name": "آشپزی",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": "آموزشی",
                        "childs": [
                            {
                                "name": "کمک درسی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "آموزش زبان انگلیسی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "آموزش کامپیوتر",
                                "childs": [
                                ]
                            },
                            {
                                "name": "دانشگاهی",
                                "childs": [
                                    {
                                        "name": "پزشکی",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "کشاورزی",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "علوم انسانی",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "علوم پایه",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "مهندسی",
                                        "childs": [
                                        ]
                                    },
                                ]
                            },
                        ]
                    },
                    {

                        "name": "عمومی",
                        "childs": [
                            {
                                "name": "رمان",
                                "childs": [
                                ]
                            },
                            {
                                "name": "موفقیت و روانشناسی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "تاریخی و سیاسی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "علوم ماوراء طبیعه",
                                "childs": [
                                ]
                            },
                            {
                                "name": "بهداشت و سلامت",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ازدواج و زناشویی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "هوش و استعداد",
                                "childs": [
                                ]
                            },
                            {
                                "name": "سفر نامه ها",
                                "childs": [
                                ]
                            },
                            {
                                "name": "مجلات داخلی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "مجلالت خارجی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "کتاب های نفیس",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": "کودک",
                        "childs": [
                            {
                                "name": "مادر و فرزند",
                                "childs": [
                                ]
                            },
                            {
                                "name": "تغذیه کودک",
                                "childs": [
                                ]
                            },
                            {
                                "name": "تربیت کودک",
                                "childs": [
                                ]
                            },
                            {
                                "name": "کتاب داستان مخصوص کودکان",
                                "childs": [
                                ]
                            },
                            {
                                "name": "بهداشت کودک",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ورزش برای کودکان",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": "مذهبی",
                        "childs": [
                            {
                                "name": "علوم قرآنی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "احادیث",
                                "childs": [
                                ]
                            },
                            {
                                "name": "قرآن نفیس",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ادعیه",
                                "childs": [
                                ]
                            },
                            {
                                "name": "عرفان و تصوف",
                                "childs": [
                                ]
                            },
                            {
                                "name": "اخلاق و معلرف",
                                "childes": [
                                ]
                            },
                            {
                                "name": "زندگی نامه ",
                                "childs": [
                                ]
                            },
                        ]
                    },
                ]
            },
            {

                "name": "لوازم تحریر",
                "childs": [
                    {
                        "name": "نوشت افزار و لوازم جانبی",
                        "childs": [
                            {
                                "name": "مداد",
                                "childs": [
                                ]
                            },
                            {
                                "name": "اتود",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ماژیک و علامت زن",
                                "childs": [
                                ]
                            },
                            {
                                "name": "پاکن ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "خودکار و روانویس",
                                "childs": [
                                ]
                            },
                            {
                                "name": "تراش و تراش رو میزی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "خط کش فنری و ژله ای ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "میز تحریر",
                                "childs": [
                                ]
                            },
                            {
                                "name": "لاک غلط گیر",
                                "childs": [
                                ]
                            },
                            {
                                "name": "جوهر ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "قلم و دوات ",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": "ابزار طراحی و نقاشی",
                        "childs": [
                            {
                                "name": "بوم نقاشی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "تخته شاسی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "قلمو نقاشی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "رنگ انگشتی ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ماژیک نقاشی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "گواش و آبرنگ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "چسب رنگی و فانتزی ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "پاستل و مداد شمعی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "مداد طراحی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "مداد رنگی ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "انواع رنگ نقاشی ",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": "ابزار طراحی و مهندسی",
                        "childs": [
                            {
                                "name": "کیف آرشیو",
                                "childs": [
                                ]
                            },
                            {
                                "name": "نقاله .گونیاو پرگار",
                                "childs": [
                                ]
                            },
                            {
                                "name": "راپید و خط کش مهندسی ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "میز نقشه کشی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ماشین حساب",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": "لوازم اداری",
                        "childs": [
                            {
                                "name": "لوازم اداری رومیزی",
                                " childs": [
                                    {
                                        "name": "زیر تقویم",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "پایه چسب",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "قیچی اداری",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "ابر پول شمار",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "پرچم رو میزی",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "ماشین دوخت",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "ماشین حساب",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "کاغذ یادداشت",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "جای کارت ویزیت",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "پانچ و سوزن کش",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "جا قلمی و پایه خودکار",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "مهر تاریخ زن و شماره زن",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "سوزن ته گرد و جا سوزنی",
                                        "childs": [
                                        ]
                                    },
                                ]
                            },
                            {
                                "name": "ملزومات دفتر کار",
                                " childs": [
                                    {
                                        "name": "کازیه",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "زیر پایی",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "زیر کیسی",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "جا مجاه ای",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "میز مانیتور",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "سطل اداری",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "صندوق پیشنهادات",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "دستگاه کاغذ خورد کن",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "لاک و دستگاه پلاک آب کن",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "تخته وایت برد",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "ساعت رو میزی و دیواری",
                                        "childs": [
                                        ]
                                    },
                                ]
                            },
                            {
                                "name": "مجموعه لوازم اداری",
                                " childs": [
                                    {
                                        "name": "طلق پرسی",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "چسب کش",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "گیوتین کاغذ",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "زونک و پوشه اداری",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "کاتر و تیغ کاتر",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "رول قیمت زن",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "دستگاه لمنتور",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "دستگاه قیمت زن",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "مگنت و تخته پاک کن",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "لیبل و برچسب پرینت",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "دستگاه پرفراژ  ",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "فنر پوشه و طلق پوشه",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "غش گیر و گیره کتاب",
                                        "childs": [
                                        ]
                                    },
                                ]
                            },
                            {
                                "name": "اقلام مصرفی اداری",
                                "childs": [
                                    {
                                        "name": "چسب",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "کاغذ",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "طلق و شیرازه",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "کاور پلاستیکی",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "پاکت نامه اداری",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "کارتابل و دیوایدر",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "جوهر خودنویس",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "پونز و سوزن نقشه",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "کلیپس و گیره دوبل",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "سوزن و ماشین دوخت",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "کلیر بوک و نخ اسکناس",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "استامپ و جوهر استامپ",
                                        "childs": [
                                        ]
                                    },
                                    {
                                        "name": "پوشه پلاستیکی دکمه دار",
                                        "childs": [
                                        ]
                                    },
                                ]
                            },
                        ]
                    },
                ]
            },
            {

                "name": "آلات موسیقی",
                "childs": [
                    {
                        "name": "ساز های زهی",
                        "childs": [
                            {
                                "name": "بربط",
                                "childs": [
                                ]
                            },
                            {
                                "name": "تار آذربایجانی (9 سیم)",
                                "childs": [
                                ]
                            },
                            {
                                "name": "تار",
                                "childs": [
                                ]
                            },
                            {
                                "name": "تار باس",
                                "childs": [
                                ]
                            },
                            {
                                "name": "تنبور",
                                "childs": [
                                ]
                            },
                            {
                                "name": "چگور",
                                "childs": [
                                ]
                            },
                            {
                                "name": "دوتار",
                                "childs": [
                                ]
                            },
                            {
                                "name": "رباب",
                                "childs": [
                                ]
                            },
                            {
                                "name": "سه تار",
                                "childs": [
                                ]
                            },
                            {
                                "name": "سنتور",
                                "childs": [
                                ]
                            },
                            {
                                "name": "کمانچه",
                                "childs": [
                                ]
                            },
                            {
                                "name": "گیتار",
                                "childs": [
                                ]
                            },
                            {
                                "name": "قانون",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ویولون",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ویلولون سل",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {
                        "name": "ساز بادی",
                        "childs": [
                            {

                                "name": "ترمبون",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ترومپت",
                                "childs": [
                                ]
                            },
                            {
                                "name": "تویتوت(نی ترکمی)",
                                "childs": [
                                ]
                            },
                            {
                                "name": "توبا",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ساکسیفون",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ساز دهنی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "سرنا",
                                "childs": [
                                ]
                            },
                            {
                                "name": "شیپور",
                                "childs": [
                                ]
                            },
                            {
                                "name": "فلوت",
                                "childs": [
                                ]
                            },
                            {
                                "name": "نی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "نی انبان",
                                "childs": [
                                ]
                            },
                            {
                                "name": "فاگوت",
                                "childs": [
                                ]
                            },
                            {
                                "name": "شهنای",
                                "childs": [
                                ]
                            },
                            {
                                "name": "دو نی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "شمشال",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {

                        "name": "ساز کوبه ای",
                        "childs": [
                            {

                                "name": "دف",
                                "childs": [
                                ]
                            },
                            {
                                "name": "دایره",
                                "childs": [
                                ]
                            },
                            {
                                "name": "تنبک(ضرب)",
                                "childs": [
                                ]
                            },
                            {
                                "name": "طبل",
                                "childs": [
                                ]
                            },
                            {
                                "name": "دامارو",
                                "childs": [
                                ]
                            },
                            {
                                "name": "دمام",
                                "childs": [
                                ]
                            },
                            {
                                "name": "سنج",
                                "childs": [
                                ]
                            },
                            {
                                "name": "زنبورک",
                                "childs": [
                                ]
                            },
                            {
                                "name": "دهل",
                                "childs": [
                                ]
                            },
                            {
                                "name": "تمپو",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {

                        "name": "سازهای صفحه کلید دار",
                        "childs": [
                            {

                                "name": "پیانو",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ارگ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "کلاوسن",
                                "childs": [
                                ]
                            },
                            {
                                "name": "آکوردئون",
                                "childs": [
                                ]
                            },
                            {
                                "name": "هارمونیکا",
                                "childs": [
                                ]
                            },
                            {
                                "name": "تمپو",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {

                        "name": "ساز های الکترونیک",
                        "childs": [
                            {

                                "name": "ارگ برقی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ترمین",
                                "childs": [
                                ]
                            },
                            {
                                "name": "کیبورد",
                                "childs": [
                                ]
                            },
                            {
                                "name": "گیتار الکترونیک",
                                "childs": [
                                ]
                            },
                            {
                                "name": "گیتار بیس",
                                "childs": [

                                ]
                            },
                        ]
                    },
                ]
            },
            {

                "name": "موسیقی",
                "childs": [
                    {
                        "name": "آموزش موسیقی های زهی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "آموزش سازهای بادی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "آموزش ساز های کوبه ای",
                        "childs": [

                        ]
                    },
                    {
                        "name": "آموزش سازهای صفحه کلید دار",
                        "childs": [

                        ]
                    },
                    {
                        "name": "آموزش ساز های الکترونیکی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "موسیقس محلی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "انواع موسیقی ",
                        "childs": [

                        ]
                    },
                    {
                        "name": "درباره موسیقی",
                        "childs": [

                        ]
                    },
                ]
            },
            {
                "name": "فیلم",
                "childs": [
                    {
                        "name": "فیلم",
                        "childs": [

                        ]
                    },
                    {
                        "name": "سریال ",
                        "childs": [

                        ]
                    },
                    {
                        "name": "انیمیشن",
                        "childs": [

                        ]
                    },
                    {
                        "name": "کارتون",
                        "childs": [
                        ]
                    },
                ]
            },
            {
                "name": "نرم افزار و بازی",
                "childs": [
                    {
                        "name": "کاربردی",
                        "childs": [
                            {
                                "name": "مالتی مدیا",
                                "childs": [
                                ]
                            },
                            {
                                "name": "امنیتی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "گرافیکی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "دیسک",
                                "childs": [
                                ]
                            },
                            {
                                "name": "اینترنت",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ارتباطات",
                                "childs": [
                                ]
                            },
                            {
                                "name": "اداری ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "دسکتاپ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "کاربردی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "سیستم عامل ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "آموزشی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "خانگی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "کاربردی ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "توسعه",
                                "childs": [
                                ]
                            },
                        ]
                    },
                    {

                        "name": "بازی",
                        "childs": [
                            {
                                "name": "اکشن",
                                "childs": [
                                ]
                            },
                            {
                                "name": "تیراندازی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "مسابقه ای",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ورزشی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "سرگرم کننده",
                                "childs": [
                                ]
                            },
                            {
                                "name": "معمایی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "استراتژی ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "ماجرایی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "پلاتفرم",
                                "childs": [
                                ]
                            },
                            {
                                "name": "نقش آفرین ",
                                "childs": [
                                ]
                            },
                            {
                                "name": "شبیه سازی",
                                "childs": [
                                ]
                            },
                            {
                                "name": "کودکان",
                                "childs": [
                                ]
                            },
                        ]
                    },
                ]
            },
        ]
    },
    {
        "name": "ابزار آلات",
        "childs": [
            {
                "name": "برقی",
                "childs": [
                    {
                        "name": "کفسابها",
                        "childs": [

                        ]
                    },
                    {
                        "name": "دریل ها",
                        "childs": [

                        ]
                    },
                    {
                        "name": "اره ها",
                        "childs": [

                        ]
                    },
                    {
                        "name": "بتن شکن ها",
                        "childs": [

                        ]
                    },
                    {
                        "name": "سمباده لرزان",
                        "childs": [

                        ]
                    },
                    {
                        "name": "فرز انگشتی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "پیچگوشتی برقی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "جارو برقی خشک و تر",
                        "childs": [

                        ]
                    },
                    {
                        "name": "سشوار صنعتی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "بکس برقی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "پروفیل بر",
                        "childs": [

                        ]
                    },
                    {
                        "name": "رنگ پاش برقی ",
                        "childs": [

                        ]
                    },
                    {
                        "name": "همزنها",
                        "childs": [

                        ]
                    },
                    {
                        "name": "ورق بر ها",
                        "childs": [

                        ]
                    },
                    {
                        "name": "شیار زن ها",
                        "childs": [
                        ]
                    },
                ]
            },
            {
                "name": "شارژی",
                "childs": [
                    {
                        "name": "دریل شارژی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "پیچگوشتی شارژی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "کفساب شارژی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "دریل بتون کن شارژی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "اره شارژی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "بکس شارژی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "منگنه و میخ کوب شارژی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "ست ابزار شارژی",
                        "childs": [

                        ]
                    },
                ]
            },
            {
                "name": "دستی",
                "childs": [
                    {
                        "name": "آچار آلات",
                        "childs": [

                        ]
                    },
                    {
                        "name": "انبر ها",
                        "childs": [

                        ]
                    },
                    {
                        "name": "چکش ها  و تبر ها",
                        "childs": [

                        ]
                    },
                    {
                        "name": "جعبه ابزار ها",
                        "childs": [

                        ]
                    },
                    {
                        "name": "بکس ها",
                        "childs": [

                        ]
                    },
                    {
                        "name": "اره",
                        "childs": [

                        ]
                    },
                    {
                        "name": "چاقو ",
                        "childs": [

                        ]
                    },
                    {
                        "name": "کاتر ها  ",
                        "childs": [

                        ]
                    },
                    {
                        "name": "کیت ابزار",
                        "childs": [

                        ]
                    },
                ]
            },
            {
                "name": "بادی",
                "childs": [
                    {
                        "name": "کمپرسور ها",
                        "childs": [

                        ]
                    },
                    {
                        "name": "بکس بادی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "دریل بادی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "رنگ پاشی و گان ها",
                        "childs": [

                        ]
                    },
                    {
                        "name": "گریس پمپ بادی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "پیچگوشتی بادی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "پولیش بادی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "سنگ فرز بادی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "پرچ کن بادی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "سمباده بادی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "فرز انگشتی بادی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "چکش تخریب کن",
                        "childs": [

                        ]
                    },
                    {
                        "name": "جغجغه بادی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "اره نوک بادی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "میخکوب و منگه کوب",
                        "childs": [

                        ]
                    },
                    {
                        "name": "لوازم جانبی ابزار های بادی",
                        "childs": [
                        ]
                    },
                ]
            },
            {
                "name": "گاراژی",
                "childs": [
                    {
                        "name": "شارژر باتری و استارتر",
                        "childs": [

                        ]
                    },
                    {
                        "name": "گریس . روغن . واسکازین پمپ ",
                        "childs": [

                        ]
                    },
                    {
                        "name": "جک ها و خرک ها",
                        "childs": [

                        ]
                    },
                    {
                        "name": "ماشین شستشوی فشار قوی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "آپارات و پنچر گیری",
                        "childs": [

                        ]
                    },
                    {
                        "name": "آچار فیلتر ها",
                        "childs": [

                        ]
                    },
                    {
                        "name": "لوازم جانبی گاراژی",
                        "childs": [

                        ]
                    },
                ]
            },
            {
                "name": "ساختمانی",
                "childs": [
                    {
                        "name": "میلگرد بر",
                        "childs": [

                        ]
                    },
                    {
                        "name": "سرامیک بر",
                        "childs": [

                        ]
                    },
                    {
                        "name": "میلگرد خم کن",
                        "childs": [

                        ]
                    },
                    {
                        "name": "ماشین میلگرد بری",
                        "childs": [

                        ]
                    },
                    {
                        "name": "حدیده لوله",
                        "childs": [

                        ]
                    },
                    {
                        "name": "جوش پلی پروپیلن(لوله سبز)",
                        "childs": [

                        ]
                    },
                    {
                        "name": "آچار لوله گیر",
                        "childs": [

                        ]
                    },
                    {
                        "name": "جوش پلی اتیلن",
                        "childs": [

                        ]
                    },
                    {
                        "name": "لوله باز کن برقی و دستی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "متر لیزری",
                        "childs": [

                        ]
                    },
                    {
                        "name": "تراز لیزری",
                        "childs": [

                        ]
                    },
                ]
            },
            {
                "name": "کارگاهی",
                "childs": [
                    {
                        "name": "گیره ها",
                        "childs": [

                        ]
                    },
                    {
                        "name": "بالا بر ها",
                        "childs": [

                        ]
                    },
                    {
                        "name": "حمل کالا",
                        "childs": [

                        ]
                    },
                ]
            },
            {
                "name": "جوش و برش",
                "childs": [
                    {
                        "name": "هوا و گاز",
                        "childs": [

                        ]
                    },
                    {
                        "name": "فلکه ای",
                        "childs": [

                        ]
                    },
                    {
                        "name": "ایتورتر",
                        "childs": [

                        ]
                    },
                    {
                        "name": "رکتی فایر تک فاز",
                        "childs": [

                        ]
                    },
                    {
                        "name": "رکتی فایر 3 فاز",
                        "childs": [

                        ]
                    },
                    {
                        "name": "ملزومات جانبی جوش",
                        "childs": [

                        ]
                    },
                    {
                        "name": "نقطه جوش",
                        "childs": [

                        ]
                    },
                    {
                        "name": "برش پلاسما",
                        "childs": [

                        ]
                    },
                    {
                        "name": "mig/mag",
                        "childs": [

                        ]
                    },
                    {
                        "name": "گاز Co2",
                        "childs": [

                        ]
                    },
                    {
                        "name": "انواع موتور جوش ",
                        "childs": [
                        ]
                    },
                ]
            },

        ]
    },
]
cursor.category.remove({})

category_order = 1
for cat in CAT_LIST:
    cat_temp = {
        "id": ID,
        "name": cat['name'],
        "order": category_order,
        "ancestors": [0, ],
        "url": "",
        "published": True,
        "ready_to_delete": False
    }
    cursor.category.insert_one(cat_temp)
    # print
    # print cat_temp['id']

    ID += 1
    category_order += 1

    group_order= 1
    group_ancestors= [cat_temp['id'], ]
    for group in cat['childs']:
        group_temp= {
            "id": ID,
            "name": group['name'],
            "order": group_order,
            "ancestors": group_ancestors,
            "url": "",
            "published": True,
            "ready_to_delete": False
        }
        cursor.category.insert_one(group_temp)
        ID += 1
        group_order += 1

        subgroup_order= 1
        subgroup_ancestors= group_ancestors[:]
        subgroup_ancestors.append(group_temp['id'])

        for subgroup in group['childs']:
            subgroup_temp= {
                "id": SUB_ID,
                "name": subgroup['name'],
                "order": subgroup_order,
                "ancestors": subgroup_ancestors,
                "url": "",
                "published": True,
                "ready_to_delete": False
            }
            cursor.category.insert_one(subgroup_temp)
            SUB_ID += 1
            subgroup_order += 1

            subsubgroup_order= 1
            subsubgroup_ancestors= subgroup_ancestors[:]
            subsubgroup_ancestors.append(subgroup_temp['id'])
            for subsubgroup in subgroup['childs']:
                subsubgroup_temp= {
                    "id": SUB_ID,
                    "name": subsubgroup['name'],
                    "order": subsubgroup_order,
                    "ancestors": subsubgroup_ancestors,
                    "url": "",
                    "published": True,
                    "ready_to_delete": False
                }
                cursor.category.insert_one(subsubgroup_temp)
                SUB_ID += 1
                subsubgroup_order += 1

cursor.counters.update_one({"_id": "category_id"}, {"$set": {"seq": ID}})
cursor.counters.update_one({"_id": "subgroup_id"}, {"$set": {"seq": SUB_ID}})
