# coding: utf-8
"""
Category and Group "id"s will Start from 1, Subgroup "id"s will start from 400.
finally counter fields for auto incrementing  "category_id" and "subgroup_id" will set in SHOP.counters.
"""
from core.db_mongo import cursor

ID = 1
SUB_ID = 400

CAT_LIST = [
    {
        "name": "محصولات دیجیتال",
        "childs": [
            {
                "name": "موبایل",
                "childs": [

                ]
            },
            {
                "name": "تبلت",
                "childs": [

                ]
            },
            {
                "name": "دوربین عکاسی",
                "childs": [

                ]
            },
            {
                "name": "دوربین دوربین فیلم برداری",
                "childs": [

                ]
            },
            {
                "name": "لپ تاپ",
                "childs": [

                ]
            },
            {
                "name": "ماشینهای اداری",
                "childs": [

                ]
            },
            {
                "name": "کنسول بازی",
                "childs": [

                ]
            },
            {
                "name": "موزیک پلیر",
                "childs": [

                ]
            },
        ]
    },
    {
        "name": "پوشاک",
        "childs": [
            {
                "name": "بانوان",
                "childs": [
                    {
                        "name": "لباس",
                        "childs": [
                            {
                                "name": "مجلسی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "مانتو",
                                "childs": [

                                ]
                            },
                            {
                                "name": "تونیک، تاپ، شومیز و بلوز",
                                "childs": [

                                ]
                            },
                            {
                                "name": "شلوار و ساپورت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "دامن و شلوارک",
                                "childs": [

                                ]
                            },
                            {
                                "name": "شال و روسری",
                                "childs": [

                                ]
                            },
                            {
                                "name": "بافت، ژاکت و پلیور",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پالتو، کاپشن و بارانی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "لباس زیر",
                                "childs": [

                                ]
                            },
                        ]
                    },
                    {
                        "name": "کیف",
                        "childs": [
                            {
                                "name": "مجلسی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "دستی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "دوشی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پاسپورتی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کوله",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پول",
                                "childs": [

                                ]
                            },
                        ]
                    },
                    {
                        "name": "کفش",
                        "childs": [
                            {
                                "name": "پاشنه بلند",
                                "childs": [

                                ]
                            },
                            {
                                "name": "بوت و نیم بوت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "تخت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "ورزشی",
                                "childs": [

                                ]
                            },
                        ]
                    },
                ]
            },
            {
                "name": "آقایان",
                "childs": [
                    {
                        "name": "لباس",
                        "childs": [
                            {
                                "name": "کت و پالتو",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پیراهن",
                                "childs": [

                                ]
                            },
                            {
                                "name": "تی شرت و پلوشرت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "شلوار",
                                "childs": [

                                ]
                            },
                            {
                                "name": "بلوز و شلوارک",
                                "childs": [

                                ]
                            },
                            {
                                "name": "بافت و پلیور",
                                "childs": [

                                ]
                            },
                            {
                                "name": "سوئیشرت، کاپشن و ژاکت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "جلیقه و ژیله",
                                "childs": [

                                ]
                            },
                            {
                                "name": "لباس زیر",
                                "childs": [

                                ]
                            },
                        ]
                    },
                    {
                        "name": "کیف",
                        "childs": [
                            {
                                "name": "اداری",
                                "childs": [

                                ]
                            },
                            {
                                "name": "دستی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "دوشی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کوله",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پول",
                                "childs": [

                                ]
                            },
                        ]
                    },
                    {
                        "name": "کفش",
                        "childs": [
                            {
                                "name": "مجلسی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کالج",
                                "childs": [

                                ]
                            },
                            {
                                "name": "تخت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "بوت و نیم بوت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "ورزشی",
                                "childs": [

                                ]
                            },
                        ]
                    },
                ]
            },
            {
                "name": "کودکان",
                "childs": [
                    {
                        "name": "دخترانه",
                        "childs": [
                            {
                                "name": "بلوز، پیراهن و تونیک",
                                "childs": [

                                ]
                            },
                            {
                                "name": "تاپ و تیشرت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "شلوار و دامن",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کت، پالتو و کاپشن",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پلیور، ژاکت و سویشرت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "لباس راحتی",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کفش",
                                "childs": [

                                ]
                            },
                        ]
                    },
                    {
                        "name": "پسرانه",
                        "childs": [
                            {
                                "name": "پیراهن و تیشرت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "شلوار و شلوارک",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کت، پالتو و کاپشن",
                                "childs": [

                                ]
                            },
                            {
                                "name": "پلیور، ژاکت و سویشرت",
                                "childs": [

                                ]
                            },
                            {
                                "name": "لباس زیر",
                                "childs": [

                                ]
                            },
                            {
                                "name": "کفش",
                                "childs": [

                                ]
                            },
                        ]
                    },

                ]
            },
            {
                "name": "خردسالان",
                "childs": [
                    {
                        "name": "لباس",
                        "childs": [

                        ]
                    },
                    {
                        "name": "کفش",
                        "childs": [

                        ]
                    },
                ]
            },
            {
                "name": "لوازم جانبی",
                "childs": [
                    {
                        "name": "بانوان",
                        "childs": [

                        ]
                    },
                    {
                        "name": "آقایان",
                        "childs": [

                        ]
                    },
                    {
                        "name": "کودکان",
                        "childs": [

                        ]
                    },
                    {
                        "name": "خردسالان",
                        "childs": [

                        ]
                    },
                ]
            },
        ]
    },
    {
        "name": "زیبایی و سلامت",
        "childs": [
            {
                "name": "بانوان",
                "childs": [

                ]
            },
            {
                "name": "آقایان",
                "childs": [

                ]
            },
            {
                "name": "کودکان",
                "childs": [

                ]
            },
            {
                "name": "خردسالان",
                "childs": [

                ]
            },
            {
                "name": "لوازم شخصی",
                "childs": [

                ]
            },
            {
                "name": "لوازم جانبی",
                "childs": [

                ]
            },
        ]
    },
    {
        "name": "لوازم خانگی",
        "childs": [
            {
                "name": "الکترونیکی",
                "childs": [
                    {
                        "name": "لوازم پخت و پز",
                        "childs": [

                        ]
                    },
                    {
                        "name": "نوشیدنی ساز",
                        "childs": [

                        ]
                    },
                    {
                        "name": "خردکن و غذا ساز",
                        "childs": [

                        ]
                    },
                    {
                        "name": "لوازم مکمل آشپزخانه",
                        "childs": [

                        ]
                    },
                    {
                        "name": "لوازم شستوشو و نظافت",
                        "childs": [

                        ]
                    },
                    {
                        "name": "تهویه سرمایش و گرمایش",
                        "childs": [

                        ]
                    },
                    {
                        "name": "یخچال و فریزر",
                        "childs": [

                        ]
                    },
                ]
            },
            {
                "name": "آشپزخانه",
                "childs": [
                    {
                        "name": "تلویزیون",
                        "childs": [

                        ]
                    },
                    {
                        "name": "سینمای خانگی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "گیرنده دیجیتال DVB-T",
                        "childs": [

                        ]
                    },
                    {
                        "name": "پخش کننده موسیقی و ویدئو پرتال",
                        "childs": [

                        ]
                    },
                    {
                        "name": "سیستم شنیداری خانگی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "دیتا - ویدئو پروژکتور",
                        "childs": [

                        ]
                    },
                    {
                        "name": "ضبط کننده صدا",
                        "childs": [

                        ]
                    },
                    {
                        "name": "پخش کننده چند رسانه خانگی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "رادیو",
                        "childs": [

                        ]
                    },

                ]
            },
            {
                "name": "مبلمان",
                "childs": [

                ]
            },
            {
                "name": "اتاق خواب",
                "childs": [

                ]
            },
            {
                "name": "دکوری و تزئینی",
                "childs": [

                ]
            },
            {
                "name": "لوازم جانبی",
                "childs": [

                ]
            },
        ]
    },
    {
        "name": "اسباب بازی",
        "childs": [
            {
                "name": "آموزشی",
                "childs": [

                ]
            },
            {
                "name": "فکری",
                "childs": [

                ]
            },
            {
                "name": "لگو و ساختنی ها",
                "childs": [

                ]
            },
            {
                "name": "عروسک",
                "childs": [

                ]
            },
            {
                "name": "موزیکال",
                "childs": [

                ]
            },
            {
                "name": "سرگرمی",
                "childs": [

                ]
            },
            {
                "name": "ورزشی",
                "childs": [

                ]
            },
        ]
    },
    {
        "name": "کتاب و لوازم التحریر",
        "childs": [
            {
                "name": "لوازم التحریر",
                "childs": [
                    {
                        "name": "نوشت افزار",
                        "childs": [

                        ]
                    },
                    {
                        "name": "ابزار نقاشی و رنگ آمیزی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "ابزار طراحی و مهندسی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "لوازم اداری",
                        "childs": [

                        ]
                    },
                    {
                        "name": "دفتر و کاغذ",
                        "childs": [

                        ]
                    },
                    {
                        "name": "جامدادی و کیف",
                        "childs": [

                        ]
                    },
                ]
            },
            {
                "name": "کتاب",
                "childs": [
                    {
                        "name": "آموزشی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "علمی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "تاریخی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "هنر",
                        "childs": [

                        ]
                    },
                    {
                        "name": "رمان",
                        "childs": [

                        ]
                    },
                    {
                        "name": "کسب و کار",
                        "childs": [

                        ]
                    },
                    {
                        "name": "کودک",
                        "childs": [

                        ]
                    },
                    {
                        "name": "کتاب اکترونیک",
                        "childs": [

                        ]
                    },
                ]
            },
        ]
    },
    {
        "name": "لوازم جانبی",
        "childs": [
            {
                "name": "الکترونیک",
                "childs": [
                    {
                        "name": "تلویزیون و سینمای خانگی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "موسیقی و شنیداری",
                        "childs": [

                        ]
                    },
                    {
                        "name": "بهداشت و تناسب اندام",
                        "childs": [

                        ]
                    },
                    {
                        "name": "دوربین های عکاسی و فیلم برداری",
                        "childs": [

                        ]
                    },
                    {
                        "name": "کامپیوتر",
                        "childs": [

                        ]
                    },
                    {
                        "name": "تلفن و رادیو",
                        "childs": [

                        ]
                    },
                    {
                        "name": "لوازم الکترونیک ماشین و جی پی اس (GPS)",
                        "childs": [

                        ]
                    },
                    {
                        "name": "منزل و مراکز تجاری",
                        "childs": [

                        ]
                    },
                    {
                        "name": "بازی و اسباب بازی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "موارد دیگر",
                        "childs": [

                        ]
                    },
                ]
            },
            {
                "name": "باطری و برق",
                "childs": [
                    {
                        "name": "برق قابل حمل",
                        "childs": [

                        ]
                    },
                    {
                        "name": "باطری AA ، AAA ، C ، D ، 9V",
                        "childs": [

                        ]
                    },
                    {
                        "name": "باطری دوربین عکاسی و فیلم برداری",
                        "childs": [

                        ]
                    },
                    {
                        "name": "باطری تلفن بیسیم",
                        "childs": [

                        ]
                    },
                    {
                        "name": "باطری لپ تاپ",
                        "childs": [

                        ]
                    },
                    {
                        "name": "باطری موبایل",
                        "childs": [

                        ]
                    },
                    {
                        "name": "محافظ و کابل برق",
                        "childs": [

                        ]
                    },
                    {
                        "name": "برق و باطری مهندسی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "باطری یاب",
                        "childs": [

                        ]
                    },
                ]
            },
            {
                "name": "مصارف شخصی (DIY)",
                "childs": [
                    {
                        "name": "قطعات",
                        "childs": [

                        ]
                    },
                    {
                        "name": "اتصالات",
                        "childs": [

                        ]
                    },
                    {
                        "name": "مواد شیمیایی، نوار و چسب",
                        "childs": [

                        ]
                    },
                    {
                        "name": "فیوز",
                        "childs": [

                        ]
                    },
                    {
                        "name": "ال ای دی ها (LED)",
                        "childs": [

                        ]
                    },
                    {
                        "name": "لامپ و لامپهای مینیاتوری",
                        "childs": [

                        ]
                    },
                    {
                        "name": "برق خورشیدی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "سیم و تقسیم",
                        "childs": [

                        ]
                    },
                    {
                        "name": "رباتیک",
                        "childs": [

                        ]
                    },
                    {
                        "name": "آردوینو (Arduino)",
                        "childs": [

                        ]
                    },
                    {
                        "name": "چاپ سه بعدی",
                        "childs": [

                        ]
                    },
                    {
                        "name": "ابزار",
                        "childs": [

                        ]
                    },
                    {
                        "name": "کیت ها (Kits)",
                        "childs": [

                        ]
                    },
                ]
            },
        ]
    }
]

cursor.category.remove({})

category_order = 1
for cat in CAT_LIST:
    cat_temp = {
        "id": ID,
        "name": cat['name'],
        "order": category_order,
        "ancestors": [0, ],
        "url": "",
        "published": True,
        "ready_to_delete": False
    }
    cursor.category.insert_one(cat_temp)
    # print
    # print cat_temp['id']

    ID += 1
    category_order += 1

    group_order = 1
    group_ancestors = [cat_temp['id'], ]
    for group in cat['childs']:
        group_temp = {
            "id": ID,
            "name": group['name'],
            "order": group_order,
            "ancestors": group_ancestors,
            "url": "",
            "published": True,
            "ready_to_delete": False
        }
        cursor.category.insert_one(group_temp)
        ID += 1
        group_order += 1

        subgroup_order = 1
        subgroup_ancestors = group_ancestors[:]
        subgroup_ancestors.append(group_temp['id'])

        for subgroup in group['childs']:
            subgroup_temp = {
                "id": SUB_ID,
                "name": subgroup['name'],
                "order": subgroup_order,
                "ancestors": subgroup_ancestors,
                "url": "",
                "published": True,
                "ready_to_delete": False
            }
            cursor.category.insert_one(subgroup_temp)
            SUB_ID += 1
            subgroup_order += 1

            subsubgroup_order = 1
            subsubgroup_ancestors = subgroup_ancestors[:]
            subsubgroup_ancestors.append(subgroup_temp['id'])
            for subsubgroup in subgroup['childs']:
                subsubgroup_temp = {
                    "id": SUB_ID,
                    "name": subsubgroup['name'],
                    "order": subsubgroup_order,
                    "ancestors": subsubgroup_ancestors,
                    "url": "",
                    "published": True,
                    "ready_to_delete": False
                }
                cursor.category.insert_one(subsubgroup_temp)
                SUB_ID += 1
                subsubgroup_order += 1

cursor.counters.update_one({"_id": "category_id"}, {"$set": {"seq": ID}})
cursor.counters.update_one({"_id": "subgroup_id"}, {"$set": {"seq": SUB_ID}})
