# coding: utf-8
from django import forms

import logging

logger = logging.getLogger(__name__)

from core.db_mongo import cursor, get_next_sequence


class CategoryForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)

    category_name = forms.CharField(label='عنوان رسته ', required=False)
    category_publish = forms.ChoiceField(
        label='منتشر شود ',
        choices=(
            (False, 'انتخاب نمایید'),
            (False, u'خیر'),
            (True, u'بله'),
        ),
        widget=forms.Select(),
        required=False)
    category_type = forms.ChoiceField(
        label='نوع ',
        choices=(
            ('', 'انتخاب نمایید'),
            ('url', 'لینک/url'),
            ('main_menu', 'منوی اصلی'),
        ),
        widget=forms.Select(attrs={'onchange': 'toggle_category_link()'}),
        required=False)
    category_url = forms.URLField(
        max_length=255, required=False
    )
    category_order = forms.IntegerField(widget=forms.HiddenInput())

    def clean(self, *args, **kwargs):
        form_data = super(CategoryForm, self).clean()
        if not form_data.get('category_name'):
            self.add_error('category_name', 'فیلد عنوان رسته نباید خالی باشد.')
        if form_data.get('category_type') == 'url':
            if not form_data.get('category_url'):
                self.add_error('category_url', 'فیلد لینک نباید خالی باشد.')

        if form_data.get('category_publish') == 'True':
            form_data['category_publish'] = True
        else:
            form_data['category_publish'] = False

    def save(self):
        try:
            new_category = {
                'name': self.cleaned_data['category_name'],
                'id': get_next_sequence("category_id"),
                'order': self.cleaned_data['category_order'],
                'ancestors': [0],
                'published': self.cleaned_data['category_publish'],
                'ready_to_delete': False,
                'url': self.cleaned_data['category_url'],
            }
            cursor.category.insert_one(new_category.copy())
            # logger.debug(new_category)
            return ({'saved': True,
                     'message': 'Inserted successfully!',
                     'new_category': new_category})
        except Exception as e:
            logger.exception(e)
            return ({'saved': False, 'message': e})

    def update(self):
        new_category = {
            'published': self.cleaned_data['category_publish'],
            'name': self.cleaned_data['category_name'],
            'group_list': [],
            'order': self.cleaned_data['category_order'],
            'url': self.cleaned_data['category_url'],
        }
        try:
            cursor.category_list.update(
                {'order': new_category['order']},
                {"$set": {'name': new_category['name'],
                          'published': new_category['published'],
                          'url': new_category['url']}},
                upsert=False)
            return ({'saved': True, 'message': 'Updated successfully!'})
        except Exception as e:
            logger.exception(e)
            return ({'saved': False, 'message': e})
