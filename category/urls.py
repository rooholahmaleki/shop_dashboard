from django.conf.urls import url

from category.views import (
    CategoryIndex, CreateCategory,
    UpdateCategoryPublish,
    UpdateSubCategoryPublish,
    UpdateCategoryName,
    UpdateCategoryOrder,
    CreateSubCategory,
    CategoryDetail,
    CategoryDelete,
    SubcategoryDelete)

urlpatterns = [
    url(r'^$', CategoryIndex.as_view(), name='category_index'),
    url(r'^create_category/$', CreateCategory.as_view(), name="create_category_json"),
    url(r'^update_category_publish_json/$', UpdateCategoryPublish.as_view(), name="update_category_publish_json"),
    url(r'^update_subcategory_publish_json/$', UpdateSubCategoryPublish.as_view(), name="update_subcategory_publish_json"),
    url(r'^update_category_name_json/$',
        UpdateCategoryName.as_view(), name="update_category_name_json"),
    url(r'^update_category_order_json/$', UpdateCategoryOrder.as_view(), name="update_category_order_json"),
    url(r'^create_subcategory/$', CreateSubCategory.as_view(), name="create_subcategory"),
    url(r'^category_detail_json/(?P<category_id>\d+)/$', CategoryDetail.as_view(), name="category_detail_json"),
    url(r'^category_delete_json/$', CategoryDelete.as_view(), name="category_delete_json"),
    url(r'^subcategory_delete_json/$', SubcategoryDelete.as_view(), name="subcategory_delete_json"),
]
