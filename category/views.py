# coding: utf-8
import json
import logging

from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View
from django.http import JsonResponse
from collections import OrderedDict
from core.db_mongo import cursor, get_next_sequence
from category.forms import CategoryForm
from static.category_tree import update_final_all_category

logger = logging.getLogger(__name__)


def ajax_json_response(response, safe=True):
    return HttpResponse(
        JsonResponse(response, safe=safe),
        content_type="application/json",
    )


def get_category_tree(category_ids=None):
    criteria = {'ready_to_delete': False}
    if category_ids:
        if type(category_ids) == list:
            criteria['$or'] = [
                {'id': {'$in': category_ids}},
                {'ancestors': {'$in': category_ids}}
            ]
        else:
            criteria['$or'] = [
                {'id': category_ids},
                {'ancestors': category_ids}
            ]

    all_categories = list(cursor.category.find(
        criteria,
        {'name': 1, 'ancestors': 1, 'id': 1, 'order': 1, '_id': 0}
    ).sort('id', 1).sort('order', 1))

    for category in all_categories:
        category['id_parent'] = category['ancestors'][-1]

    category_tree = OrderedDict()
    parents = [cat for cat in all_categories if cat['ancestors'] == [0]]
    for parent in parents:
        childs = [
            cat for cat in all_categories if parent['id'] in cat['ancestors']]

        tree = OrderedDict()
        level_depth = 0
        childs_dict = {1: [], 2: [], 3: [], 4: []}

        for child in childs:
            level_depth = min(4, max(level_depth, len(child['ancestors'])))
            try:
                childs_dict[len(child['ancestors'])].append(child)
            except KeyError:
                pass  # max level is category>group>sub>sub>sub

        for i in range(level_depth - 1, 0, -1):
            for cat in childs_dict[i]:
                cat_and_ancestors = cat['ancestors'][:]
                cat_and_ancestors.append(cat['id'])
                for sub_cat in childs_dict[i + 1]:
                    if cat_and_ancestors == sub_cat['ancestors']:
                        try:
                            cat['childs'][sub_cat['id']] = sub_cat
                        except KeyError:
                            cat['childs'] = OrderedDict()
                            cat['childs'][sub_cat['id']] = sub_cat

        for cat in childs_dict[1]:
            tree[cat['id']] = cat
        parent['childs'] = tree
        category_tree[parent['id']] = parent

    return category_tree


class CategoryIndex(View):
    template_name = 'category/category_index.html'
    form_class = CategoryForm

    def get(self, request):
        # type: (object) -> object
        form = self.form_class()
        _category_list = list(cursor.category.find(
            {'ancestors': 0, 'ready_to_delete': False},
            {'_id': 0}
        ).sort('order', 1))

        context = {
            'category_list': _category_list,
            'category_list_json': json.dumps(_category_list),
            'form': form,
            'category_tree': json.dumps(get_category_tree()),
        }
        return render(request, self.template_name, context)


class CreateCategory(View):
    form_class = CategoryForm

    def post(self, request):
        bound_form = self.form_class(request.POST)
        if bound_form.is_valid():
            save_category = bound_form.save()
            if save_category['saved']:
                update_final_all_category()
                response = {
                    'result': 'Success',
                    'message': save_category['message'],
                    'category': json.dumps(save_category['new_category'])
                }
            else:
                response = {
                    'result': 'Failed',
                    'message': save_category['message'],
                }
        else:
            response = {
                'result': 'Failed',
                'message': bound_form.errors,
            }

        return ajax_json_response(response)


class BaseCategoryPublishUpdate(View):
    def update_category_publish(self, category_id, published):
        category_update = cursor.category.update_many(
            {
                '$or': [
                    {'id': category_id},
                    {'ancestors': {'$in': [category_id]}},
                ]
            },
            {"$set": {'published': published}},
        )

        if not category_update.modified_count:
            response = {
                'result': 'Failed',
                'message': 'No change!',
            }
            return response
        update_final_all_category()
        product_update = cursor.product.update_many(
            {'categories': category_id},
            {'$set': {'category_published': published}},
        )

        if not product_update.raw_result['ok']:
            logger.debug('query failed:\r\n' + product_update.raw_result)
            response = {
                'result': 'Failed',
                'message': 'product query failed!',
            }
            return response

        response = {
            'result': 'Success',
            'message': 'Successfully updated',
            'published': 'منتشر شده' if published else 'منتشر نشده',
        }

        return response


class UpdateCategoryPublish(BaseCategoryPublishUpdate):
    def post(self, request):

        try:
            category_id = int(request.POST['id'])
            published = True if request.POST['published'] == 'true' else False
        except KeyError:
            response = {
                'result': 'Failed',
                'message': 'Invalid Data!',
            }
            return ajax_json_response(response)

        response = self.update_category_publish(category_id, published)
        return ajax_json_response(response)


class UpdateSubCategoryPublish(BaseCategoryPublishUpdate):
    def post(self, request):

        try:
            category_id = int(request.POST['id'])
            published = True if request.POST['published'] == 'true' else False
            root_category_id = int(request.POST['root_category_id'])
        except KeyError:
            return ajax_json_response(
                {'result': 'Failed', 'message': 'Invalid Data!'}
            )

        sub_cat = cursor.category.find_one({'id': category_id})
        if not sub_cat:
            return ajax_json_response(
                {'result': 'Failed', 'message': 'Invalid Data!'}
            )

        parent_id = sub_cat['ancestors'][-1]
        bad_condition = cursor.category.count(
            {'id': parent_id, 'published': False})

        if bad_condition:
            return ajax_json_response(
                {
                    'result': 'Failed',
                    'message': 'Invalid condition. parent must be published'
                }
            )

        response = self.update_category_publish(category_id, published)
        if response['result'] == 'Success':
            response = {
                'result': 'Success',
                'message': 'Successfully updated',
                'published': 'منتشر شده' if published else 'منتشر نشده',
                'tree': json.dumps(get_category_tree(root_category_id)),
            }
            response.update(category_list(root_category_id))

        return ajax_json_response(response)


class UpdateCategoryName(View):
    def post(self, request):

        try:
            category_id = int(request.POST['id'])
            name = request.POST['name']
        except Exception:
            response = {
                'result': 'Failed',
                'message': 'Invalid Data!',
            }
            return ajax_json_response(response)

        category_update = cursor.category.update_one(
            {'id': category_id}, {"$set": {'name': name}}
        )

        if category_update.modified_count:
            response = {
                'result': 'Success',
                'message': 'Successfully inserted',
                'name': name,
            }
            update_final_all_category()
        else:
            response = {
                'result': 'Failed',
                'message': 'Not Found',
            }

        return ajax_json_response(response)


def category_list(ancestors):
    try:
        category = list(cursor.category.find(
            {
                'ancestors': ancestors,
                'ready_to_delete': False
            },
            {'_id': 0}
        ).sort('id', 1).sort('order', 1))

        return {
            'groups': [
                group for group in category if len(group['ancestors']) == 1],
            'subgroups': [
                subgroup for subgroup in category if len(subgroup['ancestors']) == 2],
            'subsubgroups': [
                subgroup for subgroup in category if len(subgroup['ancestors']) == 3],
            'subsubsubgroups': [
                subgroup for subgroup in category if len(subgroup['ancestors']) == 4],
        }
    except:
        return None


class CategoryDetail(View):
    def get(self, request, category_id):
        category_id = int(category_id)

        category = category_list(category_id)
        if category:
            response = {
                'result': 'Success',
                'tree': json.dumps(get_category_tree(category_id)),
            }
            response.update(category_list(category_id))
        else:
            response = None
        return ajax_json_response(response, safe=False)


class CreateSubCategory(View):
    def update_category(self, category_sequence):
        response = {}
        new_category_sequence = []
        try:
            for category in category_sequence:
                cursor.category.update_one(
                    {'id': category['id']},
                    {
                        '$set': {
                            'order': category['order'],
                            'ancestors': category['ancestors']
                        }
                    }
                )
                new_category_sequence.push(category)
            update_final_all_category()
            response['result'] = 'Success'
            response['message'] = 'Updated successfully'
            response['category'] = new_category_sequence
            return response
        except Exception as e:
            response['result'] = 'Failed'
            response['message'] = e
            return response

    def post(self, request):

        try:
            parent_id = int(request.POST['parent_id'])
            name = request.POST['name']
        except Exception:
            response = {
                'result': 'Failed',
                'message': 'Either name or parent_id not defined!'
            }
            return ajax_json_response(response)

        parent_ancestors = cursor.category.find_one(
            {'id': parent_id},
            {'ancestors': 1, '_id': 0}
        )['ancestors']

        if not parent_ancestors:
            response = {'result': 'Failed', 'message': 'Invalid request!'}
            return ajax_json_response(response)

        ancestors = list()
        if 0 in parent_ancestors:
            ancestors.append(parent_id)
            new_category_id = get_next_sequence('category_id')
        else:
            ancestors += parent_ancestors
            ancestors.append(parent_id)
            new_category_id = get_next_sequence('subgroup_id')
        new_category = {
            'name': name,
            'id': new_category_id,
            'ancestors': ancestors,
            'order': cursor.category.count(
                {
                    'ancestors': {'$eq': ancestors},
                    'published': True,
                    'ready_to_delete': False
                }) + 1,
            'url': '',
            'published': False,
            'ready_to_delete': False
        }

        category = cursor.category.insert_one(new_category.copy())

        if not category:
            return ajax_json_response(None)

        update_final_all_category()
        response = {
            'message': 'Successfully inserted!',
            'new_category': new_category,
            'result': 'Success',
            'tree': json.dumps(get_category_tree(new_category['ancestors'][0])),
        }
        response.update(category_list((new_category['ancestors'][0])))

        return ajax_json_response(response)


class CategoryDelete(View):
    def update_categories_order(self, order):
        try:
            update_query = cursor.category.update_many(
                {'ancestors': {'$eq': [0]},
                 'order': {'$gt': order}},
                {'$inc': {'order': -1}})
            if update_query.raw_result['ok']:
                update_final_all_category()
                return True
            return False
        except Exception as e:
            logger.debug(e)
            return False

    def update_categories(self, category_id):
        update_query = cursor.category.update_many(
            {'$or': [{'id': category_id}, {'ancestors': category_id}]},
            {'$set': {
                'ready_to_delete': True,
                'published': False,
                'order': None,
            }}
        )
        if not update_query.modified_count:
            logger.debug(update_query.raw_result)
            return False

        update_final_all_category()
        return True

    def update_products(self, category_id):

        product_update = cursor.product.update_many(
            {'categories': category_id, '$isolated': 1},
            {'$set': {
                'category_published': False,
                'ready_to_delete': True,
            }}
        )

        if not product_update.raw_result['ok']:
            logger.debug(product_update.raw_result)
            return False

        return True

    def post(self, request):
        try:
            category_id = int(request.POST['category_id'])
            order = int(request.POST['order'])
        except Exception as e:
            logger.debug(e)
            response = {'result': 'Failed', 'message': 'Invalid data'}
            return ajax_json_response(response)

        failed_response = {'result': 'Failed', 'message': 'query failed'}
        if not self.update_products(category_id):
            return ajax_json_response(failed_response)
        if not self.update_categories(category_id):
            return ajax_json_response(failed_response)
        if not self.update_categories_order(order):
            return ajax_json_response(failed_response)

        category_list = list(
            cursor.category.find(
                {'ancestors': {'$all': [0], '$size': 1}, 'ready_to_delete': False},
                {'_id': 0}
            ).sort('order', 1)
        )
        response = {'result': 'Success', 'category_list': category_list}

        return ajax_json_response(response)


def log_failed_query(**kwargs):
    log = 'Query Failed'
    for k, v in kwargs.iteritems():
        log += '\n' + k + ': ' + str(v)
    logger.debug(log)


class SubcategoryDelete(View):
    def update_products(self, subgroups_ids):
        criteria = {'categories': {'$in': subgroups_ids}}
        update_query = {'$set': {
            'category_published': False,
            'ready_to_delete': True
        }}
        product_update = cursor.product.update_many(criteria, update_query)

        if not product_update.raw_result['ok']:
            log_failed_query(
                criteria=criteria,
                update_query=update_query,
                query_raw_result=product_update.raw_result
            )
            return False

        return True

    def update_categories(self, category_id):
        criteria = {'id': category_id}
        update_query = {
            '$set': {
                'ready_to_delete': True,
                'published': False,
                'order': None
            }
        }
        target_category = cursor.category.find_one_and_update(criteria, update_query)
        if not target_category:
            log_failed_query(criteria=criteria, update_query=update_query)
            return False

        criteria = {
            'ancestors': {'$eq': target_category['ancestors']},
            'order': {'$gt': target_category['order']}
        }
        update_query = {'$inc': {'order': -1}}
        order_update_query = cursor.category.update_many(criteria, update_query)
        if not order_update_query.raw_result['ok']:
            log_failed_query(criteria=criteria, update_query=update_query)
            return False

        criteria = {'ancestors': category_id}
        update_query = {'$set': {'ready_to_delete': True, 'published': False}}
        categories_update = cursor.category.update_many(criteria, update_query)
        if not categories_update:
            log_failed_query(criteria=criteria, update_query=update_query)
            return False

        update_final_all_category()
        return True

    def post(self, request):

        try:
            category_id = int(request.POST['subcategory_id'])
            root_category_id = int(request.POST['root_category_id'])
        except Exception as e:
            logger.debug(e)
            response = {'result': 'Failed', 'message': 'Invalid data'}
            return ajax_json_response(response)

        subgroups = list(cursor.category.find(
            {'ancestors': category_id},
            {'_id': 0, 'id': 1})
        )
        subgroups_ids = [cat['id'] for cat in subgroups] if subgroups else []
        subgroups_ids.append(category_id)

        failed_response = {'result': 'Failed', 'message': 'query failed'}
        if not self.update_products(subgroups_ids):
            return ajax_json_response(failed_response)
        if not self.update_categories(category_id):
            return ajax_json_response(failed_response)

        response = {
            'result': 'Success',
            'tree': json.dumps(get_category_tree(root_category_id)),
        }
        response.update(category_list(root_category_id))

        return ajax_json_response(response, safe=False)


class UpdateCategoryOrder(View):
    def update_categories(self, target_category, new_order):

        criteria = {
            'ancestors': {'$eq': target_category['ancestors']},
            'published': True,
            'ready_to_delete': False
        }

        if target_category['order'] < new_order:
            criteria['order'] = {
                '$gt': target_category['order'],
                '$lte': new_order
            }
            update_query = {'$inc': {'order': -1}}
        elif target_category['order'] > new_order:
            criteria['order'] = {
                '$lt': target_category['order'],
                '$gte': new_order
            }
            update_query = {'$inc': {'order': 1}}
        else:
            return (
                {'result': False, 'message': {'result': 'Failed', 'message': 'Repeatitive Data'}}
            )

        categories_update = cursor.category.update_many(criteria, update_query)

        if not categories_update.modified_count:
            log_failed_query(
                criteria=criteria,
                update_query=update_query,
                query_raw_result=categories_update.raw_result
            )
            return (
                {'result': False, 'message': {'result': 'Failed', 'message': 'Query Failed'}}
            )

        update_final_all_category()
        return {'result': True}

    def post(self, request):
        try:
            category_id = int(request.POST['category_id'])
            new_order = int(request.POST['new_order'])
            root_category_id = int(request.POST['root_category_id'])
        except Exception as e:
            logger.debug(e)
            response = {'result': 'Failed', 'message': 'invalid data'}
            return ajax_json_response(response)

        criteria = {'id': category_id}
        target_category = cursor.category.find_one(criteria)

        if not target_category:
            log_failed_query(criteria=criteria)
            response = {'result': 'Failed', 'message': 'Category Does Not Exist'}
            return ajax_json_response(response)

        update_cats_query = self.update_categories(target_category, new_order)

        if not update_cats_query['result']:
            return ajax_json_response(update_cats_query['message'])

        criteria = {'id': category_id}
        update_query = {'$set': {'order': new_order}}
        target_category_update = cursor.category.update_one(criteria, update_query)
        if not target_category_update.modified_count:
            log_failed_query(
                criteria=criteria,
                update_query=update_query,
                query_raw_result=target_category_update.raw_result
            )
            response = {'result': 'Failed', 'message': 'Query Failed'}
            return ajax_json_response(response)

        response = {
            'result': 'Success',
            'tree': json.dumps(get_category_tree(root_category_id)),
        }
        response.update(category_list(root_category_id))
        update_final_all_category()

        return ajax_json_response(response, safe=False)

# ------for ipdb breake point
# from ipdb import set_trace; set_trace()
