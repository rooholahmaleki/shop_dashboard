# coding: utf-8
from django import forms


class ColorForm(forms.Form):
    presentation = forms.CharField(
        label='نام',
        error_messages={'required': 'فیلد نام خالی می باشد.'}
    )

    code = forms.CharField(
        label='رنگ',
        error_messages={'required': 'فیلد رنگ خالی می باشد.'},
        widget=forms.TextInput(attrs={'readonly': 'readonly'})
    )

    def __init__(self, *args, **kwargs):
        super(ColorForm, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        form_data = super(ColorForm, self).clean()
        form_data['code'] = form_data["code"][1:]
        return form_data
