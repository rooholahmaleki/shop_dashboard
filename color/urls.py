from django.conf.urls import url
from color.views import color_list, add_color, delete_color, edit_color


urlpatterns = [
    url(r'^$', color_list, name='color_list'),
    url(r'^add/$', add_color, name='add_color'),
    url(r'^delete/$', delete_color, name='delete_color'),
    url(r'^edit/(?P<color_id>.+)$', edit_color, name='edit_color'),
]
