import datetime

from bson import ObjectId
from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from color.forms import ColorForm
from core.db_mongo import cursor


def color_list(request):
    colors = list(cursor.colors.find())

    for color in colors:
        color['id'] = str(color.pop('_id'))

    args = {
        'colors': colors
    }
    return render(request, 'color/colors.html', args)


def add_color(request):
    message = ""
    if request.method != "POST":
        form = ColorForm()
    else:
        form = ColorForm(request.POST)
        if form.is_valid():
            form_data = form.cleaned_data
            if not cursor.colors.find_one({'code': form_data['code']}):
                form_data['created_date'] = datetime.datetime.now()
                cursor.colors.insert_one(form_data)
                return HttpResponseRedirect(reverse(color_list))
            else:
                message = "Duplicate_Key"

    return render(request, 'color/add.html', {'form': form, 'message_error': message})


def delete_color(request):
    cursor.colors.delete_one({'_id': ObjectId(request.POST['color_id'])})
    return JsonResponse({})


def edit_color(request, color_id):
    message = ""
    if request.method != "POST":
        color = cursor.colors.find_one({'_id': ObjectId(color_id)})
        form = ColorForm(color)
    else:
        form = ColorForm(request.POST)
        if form.is_valid():
            form_data = {'presentation': form.cleaned_data['presentation']}
            result = cursor.colors.update_one(
                {'_id': ObjectId(color_id)},
                {'$set': form_data}
            ).modified_count

            if result:
                return HttpResponseRedirect(reverse(color_list))
            else:
                message = "no_update"

    return render(request, 'color/edit.html', {'form': form, "message": message})

