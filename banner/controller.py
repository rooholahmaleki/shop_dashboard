from PIL import Image
from django.conf import settings


def save_image(image_file, image_name):
    image_address_django = '/media' + settings.BANNER_IMAGE + str(image_name) + ".jpg"
    image = settings.MEDIA_ROOT + settings.BANNER_IMAGE + str(image_name) + ".jpg"
    img = Image.open(image_file)
    # img = img.resize((img.size[0], 24), Image.ANTIALIAS)
    img.save(image, quality=100)

    return image_address_django
