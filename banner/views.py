import datetime

from django.shortcuts import render
from django.views.decorators.http import require_POST
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, JsonResponse

from banner.forms import BannerForm
from banner.controller import save_image
from core.db_mongo import cursor
from bson.json_util import ObjectId


def banner_list(request):
    banners = list(cursor.banner.find())
    for banner in banners:
        banner['id'] = str(banner.pop('_id'))
    return render(request, 'banner/list.html', {'banners': banners})


def banner_add(request):
    if request.method != "POST":
        form = BannerForm()
    else:
        form = BannerForm(request.POST)
        if form.is_valid():
            form_data = form.cleaned_data

            form_data['image'] = save_image(
                request.FILES['image'],
                datetime.datetime.now().strftime('%Y%M%d%H%M%S')
            )
            form_data['created_date'] = datetime.datetime.now()
            cursor.banner.insert_one(form_data)

            return HttpResponseRedirect(reverse(banner_list))

    return render(request, 'banner/add.html', {'form': form})


def banner_edit(request, banner_id):
    if request.method != "POST":
        banner = cursor.banner.find_one({'_id': ObjectId(banner_id)})
        banner['id'] = str(banner.pop('_id'))

        form = BannerForm(banner)
    else:
        form = BannerForm(request.POST)
        if form.is_valid():
            form_data = form.cleaned_data

            if 'image' in request.FILES:
                form_data['image'] = save_image(
                    request.FILES['image'],
                    datetime.datetime.now().strftime('%Y%M%d%H%M%S')
                )
            else:
                form_data.pop('image', None)

            cursor.banner.update_one(
                {'_id': ObjectId(banner_id)},
                {'$set': form_data}
            )
            return HttpResponseRedirect(reverse(banner_list))

    return render(request, 'banner/edit.html', {'form': form, 'image': banner['image']})


@require_POST
def banner_delete(request):
    result = cursor.banner.delete_one(
        {'_id': ObjectId(request.POST['banner_id'])}
    ).deleted_count

    return JsonResponse({'result': result})
