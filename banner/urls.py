from django.conf.urls import url

from banner import views

urlpatterns = [
    url(r'^$', views.banner_list, name='banner_list'),
    url(r'^add/$', views.banner_add, name='banner_add'),
    url(r'^edit/(?P<banner_id>.+)$', views.banner_edit, name='banner_edit'),
    url(r'^delete$', views.banner_delete, name='banner_delete'),
]
