# coding: utf-8
from django import forms


class BannerForm(forms.Form):
    name = forms.CharField(
        label='نام',
        error_messages={'required': 'فیلد نام خالی می باشد.'},
    )
    link = forms.CharField(
        label='لینک',
        error_messages={'required': 'فیلد لینک خالی می باشد.'},
    )
    color = forms.CharField(
        label='رنگ',
        error_messages={'required': 'فیلد رنگ خالی می باشد.'},
    )
    image = forms.ImageField(
        label='انتخاب عکس',
        required=False,
    )

    def __init__(self, *args, **kwargs):
        self.image_optional = kwargs.pop('image_optional', True)

        super(BannerForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(BannerForm, self).clean()
        if self.image_optional is False and not cleaned_data.get('image', None):
            self.add_error('image', 'فیلد عکس خالی می باشد.')
