import datetime

from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from controller import SampleFormController
from bson.json_util import loads, dumps
from forms import TechnicalInfo, type_key
from category.static.category_tree import category_tree
from product.controller import CategoryController, get_products_by_criteria_projection
from update_product_filters import ChangeTecInfoProduct


def sample_forms(request):
    _sample_forms = list(SampleFormController().get_all_sample_form())
    for item in _sample_forms:
        item['categories'] = list(CategoryController().get_by_list_id(item['categories']))

    args = {
        'sample_forms': _sample_forms
    }
    return render(request, 'sample_form/forms.html', args)


def add_sample_form(request):
    if request.method == "POST":
        technical_info = loads(request.POST['technical_info'])
        searchable_keys = get_searchable_keys(technical_info)
        update = request.POST['update']
        id_sub_group = int(request.POST['id_sub_group'])

        data = {
            'id_sub_group': id_sub_group,
            'technical_info': technical_info
        }

        find_sample_form = SampleFormController().find_sample_form(id_sub_group)
        if update != "update":
            if not find_sample_form:
                categories = sorted(list(CategoryController().get_by_id(id_sub_group)['ancestors']))
                categories.append(id_sub_group)
                data['categories'] = categories

                data['searchable_keys'] = searchable_keys
                data['created_date'] = datetime.datetime.now()
                SampleFormController().insert_sample_form(data)
                message = 'insert'
            else:
                message = 'repeat_error'
        else:
            change_filter_key = request.POST['change_filter_key']
            data['modified_date'] = datetime.datetime.now()
            if change_filter_key == "changed":
                data['searchable_keys'] = return_searchable_key(searchable_keys, find_sample_form['searchable_keys'])
                SampleFormController().insert_bg_sf_job(id_sub_group)

            SampleFormController().update_sample_form(data)
            message = 'update'

        return HttpResponse(
            JsonResponse({'technical_info': technical_info, 'message': message, 'id': id_sub_group}),
            content_type='application/json')

    else:
        args = {
            'category_tree': dumps(category_tree()),
            'form':  TechnicalInfo()
        }
        return render(request, 'sample_form/add_sample_form.html', args)


def return_searchable_key(new_searchable_key, old_searchable_key):
    for new in new_searchable_key:
        for old in old_searchable_key:
            if old['key'] == new['key']:
                new['value'] = old['value']
    return new_searchable_key


def update_products_filters(technical_info, id_group, searchable_keys):
    products = get_products_by_criteria_projection({'categories': {'$in': [id_group]}}, {'technical_info': 1})
    for product in products:
        ChangeTecInfoProduct(product=product, s_technical_info=technical_info, searchable_keys=searchable_keys)

    return technical_info


def get_searchable_keys(technical_info):
    searchable_keys = []
    for item in technical_info:
        for sub in item['list_technical_sub']:
            if sub['searchable']:
                searchable_keys.append({
                    'key': sub['title_en'],
                    'presentation': sub['title'],
                    'type': sub['type_key'],
                    'value': []
                })
    return searchable_keys


def edit_sample_form(request, id_sub_group):
    sample_form = SampleFormController().find_sample_form(int(id_sub_group))

    if sample_form:
        sample_form['categories'] = list(CategoryController().get_by_list_id(sample_form['categories']))
        args = {
            'form': TechnicalInfo(),
            'type_key': type_key,
            'sample_form': sample_form
        }
        return render(request, 'sample_form/add_sample_form.html', args)
    else:
        return redirect(sample_forms)


def delete_sample_form(request):
    id_sub_group = int(request.POST['id_sub_group'])
    SampleFormController().delete_sample_form(id_sub_group)
    return HttpResponse(
        JsonResponse({}),
        content_type='application/json')

