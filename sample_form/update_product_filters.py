from product.controller import get_products_by_criteria_projection
from controller import SampleFormController

PUBLISHED = 'published'

ID_SUB_GROUP = 'id_sub_group'
FILTERS = 'filters'
CATEGORIES = 'categories'
SEARCHABLE_KEYS = 'searchable_keys'
TITLE_EN = 'title_en'
SEARCHABLE = 'searchable'
PRODUCT_ID = 'product_id'
DESCRIPTION = 'description'
DESCRIPTION_EN = 'description_en'
TECHNICAL_INFO = 'technical_info'
LIST_TECHNICAL_SUB = 'list_technical_sub'


class ChangeTecInfoProduct(object):

    def __init__(self, product, s_technical_info, searchable_keys):
        self.filters = {}
        self.technical_info = product.get(TECHNICAL_INFO, [])
        self.product_id = product[PRODUCT_ID]
        self.last_group = product[CATEGORIES][-1]
        self.s_technical_info = s_technical_info
        self.searchable_keys = searchable_keys
        self.run()

    def run(self):
        self.apply_product_filters()
        update = rpc_call.main.product.update_product(
            {PRODUCT_ID: self.product_id},
            {
                "$set": {
                    TECHNICAL_INFO: self.s_technical_info,
                    FILTERS: self.filters
                }
            }
        )
        if update:
            update_values_searchable_key(self.filters, self.last_group, self.searchable_keys)

    def apply_product_filters(self):
        for info_parent in self.technical_info:
            for s_info_parent in self.s_technical_info:
                self.product_equality_check_info(info_parent, s_info_parent)

    def product_equality_check_info(self, info_parent, s_info_parent):
        if info_parent[TITLE_EN] == s_info_parent[TITLE_EN]:
            for info_sub in info_parent[LIST_TECHNICAL_SUB]:
                for s_info_sub in s_info_parent[LIST_TECHNICAL_SUB]:
                    self.product_equality_check_sub_info(info_sub, s_info_sub)

    def product_equality_check_sub_info(self, info_sub, s_info_sub):
        if info_sub[TITLE_EN] == s_info_sub[TITLE_EN]:
            s_info_sub[DESCRIPTION] = info_sub[DESCRIPTION]
            s_info_sub[DESCRIPTION_EN] = info_sub[DESCRIPTION_EN]
            if s_info_sub[SEARCHABLE]:
                self.filters[info_sub[TITLE_EN]] = info_sub[DESCRIPTION_EN]


def update_values_searchable_key(filters, id_sub_group, searchable_keys=None):
    if searchable_keys is None:
        searchable_keys = SampleFormController().find_sample_form(id_sub_group)
        if searchable_keys is not None:
            searchable_keys = searchable_keys[SEARCHABLE_KEYS]
        else:
            searchable_keys = []

    bool_update = False
    for searchable_key in searchable_keys:
        if searchable_key['key'] in filters:
            if filters[searchable_key['key']] not in searchable_key['value']:
                if searchable_key['type'] == "int":
                    try:
                        searchable_key['value'].append(int(filters[searchable_key['key']]))
                    except:
                        pass
                else:
                    searchable_key['value'].append(filters[searchable_key['key']])
                bool_update = True
    if bool_update:
        SampleFormController().update_sample_form({SEARCHABLE_KEYS: searchable_keys, ID_SUB_GROUP: id_sub_group})


def update_require_sub_group(id_sub_group):
    products = get_products_by_criteria_projection({CATEGORIES: {"$in": [id_sub_group]}, PUBLISHED: True}, {TECHNICAL_INFO: 1, PRODUCT_ID: 1, CATEGORIES: 1})
    sample_form = SampleFormController().find_sample_form(id_sub_group)
    if sample_form and products:
        clean_searchable_key_value(sample_form[SEARCHABLE_KEYS])
        for product in products:
            ChangeTecInfoProduct(
                product=product,
                s_technical_info=sample_form[TECHNICAL_INFO],
                searchable_keys=sample_form[SEARCHABLE_KEYS]
            )


def clean_searchable_key_value(searchable_keys):
    for item in searchable_keys:
        item['value'] = []


if __name__ == "__main__":
    # group_ids = SampleFormController().get_all_bg_sf_job()
    group_ids = SampleFormController().get_all_sample_form()
    for group_id in group_ids:
        # update_require_sub_group(group_id['id'])
        # print group_id['id']
        update_require_sub_group(group_id['id_sub_group'])
