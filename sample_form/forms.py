# coding: utf-8
from django import forms


type_key = (
    ('string', u'عدد نیست'),
    ('int', u'عدد')
)


class TechnicalInfo(forms.Form):
    def __init__(self, *args, **kwargs):
        super(TechnicalInfo, self).__init__(*args, **kwargs)

    title = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': "عنوان جدول"}),
    )
    title_en = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'کلید جدول به انگلیسی'}),
    )
    sub_title = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': "عنوان زیرگروه"}),
    )
    sub_title_en = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'کلید'}),
    )
    sub_description = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': "توضیحات"}),
    )
    sub_description_en = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'مقدار قابل جستجو'}),
    )
    type_key = forms.ChoiceField(
        widget=forms.Select(attrs={'class': 'combo-large sample_form'}),
        choices=type_key,
    )
    searchable = forms.BooleanField(
        label="آیا قابل جستجو می باشد؟",
    )


