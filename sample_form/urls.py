from django.conf.urls import url
from sample_form.views import sample_forms, add_sample_form, delete_sample_form, edit_sample_form

urlpatterns = [
    url(r'^$', sample_forms, name='sample_forms'),
    url(r'^add/$', add_sample_form, name='add_sample_form'),
    url(r'^delete/$', delete_sample_form, name='delete_sample_form'),
    url(r'^edit/(?P<id_sub_group>\d+)$', edit_sample_form, name='edit_sample_form'),
]
