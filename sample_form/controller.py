from core.db_mongo import cursor
import datetime


class SampleFormController(object):
    def __init__(self):
        self.db = cursor.sample_form
        self.bg_sf_job = cursor.bg_sf_job

    def get_all_sample_form(self):
        return self.db.find()

    def get_all_sample_form_projection(self, projection):
        return self.db.find({}, projection)

    def insert_sample_form(self, data):
        self.db.insert(data)

    def find_sample_form(self, id_sub_group):
        return self.db.find_one({'id_sub_group': id_sub_group})

    def update_sample_form(self, data):
        return self.db.update_one({'id_sub_group': data['id_sub_group']}, {"$set": {item: data[item] for item in data}})

    def delete_sample_form(self, id_sub_group):
        return self.db.remove({'id_sub_group': id_sub_group})

    def insert_bg_sf_job(self, id_group):
        self.bg_sf_job.update(
            {'id': id_group},
            {"$set": {'modify_date': datetime.datetime.now(), 'id': id_group}},
            upsert=True
        )

    def get_all_bg_sf_job(self):
        return self.bg_sf_job.find()
