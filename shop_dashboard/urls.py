from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static
from rest_framework.urlpatterns import format_suffix_patterns


urlpatterns = [
    url(r'^category/', include('category.urls')),
    url(r'^products/', include('product.urls')),
    url(r'^brand/', include('brand.urls')),
    url(r'^factor/', include('factor.urls')),
    url(r'^form/', include('sample_form.urls')),
    url(r'^users/', include('users.urls')),
    url(r'^acl/', include('acl.urls')),
    url(r'^amazing_offer/', include('amazing_offer.urls')),
    url(r'^banner/', include('banner.urls')),
    url(r'^color/', include('color.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

api_pattern = format_suffix_patterns([
    url(r'^rest-api/v1/', include('rest_api.urls')),
])

urlpatterns = urlpatterns + api_pattern + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
