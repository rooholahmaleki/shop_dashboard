from bson import ObjectId
from core.db_mongo import cursor


def processor(request):
    context = {}

    if request.user.is_authenticated():
        context['user'] = cursor.users.find_one({
            '_id': ObjectId(request.user.userBox.mongo_id)
        })

    return context
