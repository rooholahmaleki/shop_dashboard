import logging
import re

from django.http import HttpResponseRedirect
from django.conf import settings
from django.http import HttpResponseForbidden
from django.utils.deprecation import MiddlewareMixin
from core.db_mongo import cursor

logger = logging.getLogger(__name__)


class LoginRequiredMiddleware(MiddlewareMixin):
    def process_request(self, request):
        if not request.user.is_authenticated():
            EXEMPT_URLS = [re.compile(settings.LOGIN_URL.lstrip('/'))]
            path = request.path_info.lstrip('/')

            if not path.startswith('rest-api/'):
                if hasattr(settings, 'LOGIN_EXEMPT_URLS'):
                    EXEMPT_URLS += [re.compile(expr) for expr in settings.LOGIN_EXEMPT_URLS]

                if not any(m.match(path) for m in EXEMPT_URLS):
                    return HttpResponseRedirect(settings.LOGIN_URL)


class ACLMiddleware(MiddlewareMixin):
    def process_view(self, request, view_func, view_args, view_kwargs):
        if not request.user.is_authenticated():
            return

        if view_func.func_name in getattr(settings, 'ACL_EXEMPT_VIEWS', set()):
            return

        group_name = request.user.userBox.group_name
        acl = cursor.acl_group.find_one({'group_name': group_name})

        if not (acl and view_func.func_name in acl.get('views', [])):
            return HttpResponseForbidden("Forbidden")
