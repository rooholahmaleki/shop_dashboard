import os
import raven

import datetime
import logging.config

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = '-@-zkrxx6bclw6o+@z^%ug8$wz22y263gjzn1r%hhlw2l_%ubd'

DEBUG = True

ALLOWED_HOSTS = ["37.59.158.251", "127.0.0.1"]
LOGIN_REDIRECT_URL = '/'

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'rest_framework.authtoken',
    'raven.contrib.django.raven_compat',
    'users'
]
JWT_AUTH = {
    'JWT_ENCODE_HANDLER':
        'rest_framework_jwt.utils.jwt_encode_handler',

    'JWT_DECODE_HANDLER':
        'rest_framework_jwt.utils.jwt_decode_handler',

    'JWT_PAYLOAD_HANDLER':
        'rest_framework_jwt.utils.jwt_payload_handler',

    'JWT_PAYLOAD_GET_USER_ID_HANDLER':
        'rest_framework_jwt.utils.jwt_get_user_id_from_payload_handler',

    'JWT_RESPONSE_PAYLOAD_HANDLER':
        'rest_framework_jwt.utils.jwt_response_payload_handler',

    'JWT_SECRET_KEY': SECRET_KEY,
    'JWT_GET_USER_SECRET_KEY': None,
    'JWT_PUBLIC_KEY': None,
    'JWT_PRIVATE_KEY': None,
    'JWT_ALGORITHM': 'HS256',
    'JWT_VERIFY': True,
    'JWT_VERIFY_EXPIRATION': True,
    'JWT_LEEWAY': 0,
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=7),
    'JWT_AUDIENCE': None,
    'JWT_ISSUER': None,

    'JWT_ALLOW_REFRESH': True,
    'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=17),

    'JWT_AUTH_HEADER_PREFIX': 'JWT',
    'JWT_AUTH_COOKIE': None,
}

REST_FRAMEWORK = {
    'UNICODE_JSON': False,
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    )
}

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'request_logging.middleware.LoggingMiddleware',
    'shop_dashboard.middleware.LoginRequiredMiddleware',
    'shop_dashboard.middleware.ACLMiddleware',
]

ROOT_URLCONF = 'shop_dashboard.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'builtins': ['core.templatetags.general_template'],
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'shop_dashboard.context_processor.processor',
            ],
        },
    },
]
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'panda',
        'USER': 'root',
        # 'PASSWORD': '123_moholah@',
        'PASSWORD': 'Benyamin@Sadegh@2020',
        'HOST': '127.0.0.1',  # Or an IP Address that your DB is hosted on
        'PORT': '3306',
    }
}

MONGO = {
    'HOST': '127.0.0.1',
    'PORT': 27017,
    'USER': 'mohollah',
    'PASS': '32mO_Db25'
}

LANGUAGE_CODE = 'fa-IR'

TIME_ZONE = 'Asia/Tehran'

USE_I18N = True

USE_L10N = True

USE_TZ = True

AUTHENTICATION_BACKENDS = ['core.auth_backend.ShopBackend']

STATIC_URL = '/static/'
# STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATICFILES_DIRS = (os.path.join(BASE_DIR, "static"),)

MEDIA_URL = '/media/'
MEDIA_DIRS = (os.path.join(BASE_DIR, "media"),)
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
BLL_ROOT = os.path.join(BASE_DIR, 'rrrrrrrrr')
LOGS_ROOT = os.path.join(BASE_DIR, 'logs')

DATA_UPLOAD_MAX_NUMBER_FIELDS = None
DATA_UPLOAD_MAX_MEMORY_SIZE = None

PRODUCT_GENERAL_CRITERIA = {
    'trash': False,
    'category_published': True,
    'published': True,
}

BRAND_IMAGE = '/brand/'

BANNER_IMAGE = '/banner/'

AMAZING_OFFER = '/amazing_offer/'

DEFAULT_IMAGE_TEXTURE = '/media/TextureDefault.jpg'

BRAND_GENERAL_CRITERIA = {'block': False}

LOGIN_URL = '/users/login/'

ACL_EXEMPT_VIEWS = {
    'login',
    'logout',
    'home',
    'serve',
    'about_us',
}

SMS_SECRET = {
    "UserApiKey": "bb4d4e461dcf2e0c9db9281f",
    "SecretKey": "m0550091742%^&*()r"
}

LINE_NUMBERS = [
    "3000483151",
    "50002015159485",
]

SMS_MESSAGED_URL = "http://RestfulSms.com/api/MessageSend"
SMS_TOKEN_URL = "http://RestfulSms.com/api/Token"

SIMILAR_THRESHOLD = 0.5

BRAND_GENERAL_PROJECTION = {
    'brand_id': 1,
    'fa_name': 1,
    'en_name': 1,
    'image': 1,
    '_id': 0,
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
        'file': {
            'class': 'logging.FileHandler',
            'filename': LOGS_ROOT + '/{}.log'.format(datetime.datetime.now().strftime('%Y_%m_%d')),
        },
        'sentry': {
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
        },

    },
    'loggers': {
        'panda_api': {
            'handlers': ['file', 'sentry'],
            'level': 'DEBUG',  # change debug level as appropiate
            'propagate': False,
        },
        'django.request': {
            'handlers': ['file', 'sentry'],
            'level': 'DEBUG',  # change debug level as appropiate
            'propagate': False,
        },
        'raven': {
            'level': 'DEBUG',
            'handlers': ['sentry'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'DEBUG',
            'handlers': ['sentry'],
            'propagate': False,
        },
    },
}

RAVEN_CONFIG = {
    'dsn': 'https://e7ec8f4c88de46fdaba1d48fcf0811eb:e4bdd5481eb049fda8d45e320784d9b3@sentry.io/271203',
}

logging.config.dictConfig(LOGGING)
logger_api = logging.getLogger("panda_api")

