# coding: utf-8

from django import forms
from product.statics import (
    YES_OR_NO,
    PUBLISHED,
    PRODUCT_PER_PAGE,
    EXIST_PRODUCT,
    EXIST_PRODUCT_FILTERS,
    GENDER,
    COUNTRY_CHOICE_DICT,
    COLOR_STATUS,
    UNIT_TYPE)


class ProductForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)

    product_id = forms.IntegerField(
        label='کد محصول',
        widget=forms.TextInput(attrs={'readonly': 'readonly'})
    )
    product_name_fa = forms.CharField(
        label='نام محصول به فارسی',
        error_messages={'required': 'فیلد نام نباید خالی باشد.'},
    )
    product_name_en = forms.CharField(
        label='نام محصول به انگلیسی',
        error_messages={'required': 'فیلد نام لاتین نباید خالی باشد.'},
    )
    national_code = forms.ChoiceField(
        label='کشور سازنده',
        choices=COUNTRY_CHOICE_DICT.items(),
        widget=forms.Select(attrs={'class': 'select_search'}),
        required=False
    )
    gender = forms.ChoiceField(
        label='جنسیت',
        choices=GENDER,
        widget=forms.Select(attrs={}),
        required=False
    )

    brand = forms.CharField(
        label='برند:',
        widget=forms.Select(attrs={'class': 'select_search'}),
        required=False
    )
    exist = forms.ChoiceField(
        label='وضعیت',
        choices=EXIST_PRODUCT,
        widget=forms.Select(attrs={}),
        required=False
    )

    general_specifications = forms.CharField(
        widget=forms.Textarea,
        required=False
    )
    gift = forms.CharField(
        label='کد محصول مورد نظر را وارد نمایید',
        widget=forms.TextInput(attrs={'type': "text"}),
        required=False
    )
    advantages = forms.CharField(
        label='نقاط قوت',
        widget=forms.TextInput(attrs={'class': "advantages"}),
        required=False
    )
    disadvantages = forms.CharField(
        label='نقاط ضعف',
        widget=forms.TextInput(attrs={'class': "disadvantages"}),
        required=False
    )
    color_status = forms.ChoiceField(
        label="نوع رنگ بندی خود را انتخاب نمایید:",
        choices=COLOR_STATUS,
        required=False,
        widget=forms.RadioSelect(attrs={'onclick': "changeColorStatus()"})
    )

    def clean(self, *args, **kwargs):
        form_data = super(ProductForm, self).clean()
        extra_field = (
            'csrfmiddlewaretoken',
            'gallery_image',
            'tags',
            'list_advantages',
            'list_disadvantages',
            'color',
            "product_id"
        )
        int_field = ('exist', 'brand', 'gender')

        form_data_clean = {}
        for field in form_data:
            if field in extra_field or form_data.get(field) == "" or form_data.get(field) is None:
                continue

            if field in int_field:
                form_data_clean[field] = int(form_data.get(field))
                continue
            form_data_clean[field] = form_data.get(field)

        if 'general_specifications' not in form_data_clean:
            form_data_clean['general_specifications'] = ""

        self.cleaned_data = form_data_clean


class GeneralProductForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(GeneralProductForm, self).__init__(*args, **kwargs)

    product_id = forms.IntegerField(
        label='کد محصول',
        widget=forms.TextInput(attrs={'readonly': 'readonly'})
    )
    product_name_fa = forms.CharField(
        label='نام محصول به فارسی',
        error_messages={'required': 'فیلد نام نباید خالی باشد.'},
    )
    product_name_en = forms.CharField(
        label='نام محصول به انگلیسی',
        error_messages={'required': 'فیلد نام لاتین نباید خالی باشد.'},
    )
    national_code = forms.ChoiceField(
        label='کشور سازنده',
        choices=COUNTRY_CHOICE_DICT.items(),
        widget=forms.Select(attrs={'class': 'select_search'}),
        required=False
    )
    gender = forms.ChoiceField(
        label='جنسیت',
        choices=GENDER,
        widget=forms.Select(attrs={}),
        required=False
    )

    brand = forms.CharField(
        label='برند:',
        widget=forms.Select(attrs={'class': 'select_search'}),
        required=False
    )
    exist = forms.ChoiceField(
        label='وضعیت',
        choices=EXIST_PRODUCT,
        widget=forms.Select(attrs={}),
        required=False
    )

    sell = forms.IntegerField(
        label='قیمت فروش',
        widget=forms.TextInput(attrs={'type': "text"}),
        error_messages={'required': 'قیمت فروش را وارد کنید.'},
        required=False
    )
    buy = forms.IntegerField(
        label='قیمت خرید',
        widget=forms.TextInput(),
        error_messages={'required': 'قیمت خرید را وارد کنید.'},
        required=False
    )
    unit = forms.FloatField(
        label='واحد',
        widget=forms.TextInput(),
        error_messages={'required': 'واحد را وارد کنید.'},
        required=True
    )
    unit_type = forms.ChoiceField(
        label='نوع',
        widget=forms.Select(attrs={}),
        choices=UNIT_TYPE,
        error_messages={'required': 'نوع واحد را وارد کنید.'},
        required=True
    )

    def clean(self, *args, **kwargs):
        form_data = super(GeneralProductForm, self).clean()
        if form_data.get('exist') == '1':
            if form_data.get('sell') and not isinstance(form_data.get('sell'), int):
                self.add_error("sell", "قیمت فروش درست وارد نشده است.")

            if form_data.get('buy') and not isinstance(form_data.get('buy'), int):
                self.add_error("buy", "قیمت خرید درست وارد نشده است.")

        extra_field = ('csrfmiddlewaretoken', "product_id")
        int_field = ('sell', 'buy', 'exist', 'brand', 'gender')
        float_field = ('unit',)

        form_data_clean = {}
        for field in form_data:
            if field in extra_field or form_data.get(field) == "" or form_data.get(field) is None:
                continue

            if field in int_field:
                form_data_clean[field] = int(form_data.get(field))
                continue

            if field in float_field:
                form_data_clean[field] = float(form_data.get(field))

            form_data_clean[field] = form_data.get(field)

        self.cleaned_data = form_data_clean


class FilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(FilterForm, self).__init__(*args, **kwargs)

    exist = forms.ChoiceField(
        choices=EXIST_PRODUCT_FILTERS,
        label="وضعیت محصول",
        widget=forms.Select(attrs={'onchange': 'paginator(1)'})
    )
    published = forms.ChoiceField(
        choices=PUBLISHED,
        label="وضعیت انتشار",
        widget=forms.Select(attrs={'onchange': 'paginator(1)'})
    )
    product_per_page = forms.ChoiceField(
        choices=PRODUCT_PER_PAGE,
        widget=forms.Select(attrs={'onchange': 'paginator(1)'})
    )
    min_price = forms.IntegerField(
        widget=forms.TextInput(attrs={'onchange': 'paginator(1)', 'placeholder': "کمترین قیمت"})
    )
    max_price = forms.IntegerField(
        widget=forms.TextInput(attrs={'onchange': 'paginator(1)', 'placeholder': "بیشترین قیمت"})
    )
    search = forms.CharField(
        widget=forms.TextInput(attrs={'onchange': 'paginator(1)', 'placeholder': 'جستجوی محصول ...'})
    )
    trash = forms.CharField(widget=forms.HiddenInput())


class PriceProductForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(PriceProductForm, self).__init__(*args, **kwargs)

    product_id = forms.CharField(
        widget=forms.HiddenInput(),
        error_messages={'required': 'مودال راببندید و دوباره باز کنید.'},
    )

    sell = forms.IntegerField(
        label='قیمت فروش',
        widget=forms.TextInput(attrs={'type': "text", 'class': 'uk-float-right'}),
        error_messages={'required': 'قیمت فروش را وارد کنید.'},
    )
    buy = forms.IntegerField(
        label='قیمت خرید',
        widget=forms.TextInput(attrs={'type': "text", 'class': 'uk-float-right'}),
        error_messages={'required': 'قیمت خرید را وارد کنید.'},
    )
    old_sell = forms.IntegerField(
        widget=forms.HiddenInput(),
        required=False
    )
    old_buy = forms.IntegerField(
        widget=forms.HiddenInput(),
        required=False
    )
