# coding: utf-8
import os
import json
import shutil
import logging
import datetime
import jdatetime
import base64
import uuid

from core import get_pool
from bson import json_util
from random import randint
from product import controller
from core.utils import deflate, inflate
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST, require_GET
from product.forms import ProductForm, FilterForm, GeneralProductForm, PriceProductForm
from sample_form.update_product_filters import update_values_searchable_key
from category.static.category_tree import category_tree
from brand.controller import get_all_brand_by_category_group
from sample_form.controller import SampleFormController
from django.conf import settings
from core.db_mongo import cursor
from pymongo import ReturnDocument
from bson.json_util import loads
from PIL import Image

logger = logging.getLogger(__name__)


@require_POST
def pagination_product(request):
    """
        get field(step,per_page,field for query) from user and create query.
        send parameter to function paginator and get result
    """
    data = request.POST
    step, per_page, trash = int(data['step']), int(data['product_per_page']), eval(data['trash_type'])
    query = create_query_from_product(data)
    products = controller.paginator(per_page, step, query)
    products['result_product'] = change_some_field_from_product(products['result_product'])
    return HttpResponse(
        json.dumps(products, default=json_util.default),
        content_type="application/json"
    )


def change_some_field_from_product(products):
    list_result_product = []
    for item in products:
        created_date = jdatetime.datetime.fromgregorian(
            year=item['created_date'].year,
            day=item['created_date'].day,
            month=item['created_date'].month
        )
        modified_date = jdatetime.datetime.fromgregorian(
            year=item['modified_date'].year,
            day=item['modified_date'].day,
            month=item['modified_date'].month
        )
        item['created_date'] = str(created_date.year) + "/" + str(created_date.month) + "/" + str(created_date.day)
        item['modified_date'] = str(modified_date.year) + "/" + str(modified_date.month) + "/" + str(modified_date.day)
        item['categories'] = list(controller.CategoryController().get_by_list_id(item['categories']))
        list_result_product.append(item)
    return list_result_product


def create_query_from_product(data):
    query = {}
    dict_bool = {'False': False, 'True': True}
    if data['exist'] != "None":
        query['exist'] = int(data['exist'])
    if data['published']:
        query['published'] = dict_bool[data['published']]

    range_price = {}
    if data['min_price']:
        range_price['$gte'] = int(data['min_price'])
    if data['max_price']:
        range_price['$lte'] = int(data['max_price'])
    if bool(range_price):
        query['price.0.sell'] = range_price

    if data['search_word']:
        query['$text'] = {
            '$search': data['search_word']
        }
    query['trash'] = dict_bool[data['trash_type']]
    return query


@csrf_exempt
def product_list(request):
    """
        created FilterForm and render products.html
    """
    filter_form = FilterForm(initial={'trash': 'False'})
    count_products = controller.get_count_products({
        'trash': False,
        'ready_to_delete': False
    })
    count_products_published = controller.get_count_products_published()
    count_products_no_published = controller.get_count_products_no_published()
    return render(request, 'product/products.html', {
        'filter_form': filter_form,
        'price_form': PriceProductForm(),
        'count_products': count_products,
        'count_products_published': count_products_published,
        'count_products_no_published': count_products_no_published
    })


@require_GET
def update_product(request, product_id):
    """
        return product to edit or return form to crated product
    """
    colors = list(cursor.colors.find())

    for color in colors:
        color['id'] = str(color.pop('_id'))

    args = {
        'product_id': product_id,
        'category_tree': json_util.dumps(category_tree()),
        'all_color': colors,
        'update_product': False
    }
    criteria = {'product_id': product_id}
    product = controller.find_one_product(criteria)
    if product:
        set_info_of_product_to_session(request, product)
        args.update(return_args_from_product(product))

    else:
        return redirect(product_list)

    return render(request, 'product/update_content_product.html', args)


def create_product(request, product_id):
    args = {}
    criteria = {'product_id': product_id}
    product = controller.find_one_product(criteria)
    if product:
        set_info_of_product_to_session(request, product)
        args.update(return_args_from_supply_unit_product(product))

    else:
        if request.session.get('product_id', None) == product_id:
            request.session['update_product'] = False
            args.update(return_args_of_new_product(request))
        else:
            return redirect(product_list)

    colors = list(cursor.colors.find())

    for color in colors:
        color['id'] = str(color.pop('_id'))

    args["update_product"] = request.session['update_product']
    args["all_color"] = colors
    return render(request, 'product/create_product.html', args)


def create_json_product(request):
    form = GeneralProductForm(request.POST)
    if 'product_id' in request.session:
        product_id = request.session['product_id']
        if form.is_valid():
            new_product = form.cleaned_data

            if 'colors' in request.POST:
                new_product['colors'] = append_colors_product(json_util.loads(request.POST['colors']))

            if not request.session['update_product']:
                message = action_create_product(request, new_product)

            else:
                criteria = {'product_id': product_id}
                projection = {'product_id': 1, 'price': 1, "colors": 1}
                old_product = controller.find_one_product(criteria, projection)

                color = check_color_change(product_id, new_product.get("colors", []), old_product.get("colors", []))

                if color:
                    add_history_price_from_product(json_util.loads(request.POST['price_change']), new_product, old_product)
                    message = action_update_product(product_id, new_product, old_product)

                else:
                    message = "color_error"
        else:
            message = 'form_error'
    else:
        message = 'session_error'

    return HttpResponse(
        json.dumps({'result': form.errors, 'message': message}),
        content_type="application/json"
    )


def check_color_change(product_id, colors_new, colors_old):
    list_old = set()
    list_new = set()
    for new in colors_new:
        list_new.add(new["code"])

    for old in colors_old:
        list_old.add(old["code"])

    subscription = list(list_new.symmetric_difference(list_old))

    if subscription:
        criteria = {
            "product_id": product_id,
            "color": {"$in": subscription},
            "marketable": {"$gt": 0}
        }
        stock = cursor.stock.find_one(criteria)
        if stock:
            return False
        return True
    return True


def append_colors_product(colors):
    if colors:
        for color in colors:
            color['image'] = settings.DEFAULT_IMAGE_TEXTURE
            color['secondary_presentation'] = "None"
            color['secondary_code'] = "None"
            color['id'] = randint(1000, 9999)
        return colors
    else:
        return []


def edit_color_product(request):
    product_id = request.session.get('product_id')
    code = request.POST['code']
    _id = request.POST['id']
    args = {
        'secondary_presentation': request.POST['secondary_presentation'],
        'secondary_code': request.POST['secondary_code'],
    }
    criteria = {
        'product_id': product_id,
        'colors.code': code
    }
    update = {"$set": {}}

    if args['secondary_code'] != "None" and args['secondary_presentation'] != "None":
        update = {
            "$set": {
                "colors.$.secondary_code": args['secondary_code'],
                "colors.$.secondary_presentation": args['secondary_presentation'],
            }
        }

    if 'image' in request.POST:
        image_name = _id + '.jpg'
        address_texture = settings.BASE_DIR + '/media/product/' + product_id + '/texture/'

        if not os.path.exists(address_texture):
            os.makedirs(address_texture)

        with open(address_texture + image_name, 'wb') as a_image:
            image_convert = base64.b64decode(request.POST['image'].split(',')[1])
            a_image.write(image_convert)
        image_dict = {'colors.$.image': '/media/product/' + product_id + '/texture/' + image_name}
        update['$set'].update(image_dict)

    criteria['$set']['modified_date'] = datetime.datetime.now()
    _update = cursor.product.find_one_and_update(
        criteria,
        update,
        return_document=ReturnDocument.AFTER
    )

    if _update:
        message = 'success'
    else:
        message = 'no_update'

    return HttpResponse(
        json.dumps({'message': message}),
        content_type="application/json"
    )


def return_args_of_new_product(request):
    args = {}
    product_id = request.session['product_id']
    args['product_id'] = product_id
    initial = {
        'product_id': product_id,
    }

    args['form'] = GeneralProductForm(initial=initial)
    category_group = product_id[0:4]
    args['brands'] = get_all_brand_by_category_group(category_group)

    return args


def return_args_from_product(product):
    product_id = product['product_id']
    args = {
        'update_product': True,
        'advantages': product.get('advantages', None),
        'disadvantages': product.get('disadvantages', None),
        'product_brand': product.get('brand', None),
        'published': product.get('published', False),
        'product_colors': product.get('colors', []),
        'galleries': json_util.dumps(product['galleries']),
        'other_sub_categories': list(controller.CategoryController().get_by_list_id(product.get('other_sub_categories', []))),
        'has_galleries': True if product['galleries'] else False,
    }
    category_group = product_id[0:4]
    args['brands'] = get_all_brand_by_category_group(category_group)
    args['technical_info'] = apply_product_filters(product['technical_info'], product['categories'][-1])
    dict_initial = return_dict_initial_product(product, args)
    args['form'] = ProductForm(initial={item: dict_initial[item] for item in dict_initial})

    return args


def return_args_from_supply_unit_product(product):
    args = {
        'product_brand': product.get('brand'),
    }
    product_id = product['product_id']
    category_group = product_id[0:4]
    args['brands'] = get_all_brand_by_category_group(category_group)
    dict_initial = return_dict_initial_product(product, args)
    args['form'] = GeneralProductForm(initial={item: dict_initial[item] for item in dict_initial})
    args['product_colors'] = product.get('colors', [])
    args['product_id'] = product_id
    return args


def apply_product_filters(technical_info, last_group):
    try:
        s_technical_info = SampleFormController().find_sample_form(last_group)['technical_info']
        for info_parent in technical_info:
            for s_info_parent in s_technical_info:
                product_equality_check_info(info_parent, s_info_parent)
        return s_technical_info
    except:
        return []


def product_equality_check_info(info_parent, s_info_parent):
    if info_parent['title_en'] == s_info_parent['title_en']:
        for info_sub in info_parent['list_technical_sub']:
            for s_info_sub in s_info_parent['list_technical_sub']:
                product_equality_check_sub_info(info_sub, s_info_sub)


def product_equality_check_sub_info(info_sub, s_info_sub):
    if info_sub['title_en'] == s_info_sub['title_en']:
        s_info_sub['v_description'] = info_sub['description']
        s_info_sub['v_description_en'] = info_sub['description_en']


def set_info_of_product_to_session(request, product):
    request.session['product_id'] = product['product_id']
    request.session['update_product'] = True


def return_dict_initial_product(product, args):
    dict_initial = {}
    for key in product:
        if key in args:
            continue

        if key == 'price':
            if product['price']:
                dict_initial['buy'] = product['price'][0]['buy']
                dict_initial['sell'] = product['price'][0]['sell']
                continue

        dict_initial[key] = product[key]

    return dict_initial


@require_POST
def update_json_product(request):
    """
        create product or update product
    """
    result = {}
    product_id = request.session.get("product_id", False)
    if product_id:
        form = ProductForm(request.POST)
        if form.is_valid():
            new_product = form.cleaned_data
            set_tech = set_technical_info_to_product(request, new_product)
            if not set_tech:
                message = 'technical_error'

            else:
                add_some_data_from_product(request, new_product)

                criteria = {'product_id': product_id}
                projection = {
                    'product_id': 1,
                    'categories': 1,
                    'amazing_offer': 1,
                    'price': 1,
                    'published': 1,
                    # 'material': 1,
                    'exist': 1
                }
                old_product = controller.find_one_product(criteria, projection)

                message = action_update_product(product_id, new_product, old_product)

                if message == "update":
                    if 'gallery_image' in request.POST:
                        # get_pool.add_task(insert_galleries, request.POST['gallery_image'], product_id)
                        insert_galleries(request.POST['gallery_image'], product_id)

        else:
            result = form.errors
            message = 'form_error'
    else:
        message = 'session_error'

    return HttpResponse(
        json.dumps({'message': message, 'result': result}),
        content_type="application/json"
    )


def clear_all_sub_description(technical_info):
    for item in technical_info:
        for sub in item['list_technical_sub']:
            sub['description'] = ""
            sub['description_en'] = ""
    return technical_info


def action_update_product(product_id, new_product, old_product):
    query = {'$set': {p: new_product[p] for p in new_product}}
    update = cursor.product.update_one(
        {'product_id': product_id},
        query,
    )

    if update:
        if old_product.get('published', False) and new_product.get('filters', False):
            last_group = old_product['categories'][-1]
            update_values_searchable_key(new_product['filters'], last_group)

        return 'update'
    else:
        return 'error'


def action_create_product(request, new_product):
    set_price_to_product(new_product)
    new_product.update({
        'categories': request.session['categories'],
        'product_id': request.session['product_id']
    })
    set_default_data(new_product)
    result = cursor.product.insert_one(new_product)
    if result:
        request.session['update_product'] = True
        return 'insert'
    else:
        return 'error'


def set_default_data(product):
    now = datetime.datetime.now()
    product.update({
        'modified_date': now,
        'created_date': now,
        'discount_maker': 0,
        'rate': 0,
        'trash': False,
        'rate_list': {},
        'category_published': True,
        'published': False,
        'technical_info': [],
        'galleries': [],
        'amazing_offer': {'active': False},
        'rate_percent': {'1': 0, '2': 0, '3': 0, '4': 0, '5': 0}
    })


def set_price_to_product(new_product):
    try:
        dict_price, customer_price = get_price_of_product(int(new_product.pop('sell')), int(new_product.pop('buy')))
        new_product.update({
            'price': [dict_price],
            'customer_price': customer_price,
        })
    except:
        new_product.update({
            'price': [],
            'customer_price': 0,
        })


def add_history_price_from_product(price_change, new_product, old_product):
    if price_change and 'buy' in new_product and 'sell' in new_product:
        dict_price, customer_price = get_price_of_product(int(new_product.pop('sell')), int(new_product.pop('buy')))
        if old_product:
            amazing_offer = old_product.get('amazing_offer', {}).get('active', False)
            if not amazing_offer:
                new_product['customer_price'] = customer_price
            price = old_product['price']
        else:
            price = []
        price.insert(0, dict_price)
        new_product['price'] = price


def get_price_of_product(sell, buy):
    dict_price = {
        'buy': buy,
        'sell': sell,
        'customer_price': sell,
        'created_date': datetime.datetime.now()
    }
    return dict_price, sell


def add_some_data_from_product(request, new_product):
    new_product.update({
        'advantages': json_util.loads(request.POST['list_advantages']),
        'disadvantages': json_util.loads(request.POST['list_disadvantages']),
        'other_sub_categories': list(set(json_util.loads(request.POST['categories']))),
    })
    return new_product


def set_technical_info_to_product(request, new_product):
    if 'technical_info' in request.POST:
        technical_info = json_util.loads(request.POST['technical_info'])
        new_product['technical_info'] = technical_info
        result = get_search_technical_info(technical_info)
        if result:
            keywords, new_product['filters'], list_tags = result['keywords'], result['filters'], result['list_tags']
            # new_product['keywords'] = request.POST['product_name_fa'] + ' ' + request.POST['product_name_en'] + keywords
            new_product['keywords'] = keywords
            list_tags.extend([request.POST['product_name_fa'], request.POST['product_name_en']])
            new_product['tags'] = list_tags
            return True
        else:
            return False
    return True


def upload_images_rpc(img_data, product_id):
    address = '/media/zip_image/' + product_id + '.so'
    address_use = settings.BASE_DIR + '/' + address

    with open(address_use, "w") as galleries:
        galleries.write(deflate(img_data))


def get_search_technical_info(technical_info):
    keywords = ""
    filters = {}
    list_tags = []
    for item in technical_info:
        for sub in item['list_technical_sub']:
            if sub['description_en'] != "" and sub['description'] != "":
                list_tags.extend([sub['description_en'], sub['description']])

            if sub['searchable']:
                keywords += sub['description'] + " " + sub['description_en'] + " "
                try:
                    if sub['type_key'] == 'int':
                        sub['description_en'] = int(sub['description_en'])
                except:
                    return False
                filters[sub['title_en']] = sub['description_en']

    return {
        "keywords": keywords,
        "filters": filters,
        "list_tags": list_tags,
    }


def return_system_tags_product(subgroup, category, group, name_fa, name_en):
    system_tags = [name_fa, name_en]
    if subgroup != 'no_has_sub_group':
        system_tags_1 = return_tags_product_ancestors(subgroup)
    else:
        name_cat = controller.CategoryController().get_by_id(category)['name']
        name_group = controller.CategoryController().get_by_id(group)['name']
        system_tags_1 = [name_cat, name_group]

    system_tags.extend(system_tags_1)
    return system_tags


def return_tags_product_ancestors(last_sub_group):
    sub = controller.CategoryController().get_by_id(int(last_sub_group))
    tags = [sub['name']]
    for parent in sub['ancestors']:
        tags.append(controller.CategoryController().get_by_id(parent)['name'])
    return tags


@require_POST
def delete_product(request):
    """
        delete product by id
    """
    product_id = request.POST['product_id']
    controller.remove_by_product_id(product_id)
    address = settings.MEDIA_ROOT + '/product/' + product_id
    if os.path.exists(address):
        shutil.rmtree(address)
    return HttpResponse(
        json.dumps({"result": ""}),
        content_type="application/json"
    )


def product_trashed(request):
    """
        created FilterForm and render productstrash.html
    """
    filter_form = FilterForm(initial={'trash': 'True'})
    return render(request, 'product/productstrash.html', {'filter_form': filter_form})


@require_POST
def change_product_to_trash(request):
    """
        Change the product of not trash to the trash or inverse
    """
    product_id = request.POST['product_id']
    trash_bool = eval(request.POST['trash_type'])
    update = controller.change_status_trash_product(product_id, trash_bool)

    message = 'error'
    if update:
        message = 'success'

    return HttpResponse(
        json.dumps({"message": message}),
        content_type="application/json"
    )


@require_POST
def load_group(request):
    """
        return group of collections
    """
    category_id = int(request.POST['category'])
    result = controller.CategoryController().get_group_from_category_list(category_id)
    return HttpResponse(
        json.dumps({'result': result}),
        content_type="application/json"
    )


def create_product_id(request):
    """
        created id for product
    """
    if request.method == 'POST':
        response = {}
        try:
            last_group = int(json_util.loads(request.POST['id_last_group']))
            ancestors = sorted(controller.CategoryController().get_by_id(last_group)['ancestors'])
            if ancestors:
                ancestors.append(last_group)
                request.session['categories'] = ancestors
                product_id = generate_product_id(ancestors[0], ancestors[1])
                request.session['product_id'] = product_id
                response['product_id'] = product_id
                response['message'] = 'success'

            else:
                response['message'] = 'error'

        except:
            response['message'] = 'error'

        return HttpResponse(
            json.dumps(response),
            content_type="application/json"
        )
    else:
        args = {
            'category_tree': json_util.dumps(category_tree()),
        }
        return render(request, 'product/createvpid.html', args)


def generate_product_id(category, group):
    product_id = 0
    category = generate_double_digits(category)
    group = generate_double_digits(group)
    product = True
    while product:
        product_id = category + group + str(randint(100000, 999999))
        criteria = {'product_id': product_id}
        projection = {'_id': 1}
        product = controller.find_one_product(criteria, projection)
    return str(product_id)


def generate_double_digits(integer):
    integer = str(integer)
    if len(integer) == 1:
        integer = "0" + integer
    elif len(integer) > 2:
        logger.debug('id category or group great 2')
    return integer


def load_gallery_to_show(request):
    gallery = json_util.loads(request.POST['gallery'])
    gallery = controller.show_gallery(gallery)
    return HttpResponse(
        json.dumps({'gallery': gallery}),
        content_type="application/json"
    )


def delete_gallery(request):
    message = 'error'

    if 'product_id' in request.session:
        product_id = request.session['product_id']
        gallery_id = request.POST['gallery_id']
        count_gallery = controller.count_gallery_product(product_id)

        if count_gallery > 1 or count_gallery == 'None':
            color_code = controller.delete_gallery_by_id(product_id, gallery_id)
            delete_gallery_file_image(product_id, color_code)
            message = 'success'

        else:
            message = 'empty_gallery'

    return HttpResponse(
        json.dumps(message),
        content_type="application/json"
    )


def delete_gallery_file_image(product_id, color_gallery):
    if color_gallery != 'None':
        address_gallery = settings.BASE_DIR + '/media/product/' + product_id + '/gallery/' + color_gallery
        shutil.rmtree(address_gallery)
    return


def change_main_gallery(request):
    message = 'error'
    if 'product_id' in request.session:
        product_id = request.session['product_id']
        gallery_id = request.POST['gallery_id']
        result = controller.change_main_gallery_query(product_id, gallery_id)
        if result:
            message = 'success'
        else:
            message = 'error'
    return HttpResponse(
        json.dumps(message),
        content_type="application/json"
    )


def change_product_published(request):
    product_id = request.POST['product_id']
    status = request.POST['status']
    product_update, errors = controller.change_status_product(product_id, status)
    if product_update:
        message = 'success'
        if status == 'true':
            last_group = product_update['categories'][-1]
            if product_update.get('filters', False):
                update_values_searchable_key(product_update['filters'], last_group)
    else:
        message = 'schema'

    return HttpResponse(
        json.dumps({'message': message, 'errors': errors}),
        content_type="application/json"
    )


def find_one_product_by_id(request):
    response = {}
    criteria = {
        'product_id': request.POST['product_id'],
        "published": True
    }
    projection = {
        '_id': 0,
        'product_name_fa': 1,
        'product_name_en': 1,
        'price': 1,
        'product_id': 1,
        'image': 1
    }
    product = controller.find_one_product(criteria, projection)
    if product:
        try:
            product['price'] = product['price'][0]
        except:
            product['price'] = None

        response['message'] = "success"
    else:
        response['message'] = "not_found"

    response['product'] = product
    return HttpResponse(
        json.dumps(response, default=json_util.default),
        content_type="application/json"
    )


def save_image(image_file, address, name):
    image_address = settings.MEDIA_ROOT + '/' + address
    if not os.path.exists(image_address):
        os.makedirs(image_address)

    with open(image_address + name, 'wb+') as destination:
        for chunk in image_file.chunks():
            destination.write(chunk)


def insert_galleries(galleries, product_id):
    galleries = loads(galleries)

    def save_image_g(size, image_data, path):
        img = Image.open(image_data)
        img = img.resize(size, Image.ANTIALIAS)
        img.save(path)

    result = []
    global_address = settings.BASE_DIR + '/media/product/' + product_id + '/gallery/'
    format_image = '.png'
    thumbnail = 'thumbnail_'
    main_image_address = ''

    if os.path.exists(global_address):
        shutil.rmtree(global_address)

    for gallery in galleries:
        dict_image = {}
        list_image = []
        address_new = global_address + gallery['color'] + '/'
        dict_image['color'] = gallery['color']
        dict_image['main_gallery'] = gallery['main_gallery']
        dict_image['id'] = str(uuid.uuid4())
        address_mongo = '/media/product/' + product_id + '/gallery/' + gallery['color'] + '/'

        if not os.path.exists(address_new):
            os.makedirs(address_new)

        for image in gallery['list_image']:
            id_image = str(uuid.uuid4())

            with open(address_new + 'large_' + id_image + format_image, 'wb+') as file_image_large:
                image_convert_large = base64.b64decode(image[0])
                file_image_large.write(image_convert_large)
                small_address = address_new + 'small_' + id_image + format_image
                large_address = address_new + 'large_' + id_image + format_image
                save_image_g((387, 419), file_image_large, small_address)
                save_image_g((1512, 1676), file_image_large, large_address)

            with open(address_new + thumbnail + '150x170_' + id_image + format_image, 'wb+') as file_thumbnail:
                image_convert_large = base64.b64decode(image[1])
                file_thumbnail.write(image_convert_large)
                address_150x170 = address_new + thumbnail + '150x170_' + id_image + format_image
                save_image_g((150, 170), file_thumbnail, address_150x170)

            with open(address_new + thumbnail + '97x97_' + id_image + format_image, 'wb+') as file_thumbnail:
                image_convert_large = base64.b64decode(image[2])
                file_thumbnail.write(image_convert_large)
                address_97x97 = address_new + thumbnail + '97x97_' + id_image + format_image
                save_image_g((97, 97), file_thumbnail, address_97x97)
                address_87x87 = address_new + thumbnail + '87x87_' + id_image + format_image
                save_image_g((87, 87), file_thumbnail, address_87x87)
                address_82x82 = address_new + thumbnail + '82x82_' + id_image + format_image
                save_image_g((82, 82), file_thumbnail, address_82x82)

            list_image.append({
                'id': id_image,
                'small': address_mongo + "small_" + id_image + format_image,
                'large': address_mongo + "large_" + id_image + format_image,
                'thumbnail_150x170': address_mongo + thumbnail + '150x170_' + id_image + format_image,
                'thumbnail_97x97': address_mongo + thumbnail + '97x97_' + id_image + format_image,
                'thumbnail_87x87': address_mongo + thumbnail + '87x87_' + id_image + format_image,
                'thumbnail_82x82': address_mongo + thumbnail + '82x82_' + id_image + format_image
            })
        dict_image['list_image'] = list_image
        if dict_image['main_gallery'] == "True":
            main_image_address = list_image[0]['thumbnail_150x170']

        result.append(dict_image)

    cursor.product.update_one({'product_id': product_id}, {'$set': {'galleries': result, 'image': main_image_address}})
