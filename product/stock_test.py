from core.db_mongo import cursor


class StockController(object):
    def __init__(self):
        self.stock = cursor.stock
        self.product = cursor.product

    def set_product_stock(self):
        products = self.get_all_product()
        for product in products:
            self.set_stock_status(product)

        return self.stock.find()

    def get_all_product(self):
        return self.product.find({}, {"sizes": 1, "colors": 1, "product_id": 1})

    def insert_stock(self, size, color, product_id):
        self.stock.insert_one({
            "size": size["code"] if size else False,
            "color": color["code"] if color else False,
            "product_id": product_id,
            "marketable": 0,
            "stock": 0,
            "canceled": 0,
            "undelivered": 0
        })

    def set_stock_status(self, product):
        product_id = product["product_id"]
        sizes = product.get("sizes", [])
        colors = product.get("colors", [])

        if sizes and colors:
            for size in sizes:
                for color in colors:
                    self.insert_stock(size, color, product_id)

        elif sizes and not colors:
            for size in sizes:
                self.insert_stock(size, False, product_id)

        elif colors and not sizes:
            for color in colors:
                self.insert_stock(False, color, product_id)

        else:
            self.insert_stock(False, False, product_id)


if __name__ == "__main__":
    StockController().set_product_stock()
