import datetime
from django.conf import settings
from core.db_mongo import cursor
from sample_form.controller import SampleFormController
from pymongo import ReturnDocument
from cerberus import Validator
from schema import product_published_schema
import base64


def get_count_products(criteria):
    return cursor.product.count(criteria)


def get_count_products_published():
    criteria = settings.PRODUCT_GENERAL_CRITERIA.copy()
    criteria['published'] = True
    return cursor.product.count(criteria)


def get_count_products_no_published():
    criteria = settings.PRODUCT_GENERAL_CRITERIA.copy()
    criteria['published'] = False
    return cursor.product.count(criteria)


def find_one_product(criteria, projection=None):
    return cursor.product.find_one(criteria, projection)


def remove_by_product_id(product_id):
    return cursor.product.delete_one({"product_id": product_id})


def change_status_trash_product(product_id, trash_bool):
    criteria = {'product_id': product_id}
    data = {'$set': {'trash': trash_bool, 'published': False}}
    update = cursor.product.update_one(criteria, data)
    return update


def delete_gallery_by_id(product_id, gallery_id):
    criteria = {'product_id': product_id, "galleries.id": gallery_id}
    data = {
        "$pull": {"galleries": {"id": gallery_id}},
        '$set': {'modified_date': datetime.datetime.now()}
    }

    product = cursor.product.find_one_and_update(
        criteria,
        data,
        return_document=ReturnDocument.BEFORE
    )

    color_gallery = 'None'
    for gallery in product['galleries']:
        if gallery['id'] == gallery_id:
            color_gallery = gallery['color']
            break
    return color_gallery


def change_main_gallery_query(product_id, gallery_id):
    criteria = {
        'product_id': product_id,
        "galleries.main_gallery": 'True'
    }
    update = {"$set": {"galleries.$.main_gallery": 'False'}}
    cursor.product.update_one(criteria, update)

    criteria = {'product_id': product_id, "galleries.id": gallery_id}
    update = {"$set": {"galleries.$.main_gallery": 'True'}}
    galleries = cursor.product.find_one_and_update(
        criteria,
        update,
        return_document=ReturnDocument.AFTER
    )['galleries']

    main_image = ''

    for gallery in galleries:
        if gallery['main_gallery'] == "True":
            main_image = gallery['list_image'][0]['thumbnail_150x170']
            break
        else:
            main_image = gallery['list_image'][0]['thumbnail_150x170']

    criteria = {'product_id': product_id}
    update = {"$set": {"image": main_image}}
    update_product = cursor.product.update_one(criteria, update)

    return update_product.raw_result


def count_gallery_product(product_id):
    field_name = '$galleries'

    match = {'$match': {"product_id": product_id}}
    project = {'$project': {'_id': 0, 'count': {'$size': field_name}}}
    count = list(cursor.product.aggregate([match, project]))

    return count[0]['count'] if count and (len(count) > 0) and count[0].get('count', None) else 0


def paginator(product_per_page, step, criteria):
    """
        :return filter products
    """

    if 'trash' not in criteria:
        criteria.update({'ready_to_delete': False})

    count_skip = (step - 1) * product_per_page
    count_product_init = cursor.product.count(criteria)
    count_product = count_product_init % product_per_page
    if count_product > 0:
        count_product = (count_product_init / product_per_page) + 1

    else:
        count_product = count_product_init / product_per_page

    projection = {
        'product_id': 1,
        'categories': 1,
        'price': {'$slice': [0, 1]},
        'customer_price': 1,
        'modified_date': 1,
        'product_name_en': 1,
        'published': 1,
        'created_date': 1
    }
    result_product = cursor.product.find(criteria, projection).sort([('created_date', -1)]).skip(count_skip).limit(product_per_page)
    return {'count_product': count_product, 'result_product': result_product}


def get_products_by_criteria_projection(criteria, projection):
    return cursor.product.find(criteria, projection)


def show_gallery(gallery):
    dict_gallery = {}
    list_image = []
    dict_gallery['id'] = gallery['id']
    dict_gallery['color'] = gallery['color']
    dict_gallery['main_gallery'] = gallery['main_gallery']

    for image in gallery['list_image']:
        with open(settings.BASE_DIR + image['large'], "rb") as image_file_large:
            encoded_string_large = base64.b64encode(
                image_file_large.read())

        with open(settings.BASE_DIR + image['thumbnail_150x170'], "rb") as file_thumbnail:
            encoded_string_150x170 = base64.b64encode(
                file_thumbnail.read())

        dict_image = {
            'id': image['id'],
            'small': image['small'],
            'thumbnail_150x170': image['thumbnail_150x170'],
            'data_val_large': encoded_string_large,
            'data_val_150x170': encoded_string_150x170,
        }
        list_image.append(dict_image)
    dict_gallery['list_image'] = list_image
    return dict_gallery


def change_status_product(product_id, status):
    status_translator = {'false': False, 'true': True}
    if status_translator[status]:
        v = Validator()
        v.allow_unknown = True
        product = find_one_product({"product_id": product_id})
        sample_form = SampleFormController().find_sample_form(product['categories'][-1])
        if not sample_form or len(sample_form['searchable_keys']) == 0:
            product['filters'] = {"none": "none"}

        schema = product_published_schema()
        if v.validate(product, schema):
            product = cursor.product.find_one_and_update(
                {'product_id': product['product_id']},
                {"$set": {'published': True}},
                return_document=ReturnDocument.AFTER
            )
            return product, False
        else:
            return False, v.errors

    else:
        product = cursor.product.find_one_and_update(
            {'product_id': product_id},
            {"$set": {'published': False}},
            return_document=ReturnDocument.AFTER
        )
        return product, False


class CategoryController(object):
    def __init__(self):
        self.db = cursor.category

    def get_all_category(self):
        return self.db.find({'ancestors': {'$all': [0], '$size': 1}, 'published': True}, {'id': 1, 'name': 1}).sort('id', 1)

    def get_all_category_tuple(self):
        categories = self.get_all_category()
        list_categories = []
        for category in categories:
            tuple_c = (category['id'], category['name'])
            list_categories.append(tuple_c)
        return tuple(list_categories)

    def get_group_from_category(self, id_category):
        return self.db.find({'ancestors': {'$all': [id_category], '$size': 1}, 'published': True}, {'id': 1, 'name': 1})

    def get_group_from_category_list(self, id_category):
        groups = self.get_group_from_category(id_category)
        list_groups = []
        for group in groups:
            tuple_c = (group['id'], group['name'])
            list_groups.append(tuple_c)
        return list_groups

    def get_all_sub_group(self, list_parent):
        return self.db.find({'ancestors': {'$eq': sorted(list_parent)}, 'published': True, 'ready_to_delete': False}, {'id': 1, 'name': 1})

    def get_one_sub_group(self, list_parent):
        return self.db.find_one({'ancestors': {'$eq': sorted(list_parent)}, 'published': True}, {'id': 1, 'name': 1})

    def get_all_sub_group_list(self, list_parent):
        sub_groups = self.get_all_sub_group(list_parent)
        list_sub_groups = []
        for sub_group in sub_groups:
            dict_c = {'id': sub_group['id'], 'name': sub_group['name']}
            list_sub_groups.append(dict_c)

        return list_sub_groups

    def get_by_id(self, id):
        return self.db.find_one({'id': id})

    def get_by_list_id(self, list_id):
        return self.db.find(
            {'id': {"$in": list_id}},
            {'name': 1, "_id": 0, 'id': 1}
        )
