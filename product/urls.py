from django.conf.urls import url
from product.views import product_list, update_product,\
    update_json_product, create_product,\
    create_json_product, create_product_id, delete_product,\
    product_trashed, pagination_product, change_product_to_trash,\
    load_group, load_gallery_to_show, delete_gallery,\
    change_main_gallery, change_product_published, \
    find_one_product_by_id, edit_color_product

urlpatterns = [
    url(r'^$', product_list, name='productList'),
    url(r'^update/(?P<product_id>\d+)$', update_product, name='update_product'),
    url(r'^update_json_product/$', update_json_product, name='update_json_product'),

    url(r'^create/product_id$', create_product_id, name='create_product_id'),
    url(r'^create/(?P<product_id>\d+)$', create_product, name='create_product'),
    url(r'^create_json_product/$', create_json_product, name='create_json_product'),

    url(r'^delete/$', delete_product, name='delete_product'),

    url(r'^trash/$', product_trashed, name='product_trashed'),
    url(r'^pagination/$', pagination_product, name='paginationProduct'),
    url(r'^trash/pagination/$', pagination_product, name='paginationProduct'),
    url(r'^change_product_to_trash/$', change_product_to_trash, name='change_Product_To_Trash'),

    url(r'^loadGroup/$', load_group, name='load_group'),

    url(r'^load_gallery/$', load_gallery_to_show,  name='load_gallery_to_show'),
    url(r'^delete_gallery/$', delete_gallery, name='delete_gallery'),
    url(r'^change_main_gallery/$', change_main_gallery, name='change_main_gallery'),

    url(r'^change_product_published/$', change_product_published, name='change_product_published'),

    url(r'^find/one/', find_one_product_by_id, name='find_one_product_by_id'),

    url(r'^edit_color_product/', edit_color_product, name='edit_color_product'),
]
