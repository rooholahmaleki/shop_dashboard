def product_published_schema():
    return {
        'national_code': {
            'type': 'string',
            'empty': False,
            'required': True,
        },
        'published': {
            'type': 'boolean',
            'allowed': [False],
            'required': True
        },
        'trash': {
            'type': 'boolean',
            'allowed': [False],
            'required': True
        },
        'gender': {
            'type': 'integer',
            'empty': False,
            'required': True
        },
        'unit': {
            'type': 'float',
            'empty': False,
            'required': True
        },
        # 'unit_type': {
        #     'type': 'integer',
        #     'empty': False,
        #     'required': True
        # },
        'product_name_en': {
            'type': 'string',
            'empty': False,
            'required': True
        },
        'product_name_fa': {
            'type': 'string',
            'empty': False,
            'required': True
        },
        # 'brand': {
        #     'type': 'integer',
        #     'empty': False,
        #     'required': True
        # },
        'filters': {
            'type': 'dict',
            'empty': False,
            'required': True,
            'valueschema': {
                'empty': False,
                'nullable': False
            }
        },
        'galleries': {
            'type': 'list',
            'required': True,
            'minlength': 1,
            'schema': {
                'type': 'dict',
                'required': True,
                'empty': False,
                'schema': {
                    'color': {
                        'type': 'string',
                        'empty': False,
                        'required': True
                    },
                    'list_image': {
                        'type': 'list',
                        'required': True,
                        'empty': False,
                        'schema': list_image_schema()
                    },
                    'main_gallery': {
                        'type': 'string',
                        'required': True,
                        'empty': False
                    },
                    'id': {
                        'type': 'string',
                        'required': True,
                        'empty': False
                    }
                }
            }
        }
    }


def list_image_schema():
    return {
        'type': 'dict',
        'required': True,
        'empty': False,
        'schema': {
            'thumbnail_97x97': {
                'type': 'string',
                'required': True,
                'empty': False
            },
            'thumbnail_82x82': {
                'type': 'string',
                'required': True,
                'empty': False
            },
            'thumbnail_87x87': {
                'type': 'string',
                'required': True,
                'empty': False
            },
            'thumbnail_150x170': {
                'type': 'string',
                'required': True,
                'empty': False
            },
            'large': {
                'type': 'string',
                'required': True,
                'empty': False
            },
            'small': {
                'type': 'string',
                'required': True,
                'empty': False
            },
            'id': {
                'type': 'string',
                'required': True,
                'empty': False
            }
        }
    }
