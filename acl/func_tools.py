# -*- coding:utf-8 -*-
from bson.objectid import ObjectId
from core.db_mongo import cursor


def insert_acl_group(doc):
    result = cursor.acl_group.insert(doc)
    if ObjectId.is_valid(result):
        return True
    else:
        return False


def group_query(data, _id):
    criteria = {'_id': ObjectId(_id)}
    update = cursor.acl_group.find_one_and_replace(criteria, data)
    return update


def get_groups():
    groups = list(cursor.acl_group.find({}, {"group_name": 1, "_id": 1}))
    dict_groups = {}
    for item in groups:
        # dict_groups[str(item["_id"])] = item["group_name"]
        dict_groups[item["group_name"]] = item["group_name"]
    return dict_groups
