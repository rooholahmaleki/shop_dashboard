# coding: utf-8

from django import forms
from utils import acl_visible_segment, acl_view_segment
from acl.func_tools import get_groups


class GroupForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(GroupForm, self).__init__(*args, **kwargs)

    group_name = forms.CharField(
        max_length=100,
        error_messages={'required': 'نام گروه را وارد کنید.'},
        label="نام گروه",
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'نام انگلیسی'})
    )
    visible_templatetags = forms.MultipleChoiceField(
        choices=acl_visible_segment.items(),
        required=False,
        label="دسترسی ها",
        widget=forms.CheckboxSelectMultiple(attrs={"class": "uk-grid"})
    )

    def clean(self, *args, **kwargs):
        form_data = super(GroupForm, self).clean()
        visible_templatetags = form_data.get('visible_templatetags')
        form_data_clean = {
            'visible_templatetags': visible_templatetags,
            'group_name': form_data.get('group_name'),
            'views': []
        }
        for template in visible_templatetags:
            form_data_clean['views'].extend(acl_view_segment.get(template, []))

        self.cleaned_data = form_data_clean


class UserForm(forms.Form):
    gender_option = (
        (1, 'آقا'),
        (2, 'خانم')
    )

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)

    username = forms.CharField(label='نام کاربری', max_length=15, required=True)
    password1 = forms.CharField(label='رمز عبور', widget=forms.PasswordInput, required=False)
    password2 = forms.CharField(label='تکرار و تایید رمز عبور', widget=forms.PasswordInput, required=False)
    group_name = forms.ChoiceField(
        label='گروه خود را انتخاب کنید',
        widget=forms.Select(attrs={'class': 'combo-large dash-groupname-combo'}),
        choices=get_groups().items(),
        required=True
    )
    first_name = forms.CharField(label='نام', required=True)
    last_name = forms.CharField(label='نام خانوادگی', required=True)
    email = forms.EmailField(label='آدرس صندوق الکترونیکی', required=True)
    gender = forms.ChoiceField(
        label='جنسیت:',
        choices=gender_option,
        widget=forms.RadioSelect(attrs={'tabindex': '3'})
    )

    def clean(self):
        cleaned_data = super(UserForm, self).clean()
        password1 = cleaned_data.get('password1', '')
        password2 = cleaned_data.get('password2', '')

        if password1 != password2:
            self.add_error('password1', 'رمز عبورهای وارد شده، یکسان نمی باشد.')

        if not (password1 and password2):
            cleaned_data.pop('password1', '')
            cleaned_data.pop('password2', '')
