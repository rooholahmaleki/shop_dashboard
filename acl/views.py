from bson import ObjectId
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.http import require_POST
from acl.func_tools import group_query, insert_acl_group
from core.jalali import Jalali
from forms import GroupForm, UserForm
from core.db_mongo import cursor

import bcrypt
import datetime


def define_group(request):
    args = {}
    if request.method == 'POST':
        args['form'] = GroupForm(request.POST)
        if args['form'].is_valid():
            data = args['form'].cleaned_data
            insert = insert_acl_group(data)
            if insert:
                return HttpResponseRedirect('/acl/group_list')
            else:
                args['error'] = {'errors': {'insert error': 'insert was not successful'}}
        else:
            args['error'] = {'errors': args['form'].errors}

    args['form'] = GroupForm()
    return render(request, 'acl/define_group.html', args)


def group_list(request):
    args = {'data': list(cursor.acl_group.find())}
    for row in args['data']:
        row['id'] = row.pop('_id')

    return render(request, 'acl/group_list.html', args)


def edit_group(request, _id):
    args = {}
    initial = cursor.acl_group.find_one({'_id': ObjectId(_id)}, {'_id': 0})

    if request.POST:
        args['form'] = GroupForm(request.POST)
        if args['form'].is_valid():
            data = args['form'].cleaned_data
            update = group_query(data, _id)
            if update:
                return HttpResponseRedirect('/acl/group_list')
        else:
            args['error'] = {'errors': args['form'].errors}

    args['form'] = GroupForm(initial)

    return render(request, 'acl/edit_group.html', args)


@require_POST
def delete_group(request, _id):
    remove = cursor.acl_group.delete_one({'_id': ObjectId({'_id': _id})})
    return JsonResponse(remove, safe=False)


def delete_user(request, user_id):
    cursor.users.delete_one({'_id': ObjectId(user_id)})
    return redirect(user_list)


def add_user(request):
    context = {}

    if request.method != 'POST':
        form = UserForm()
    else:
        form = UserForm(request.POST)

        if not form.is_valid():
            context['errors'] = form.errors
        else:
            data = form.cleaned_data

            processed_data = {
                'personal': {
                    'first_name': data['first_name'],
                    'last_name': data['last_name'],
                },
                'group_name': data['group_name'],
                'username': data['username'].lower(),
                'gender': int(data['gender']),
                'password': data['password1'],
                'user_type': 'OPERATOR',
                'email': data['email'].lower(),
            }

            try:
                user_id = str(register_dashboard(processed_data))
            except Exception:
                context['errors'] = {'message': 'DuplicateKeyError'}
            else:
                return HttpResponseRedirect('/acl/user_list')

    context['form'] = form

    return render(request, 'acl/add_user.html', context)


def register_dashboard(data):
    password = data['password'].encode('utf-8')
    hashed = bcrypt.hashpw(password, bcrypt.gensalt(10))
    data['password'] = bcrypt.hashpw(password, hashed)
    data['created_date'] = datetime.datetime.now()
    return cursor.users.insert(data)


def user_list(request):
    args = {}
    new_data = []
    users = cursor.users.find({"user_type": "OPERATOR"})
    for user in users:
        doc = {
            'id': str(user['_id']),
            'username': user['username'],
            'group_name': user['group_name'],
            'full_name': user['personal']['first_name'] + " " + user['personal']['last_name'],
            'email': user['email']
        }

        new_data.append(doc)

    args['users'] = new_data
    return render(request, 'acl/user_list.html', args)


def edit_username(request, user_id):
    context = {}
    user = cursor.users.find_one({"_id": ObjectId(user_id), "user_type": "OPERATOR"})

    form = UserForm(request.POST or None, initial={
        'username': user['username'],
        'group_name': user['group_name'],
        'first_name': user['personal']['first_name'],
        'last_name': user['personal']['last_name'],
        'email': user['email'],
        'gender': user['gender'],
    })

    if request.method == 'POST':
        if form.is_valid():
            form_data = form.cleaned_data

            form_data['personal'] = {}
            if 'last_name' in form_data:
                form_data['personal']['last_name'] = form_data.pop('last_name')
            if 'first_name' in form_data:
                form_data['personal']['first_name'] = form_data.pop('first_name')

            if 'password' in form_data:
                password = form_data['password'].encode('utf-8')
                hashed = bcrypt.hashpw(password, bcrypt.gensalt(10))
                form_data['password'] = bcrypt.hashpw(password, hashed)

            cursor.users.update_one({'_id': ObjectId(user_id)}, {'$set': form_data})
            return HttpResponseRedirect('/acl/user_list')
        else:
            context['errors'] = form.errors

    context['form'] = form
    return render(request, 'acl/edit_username.html', context)


def user_shop_list(request):
    args = {}
    new_data = []
    users = cursor.users.find({"user_type": "USER"})
    for user in users:
        jalali_invoice_date = Jalali(user['created_date'])
        doc = {
            'id': str(user['_id']),
            'full_name': user.get('first_name', "") + " " + user.get('last_name', ""),
            'phone_number': user.get('phone_number'),
            'created_date': jalali_invoice_date.date_with_space,
            'code': user.get('code'),
        }

        new_data.append(doc)

    args['users'] = new_data
    return render(request, 'acl/user_shop_list.html', args)

