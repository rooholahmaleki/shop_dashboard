from django.conf.urls import url
from acl import views


urlpatterns = [
    url(r'^define_group/$', views.define_group, name='define_group'),
    url(r'^group_list/$', views.group_list, name='group_list'),
    url(r'^edit_group/(?P<_id>[\w\d]+)/$', views.edit_group, name='edit_group'),
    url(r'^delete_group/(?P<user_id>[\w\d]+)/$', views.delete_group, name='delete_group'),

    url(r'^add_user/$', views.add_user, name='add_user'),
    url(r'^user_list/$', views.user_list, name='user_list'),
    url(r'^user_shop_list/$', views.user_shop_list, name='user_shop_list'),
    url(r'^edit_username/(?P<user_id>[\w\d]+)/$', views.edit_username, name='edit_username'),
    url(r'^delete_user/(?P<user_id>[\w\d]+)/$', views.delete_user, name='delete_user'),
]
