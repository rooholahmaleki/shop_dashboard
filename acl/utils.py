# coding: utf-8
from collections import OrderedDict

acl_visible_segment = OrderedDict()

acl_visible_segment['acl_management'] = u'مدیریت کاربران'
acl_visible_segment['category_management'] = u'دسته بندی محصولات'
acl_visible_segment['banners_management'] = u'بنر'
acl_visible_segment['sample_forms_management'] = u'مشخصات فنی محصولات'
acl_visible_segment['colors_management'] = u'رنگ بندی ها'
acl_visible_segment['brands_management'] = u'مدیریت برندها'
acl_visible_segment['sizes_management'] = u'مدیریت سایزبندی'
acl_visible_segment['products_management'] = u'مدیریت محصولات'
acl_visible_segment['amazing_offers_management'] = u'پیشنهاد ویژه'
acl_visible_segment['factors_management'] = u'مالی و حسابداری'

acl_view_segment = {
    "acl_management": [
        "define_group",
        "group_list",
        "user_list",
        "edit_group",
        "delete_group",
        "add_user",
        "edit_username",
        "user_shop_list",
        "delete_user"
    ],
    "category_management": [
        "CategoryIndex",
        "CreateCategory",
        "UpdateCategoryPublish",
        "UpdateSubCategoryPublish",
        "UpdateCategoryName",
        "UpdateCategoryOrder",
        "CreateSubCategory",
        "CategoryDetail",
        "SubcategoryDelete",
        "CategoryDelete"
    ],
    "colors_management": [
        "color_list",
        "add_color",
        "edit_color",
        "delete_color"
    ],
    "sample_forms_management": [
        "sample_forms",
        "add_sample_form",
        "delete_sample_form",
        "edit_sample_form",
    ],
    "brands_management": [
        "brands",
        "block_brands",
        "pending_brands",
        "add_or_edit_brand",
        "delete_brand",
        "find_brand_by_id",
        "delete_brand_word",
        "load_group",
    ],
    "sizes_management": [
        "sizes",
        "add_size",
        "delete_size",
        "edit_size",
        "check_group_has_size",
        "find_size_by_group_id",
    ],
    "products_management": [
        "product_list",
        "pagination_product",
        "product_trashed",
        "update_product",
        "update_json_product",
        "delete_product",
        "upload_video",
        "product_trashed",
        "change_product_to_trash",
        "load_review",
        "load_gallery_to_show",
        "delete_gallery",
        "change_main_gallery",
        "change_product_published",
        "find_one_product_by_id",
        "pending_product",
        "edit_color_product",
        "create_product_id",
        "create_product",
        "create_json_product",
        "delete_color_product",
        "add_color_product",
    ],
    "amazing_offers_management": [
        "amazing_offers",
        "add_amazing_offer",
        "delete_amazing_offer",
        "edit_amazing_offer",
    ],
    "factors_management": [
        "factors",
        "view_factor",
        "delete_factor",
        "pagination_factor",
        "change_state",
        "finance",
        "get_factor",
    ],
    "banners_management": [
        "banner_list",
        "banner_add",
        "banner_edit",
        "banner_delete",
    ]
}
