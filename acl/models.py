from django.db import models
from django.contrib.auth.models import User as DjangoUser


class UserBox(models.Model):
    user = models.OneToOneField(DjangoUser)
    mongo_id = models.CharField(max_length=24)
    image_flag = models.BooleanField(default=False)
    group_name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.user.username
