import jdatetime
from core.db_mongo import cursor
from django.conf import settings
import os


def get_all_amazing_offer(criteria):
    amazing_offers = list(cursor.amazing_offer.find(criteria))
    list_product_id = []

    for item in amazing_offers:
        list_product_id.append(item['product_id'])

    products = list(cursor.product.find(
        {'product_id': {'$in': list_product_id}},
        {'_id': 0, 'product_name_en': 1, 'product_id': 1}
    ))

    for amazing_offer in amazing_offers:
        for product in products:
            if product['product_id'] == amazing_offer['product_id']:
                amazing_offer['product'] = product
                break

    return amazing_offers


def insert_amazing_offer(data):
    cursor.product.update_one(
        {'product_id': data['product_id']},
        {
            "$set": {
                'amazing_offer': {
                    'active': True,
                    'expiration_date': data['expiration_date']
                },
                'customer_price': int(data['amazing_price'])
            }
        }
    )
    cursor.amazing_offer.insert(data)

    # scheduler.add_job(
    #     expire_amazing_offer,
    #     'date',
    #     run_date=data['expiration_date'],
    #     args=[data['product_id']],
    #     timezone=settings.LOCAL_TZ
    # )
    return True


def find_for_edit_amazing_offer(criteria):
    amazing_offer = cursor.amazing_offer.find_one(criteria)

    if amazing_offer:
        criteria_product = {'product_id': criteria['product_id']}
        projection = {
            '_id': 0,
            'product_name_fa': 1,
            'product_name_en': 1,
            'price': 1,
            'product_id': 1,
            'image': 1
        }
        product = cursor.product.find_one(
            criteria_product,
            projection
        )
        try:
            product['price'] = product['price'][0]
        except:
            product['price'] = None

        amazing_offer['product'] = product

        amazing_offer['start_date'] = change_jalali_to_gregorian(amazing_offer['start_date'])
        amazing_offer['expiration_date'] = change_jalali_to_gregorian(amazing_offer['expiration_date'])
        return amazing_offer

    return False


def change_jalali_to_gregorian(date):
    gregorian = jdatetime.date.fromgregorian(date=date)
    hour = check_len_hour_or_minute(date.hour)
    minute = check_len_hour_or_minute(date.minute)
    date = "{0}/{1}/{2} {3}:{4}".format(gregorian.year, gregorian.month, gregorian.day, hour, minute)
    return date


def check_len_hour_or_minute(number):
    if number < 10:
        number = "0" + str(number)
    return number


def find_amazing_offer(product_id):
    criteria = {'product_id': product_id}
    return cursor.amazing_offer.find_one(criteria)


def update_amazing_offer(product_id, update):
    cursor.product.update_one(
        {'product_id': product_id},
        {
            "$set": {
                'amazing_offer': {
                    'active': True,
                    'expiration_date': update['expiration_date']
                },
                'customer_price': int(update['amazing_price'])
            }
        }
    )
    amazing_offer = cursor.amazing_offer.update_one(
        {'product_id': product_id},
        {"$set": {item: update[item] for item in update}}
    )
    return amazing_offer.raw_result


def remove_amazing_offer(product_id):
    criteria = {'product_id': product_id}
    product = cursor.product.find_one(criteria, {'_id': 0, 'price': 1})
    try:
        customer_price = product['price'][0]['customer_price']
    except:
        customer_price = 0

    update_result = cursor.product.update_one(
        criteria,
        {"$set": {'amazing_offer': {'active': False}, 'customer_price': int(customer_price)}}
    )

    if update_result.raw_result['nModified']:
        cursor.amazing_offer.remove(criteria)
        try:
            address = settings.BASE_DIR + '/media/amazing_offer/' + product_id + '.png'
            os.remove(address)
        except:
            pass

        return True
    else:

        return False
