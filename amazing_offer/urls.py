from django.conf.urls import url
from amazing_offer.views import amazing_offers, add_amazing_offer, delete_amazing_offer, edit_amazing_offer

urlpatterns = [
    url(r'^$', amazing_offers, name='amazing_offers'),
    url(r'^add/$', add_amazing_offer, name='add_amazing_offer'),
    url(r'^delete/$', delete_amazing_offer, name='delete_amazing_offer'),
    url(r'^edit/(?P<product_id>[\w\d]+)$', edit_amazing_offer, name='edit_amazing_offer'),
]
