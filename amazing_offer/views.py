import os

from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from forms import AmazingOffer
from django.conf import settings
from amazing_offer.controller import get_all_amazing_offer,\
    find_amazing_offer, insert_amazing_offer,\
    update_amazing_offer, find_for_edit_amazing_offer,\
    remove_amazing_offer


def amazing_offers(request):

    args = {
        'amazing_offers': get_all_amazing_offer({}),
    }
    return render(request, 'amazing_offer/amazing_offers.html', args)


def add_amazing_offer(request):
    if request.method == "POST":
        amazing_form = AmazingOffer(request.POST)
        response = {}
        if amazing_form.is_valid():
            form_data = amazing_form.cleaned_data
            update = form_data.pop('update', None)
            if update == 'no':
                find_product = find_amazing_offer(form_data['product_id'])
                if not find_product and 'image' in request.FILES:
                    form_data['image'] = save_image(request.FILES['image'], form_data['product_id'])
                    result = insert_amazing_offer(form_data)
                    if result:
                        response = {
                            'message': "success",
                            'value': "insert",
                        }
                    else:
                        response['message'] = "query_error"
                else:
                    response['message'] = "repeat_error"
                    if 'image' not in request.FILES:
                        response['message'] = "image_error"
            else:
                if 'image' in request.FILES:
                    form_data['image'] = save_image(request.FILES['image'], form_data['product_id'])
                product_id = form_data.pop('product_id')
                result = update_amazing_offer(product_id, form_data)
                if result:
                    response = {
                        'message': "success",
                        'value': "update"
                    }
                else:
                    response['message'] = "query_error"
        else:
            response = {
                'message': "form_error",
                'value': amazing_form.errors
            }

        return HttpResponse(
            JsonResponse(response),
            content_type='application/json')
    else:
        args = {
            'form': AmazingOffer(initial={'update': 'no'})
        }
        return render(request, 'amazing_offer/add_amazing_offer.html', args)


def save_image(image_file, image_name):
    image_address_django = '/media' + settings.AMAZING_OFFER + str(image_name) + '.png'
    image = settings.MEDIA_ROOT + settings.AMAZING_OFFER
    if not os.path.exists(image):
        os.makedirs(image)

    with open(image + str(image_name) + '.png', 'wb+') as destination:
        for chunk in image_file.chunks():
            destination.write(chunk)

    return image_address_django


def delete_amazing_offer(request):
    product_id = request.POST['product_id']
    amazing_offer = remove_amazing_offer(product_id)
    if amazing_offer:
        message = "success"
    else:
        message = "query_error"

    return HttpResponse(
        JsonResponse({'message': message}),
        content_type='application/json')


def edit_amazing_offer(request, product_id):
    criteria = {'product_id': product_id}
    amazing_offer = find_for_edit_amazing_offer(criteria)
    if amazing_offer:
        initial = {
            'product_id': amazing_offer['product_id'],
            'amazing_price': amazing_offer['amazing_price'],
            'start_date': amazing_offer['start_date'],
            'update': 'yes',
            'expiration_date': amazing_offer['expiration_date'],
        }
        args = {
            'amazing_offer': amazing_offer,
            'product_id': product_id,
            'form': AmazingOffer(initial=initial)
        }
        return render(request, 'amazing_offer/add_amazing_offer.html', args)
    else:
        return redirect(amazing_offers)
