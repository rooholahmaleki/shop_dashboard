# coding: utf-8

from django import forms
import jdatetime
import datetime


class AmazingOffer(forms.Form):
    def __init__(self, *args, **kwargs):
        super(AmazingOffer, self).__init__(*args, **kwargs)

    product_id = forms.CharField(
        widget=forms.HiddenInput(),
        error_messages={'required': 'محصول انتخاب نشده است.'},
        required=True
    )
    customer_price = forms.CharField(
        widget=forms.HiddenInput(),
        required=False
    )
    update = forms.CharField(
        widget=forms.HiddenInput(),
        required=False
    )
    amazing_price = forms.IntegerField(
        label='قیمت پیشنهادی',
        error_messages={'required': 'قیمت پیشنهادی وارد نشده است'},
        widget=forms.TextInput(attrs={'type': "text"}),
        required=True
    )
    start_date = forms.CharField(
        label='تاریخ شروع',
        widget=forms.TextInput(attrs={'style': 'direction: ltr'}),
        required=False
    )
    expiration_date = forms.CharField(
        label='تاریخ انقضا',
        widget=forms.TextInput(attrs={'style': 'direction: ltr'}),
        required=False
    )
    image = forms.FileField(
        label='بنر پیشنهاد ویژه',
        required=False
    )

    def clean(self, *args, **kwargs):
        form_data = super(AmazingOffer, self).clean()
        start_date = convert_date_jalaji_to_gregorian(form_data.get('start_date'))
        expiration_date = convert_date_jalaji_to_gregorian(form_data.get('expiration_date'))

        del form_data['image']
        try:
            form_data['customer_price'] = int(form_data['customer_price'])
            form_data['amazing_price'] = int(form_data['amazing_price'])
        except:
            pass

        if start_date and expiration_date and start_date < expiration_date:
            if expiration_date < datetime.datetime.now():
                self.add_error("expiration_date", "زمان انقضا تمام شده است")

            form_data['expiration_date'] = expiration_date
            form_data['start_date'] = start_date
        else:
            self.add_error("expiration_date", "تاریخ ها را حتما وارد کنید.تاریخ شروع از انقضا باید کمتر باشد")


def convert_date_jalaji_to_gregorian(_date_time):
    try:
        _date_time = jdatetime.datetime.strptime(_date_time, "%Y/%m/%d %H:%M")
        _gdate = jdatetime.JalaliToGregorian(_date_time.year, _date_time.month, _date_time.day)
        _date_time = datetime.datetime(
            _gdate.gyear,
            _gdate.gmonth,
            _gdate.gday,
            _date_time.hour,
            _date_time.minute
        )
        return _date_time
    except:
        return False
