# coding: utf-8

FACTOR_STATE_MAP = {
    'pending': u'در صف بررسی',
    'sending': u'در حال ارسال',
    'sent': u'ارسال شد',
    'canceled': u'کنسل شد',
    'delivered': u'تحویل داده شد',
}

STATE_METHOD = {
    1: u'پرداخت در محل',
    2: u'پرداخت آنلاین',
}
