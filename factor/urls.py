from django.conf.urls import url
from factor import views

urlpatterns = [
    url(r'^factors/(?P<state>[\w\d]+)$', views.factors, name='factors'),

    url(r'^view_factor/(?P<factor_id>[\w\d]+)$', views.view_factor, name='view_factor'),
    url(r'^change_state/(?P<factor_id>[\w\d]+)$', views.change_state, name='change_state'),

    url(r'^finance/$', views.finance, name='finance'),
    # url(r'^delete_factor/(?P<factor_id>[\w\d]+)$', views.delete_factor, name='delete_factor'),
]
