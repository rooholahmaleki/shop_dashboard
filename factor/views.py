from django.views.decorators.http import require_POST
from django.shortcuts import render
from django.http import JsonResponse
from core.db_mongo import cursor
from factor.static import FACTOR_STATE_MAP, STATE_METHOD
from bson.json_util import ObjectId
from core.jalali import Jalali


def factors(request, state):
    if state == "all":
        _factors = list(cursor.invoice.find().sort('created_date', -1))
    else:
        _factors = list(cursor.invoice.find({"state": state}).sort('created_date', -1))

    for factor in _factors:
        refine_invoice(factor)
        factor['user'] = cursor.users.find_one({'phone_number': factor['user_id']})

    return render(request, 'factor/factors.html', {
        'factors': _factors,
        'page_state': FACTOR_STATE_MAP.get(state, state),
        'state': state,
    })


def refine_invoice(invoice):
    invoice['id'] = str(invoice.pop('_id'))
    payment_state = invoice.get('state', None)
    if payment_state:
        invoice['payment_state_fa'] = FACTOR_STATE_MAP.get(payment_state, payment_state)

    payment_method = invoice.get('payment_method', None)
    if payment_method:
        invoice['payment_method'] = STATE_METHOD.get(payment_method, payment_method)

    jalali_invoice_date = Jalali(invoice['created_date'])

    invoice['shamsi_date'] = jalali_invoice_date.date_with_space
    invoice['created_date'] = jalali_invoice_date.ago


def view_factor(request, factor_id):
    factor = cursor.invoice.find_one({'_id': ObjectId(factor_id)})

    refine_invoice(factor)

    factor['user'] = cursor.users.find_one({'phone_number': factor['user_id']})

    return render(request, 'factor/view_factor.html', {'factor': factor})


def finance(request):
    year = request.GET.get("year", "1396")
    month = request.GET.get("month", "1")
    reports = []
    from bson.son import SON
    if year and year.isdigit() and month and month.isdigit():
        reports = list(cursor.invoice.aggregate([
            {"$match": {"jalali_year": int(year), "jalali_month": int(month)}},
            {"$group": {
                "_id": "$jalali_day",
                "total_price": {"$sum": "$total_price"},
            }},
            {"$sort": SON([("_id", 1)])}
        ]))

    return render(request, 'factor/finance.html', {
        "reports": reports,
        "month_select": month,
        "year_select": year,
        "month_count": [str(i) for i in range(1, 13)],
        "year_count": [str(1394 + i) for i in range(1, 10)],
    })


def change_state(request, factor_id):
    state = request.POST.get('state')
    if state and state in FACTOR_STATE_MAP:
        factor = cursor.invoice.update_one(
            {'_id': ObjectId(factor_id)},
            {'$set': {
                'state': state,
                'state_text': FACTOR_STATE_MAP[state],
            }},
        ).modified_count
        if factor:
            return JsonResponse({'success': True})
    return JsonResponse({'success': False})
