function delete_comment(comment_id){
    UIkit.modal.confirm("آیا می خواهید کامنت پاک شود؟", function () {
        $.ajax({
            url: '/comment/deleteComment/',
            type: 'POST',
            data: {
                'comment_id': comment_id,
                'csrfmiddlewaretoken': getCookie('csrftoken')
            },
            success: function (result) {
                $('#'+comment_id).remove();
                UIkit.notify({
                    message : 'کامنت با موفقیت پاک شد.',
                    status  : 'success',
                    timeout : 2000,
                    pos     : 'bottom-left'
                });
            },
            error: function (e) {
                notify_error(e, true)
            }
        });
    }, {labels:{ 'Ok': 'بلی', 'Cancel': 'خیر'}});
}

function check_report(comment_id){
    UIkit.modal.confirm("آیا مطمءن هستید؟", function () {
        $.ajax({
            url: '/comment/checkReport/',
            type: 'POST',
            data: {
                'comment_id': comment_id,
                'csrfmiddlewaretoken': getCookie('csrftoken')
            },
            success: function (result) {
                $('#'+comment_id).remove();
                UIkit.notify({
                    message : 'کامنت با موفقیت پاک شد.',
                    status  : 'success',
                    timeout : 2000,
                    pos     : 'bottom-left'
                });
            },
            error: function (e) {
                notify_error(e, true)
            }
        });
    });
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}