var csrf_token = "";

function add_word() {
    var word = $('#word').val();
    if(word.trim() != ""){
        $.ajax({
            url: "/word/add_word/",
            type: "post", // http method
            dataType: 'json',
            data: {
                'word': word,
                'csrfmiddlewaretoken': csrf_token
            },
            success: function (data) {
                if (data['message'] == 'error') {
                    uikitNotify('با عرض پوزش خطای سیستمی رخ داده است.', 'panda\&#32panda-cross', 'danger');
                }
                if (data['message'] == 'DuplicateKey') {
                    uikitNotify('کلمه تکرامی می باشد', 'panda\&#32panda-warning', 'warning');
                }

                 else {
                    append_word_to_html(data, word)
                }
            },
            error: function () {
                uikitNotify('با عرض پوزش خطای سیستمی رخ داده است.', 'panda\&#32panda-cross', 'danger');
            }
        });
    }else{
        uikitNotify('فیلد نام خالی می باشد.', 'panda\&#32panda-warning', 'warning');
    }
}

function append_word_to_html(data, word){
    if(data['list_id']){
        if(data['list_id'][0] == false){
            uikitNotify('کلمه تکراری می باشد.', 'panda\&#32panda-warning', 'warning');
        }else{
            for(var i in data['list_id']){
                var html_word =
                    '<li class="sh-filter-worlds" id="'+data['list_id'][i]+'">'+
                    '<a onclick="delete_word(\''+data['list_id'][i]+'\')"><i class="panda panda-cross"></i></a>'+
                    '<span>'+word+'</span>'+
                    '</li>';
                $('#list_word_filtering').prepend(html_word)
            }
            uikitNotify('کلمه اضافه شد.', 'panda\&#32panda-checkmark2', 'success');
            $('#word').val('');
        }
    }
}

function delete_word(word_id) {
    UIkit.modal.confirm("آیا می خواهید این کلمه از فیلترینگ کلمات حذف شود؟",function () {
        $.ajax({
            url: "/word/delete_word/",
            type: "post", // http method
            dataType: 'json',
            data: {
                'word_id': word_id,
                'csrfmiddlewaretoken': csrf_token
            },
            success: function (data) {
                if (data['message'] == 'error') {
                    uikitNotify('با عرض پوزش خطای سیستمی رخ داده است.', 'panda\&#32panda-cross', 'danger');
                } else {
                    $("#" + word_id).remove();
                    uikitNotify('با موفقیت پاک شد.', 'panda\&#32panda-checkmark2', 'success');
                }
            },
            error: function () {
                uikitNotify('با عرض پوزش خطای سیستمی رخ داده است.', 'panda\&#32panda-cross', 'danger');
            }
        });
    }, {labels:{ 'Ok': 'بلی', 'Cancel': 'خیر'}});
}
