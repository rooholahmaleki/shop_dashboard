var csrf_token = "",
    list_gift = [];

function getInfoProduct(){
    var product_id = $('#id_gift').val();
    if(product_id != ""){
        $.ajax({
            url: "/products/find/one/",
            type: "POST", // http method
            dataType: 'json',
            data: {
                'product_id': product_id,
                'csrfmiddlewaretoken': csrf_token
            },
            success: function (data) {
                if (data['message'] == 'success') {
                    var product = data['product'];
                    $('.gift-search').removeClass('uk-hidden');
                    $('#gift_name_fa').html(product['product_name_fa']);
                    $('#gift_name_en').html(product['product_name_en']);
                    $('#gift_image').attr('src', product['image']);
                    $('#gift_product_id').val(product['product_id']);
                } else {
                    $('.gift-search').addClass('uk-hidden');
                    uikitNotify('محصول مورد نظر یافت نشد.', 'panda\&#32panda-warning', 'warning');
                }
            },
            error: function () {
                uikitNotify('با عرض پوزش خطای سیستمی رخ داده است.', 'panda\&#32panda-cross', 'danger');
            }
        });
    }else{
        uikitNotify('کد محصول را وارد کنید.', 'panda\&#32panda-warning', 'warning');
    }
}

function addGift(){
    var bool = true;
    var gift_id = $('#gift_product_id').val();
    for (var item in list_gift) {
        if (list_gift[item] == gift_id) {
            bool = false;
        }
    }
    if(bool){
        list_gift.push(gift_id);
        var html = '<a class="uk-margin-large-top uk-button sh-btn-10" id="gift_' + gift_id + '" class="sh-btn-10 uk-button" style="margin-left:5px;"><i class="panda panda-cross" onclick="removeGift(\'' + gift_id + '\')"></i>' + gift_id + '</a>';
        $('#gifts').append(html);

    }else {
        uikitNotify('محصول تکراری می باشد.', 'panda\&#32panda-warning', 'warning');
    }
}

function removeGift(gift_id){
    $('#gift_' + gift_id).remove();
    for (var item in list_gift) {
        if (list_gift[item] == gift_id) {
            list_gift.splice(item, 1)
        }
    }
}

function getGifts(){
    return list_gift;
}
