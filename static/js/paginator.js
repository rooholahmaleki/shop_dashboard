var csrf_token = "",
    perm = "",
    content_product = "",
    supply_unit_perm = "",
    trash = "",
    step = 1;

function paginator(step_selected) {
    step = step_selected;
    $("#products").empty();
    $("#paginator").children().eq(1).nextAll().remove();
    trash = $('#id_trash').val();
    var data = {
        'product_per_page':     $("#id_product_per_page").val(),
        'exist':                $("#id_exist").val(),
        'published':            $("#id_published").val(),
        'min_price':            $("#id_min_price").val(),
        'max_price':            $("#id_max_price").val(),
        'search_word':          $("#id_search").val(),
        'step':                 step,
        'trash_type':           trash,
        'csrfmiddlewaretoken':  csrf_token
    };

    $.ajax({
        url: 'pagination/',
        type: 'POST',
        data: data,
        success: function (result) {
            var count_product = result['count_product'];
            var result_product = result['result_product'];
            update_paginator(parseInt(count_product), step);
            update_list(result_product);
        },
        error: function (e) {
            notify_error(e, true)
        }
    })
}

function update_paginator(count_product, step) {
    $("#paginator").html('');
    for (var i = 1; i <= count_product; i++) {
        if (i == step) {
            $("#paginator").append('<li class="uk-active"><span>' + i + '</span></li>');
        } else {
            $("#paginator").append('<li><a onclick="paginator(' + i + ')">' + i + '</a></li>');
        }
    }
}

function update_list(result_product) {
    var count = 1, top = 'top';
    for (var i = 0; i < result_product.length; i++) {
        try{
            sell = result_product[i]['price'][0]['sell']
        }catch (e){
            sell = 0
        }
        var sell = numberWithCommas(sell);
        var categories = "", full_name_category = '';
        for (var j in result_product[i]['categories']) {
            if (categories.length < 200) {
                categories += " " + result_product[i]['categories'][j]["name"];
                if (j != result_product[i]['categories'].length - 1) {
                    categories += " " + '<i class="panda panda-bold-left sh-breadcrumb-next"></i>';
                }
            }

        }

        for (var j in result_product[i]['categories']) {
            full_name_category += " " + result_product[i]['categories'][j]["name"];
            if (j != result_product[i]['categories'].length - 1) {
                    full_name_category += " " + '>';
                }
        }
        $("#products").append(
            '<tr id="' + result_product[i]['product_id'] + '">' +
                '<td>' + (count < 10 ? '0'+count : count) + '</td>' +
                '<td>VPID-' + result_product[i]['product_id'] + '</td>' +
                '<td  data-uk-tooltip="{pos: '+ top +'}" title="'+ result_product[i]['product_name_en']+'">...' + result_product[i]['product_name_en'].substring(0, 20) + '</td>' +
                '<td data-uk-tooltip="{pos: '+ top +'}" title="'+ full_name_category +'">' + categories + '</td>' +
                '<td>' + sell + '</td>' +
                '<td>' + result_product[i]['created_date'] + '</td>' +
                '<td>' + result_product[i]['modified_date'] + '</td>' +
                create_html_publish_span(result_product[i]['product_id'], result_product[i]['published']) +
                create_html_work_product(result_product[i]) +
            '</tr>'
        );
        count += 1
    }

    if(result_product.length == 0){
        $("#dash-sh-content").html('<p class="uk-text-center">هیچ محصولی پاک نشده است.</p>')
    }
}

function create_html_work_product(product){
    var html = "";
    if(perm != "None"){
        var product_id = product['product_id'];
        var published = product['published'];
        try {var sell = product['price'][0]['sell']}catch(e){sell = 0}
        try {var buy = product['price'][0]['buy']}catch(e){buy = 0}
        if(trash == 'False'){
            html =
                '<td class="uk-float-left sh-operations">' +
                create_html_publish_selector(published, product_id)+
                create_function_edit_product(product_id)+
                '<button class="sh-btn-6 uk-button" onclick="transitions_product(\'' + product_id + '\', \'True\')">' +
                '<i class="panda panda-bin"></i>' +
                'پاک کردن' +
                '</button>' +
                '</td>';
        }else{
            html =
                '<td class="uk-float-left sh-operations">' +
                '<button class="sh-btn-7 uk-button" onclick="transitions_product(\'' + product_id + '\', \'False\')">' +
                '<i class="panda panda-eye"></i>' +
                'بازگرداندن' +
                '</button>' +
                '<button class="sh-btn-6 uk-button" onclick="delete_product(\'' + product_id + '\')">' +
                '<i class="panda panda-bin"></i>' +
                'پاک کردن' +
                '</button>' +
                '</td>';
        }
    }
    return html
}

function create_function_edit_product(product_id) {

    if(content_product != "None" && supply_unit_perm != "None"){
        return  '<a class="sh-btn-7 uk-button" href="/products/update/'+product_id+'" style="margin-right:5px;">' +
                '<i class="panda panda-eye"></i>' +
                'ویرایش محتوا' +
                '</a>'+
                '<a class="sh-btn-7 uk-button" href="/products/create/'+product_id+'" style="margin-right:5px;">' +
                '<i class="panda panda-eye"></i>' +
                'ویرایش تامین' +
                '</a>';
    }else if(content_product != "None"){
        return  '<a class="sh-btn-7 uk-button" href="/products/update/'+product_id+'" style="margin-right:5px;">' +
                '<i class="panda panda-eye"></i>' +
                'ویرایش محتوا' +
                '</a>';
    }else if(supply_unit_perm != "None"){
        return  '<a class="sh-btn-7 uk-button" href="/products/create/'+product_id+'" style="margin-right:5px;">' +
                '<i class="panda panda-eye"></i>' +
                'ویرایش تامین' +
                '</a>';
    }else {
        return ""
    }
}

function create_html_publish_selector(published, product_id){
    var published_selected = "";
    if(content_product != "None"){
        published_selected =
            '<button class="uk-margin-small-left sh-bg-publish published_product_'+product_id+' sh-btn-7 uk-button '+((published) ? "uk-hidden" : "")+' " style="border:none;" onclick="changePublishedProduct(\''+ product_id +'\', true)">'+
                '<i class="panda panda-checkmark"></i>منتشر شود'+
            '</button>'+
            '<button class="uk-margin-small-left sh-bg-unpublish unpublished_product_'+product_id+' sh-btn-7 uk-button '+((!published) ? "uk-hidden" : "")+' " style="border:none;" onclick="changePublishedProduct(\''+ product_id +'\', false)">'+
                '<i class="panda panda-file-excel"></i>منتشر نشود'+
            '</button>';

    }
    return published_selected
}

function create_html_publish_span(product_id, published){
    var published_span = "";
    if(trash == 'False'){
        if (published == true)  {
            published_span = '<td id="span_publish_'+product_id+'"><div class="uk-badge sh-badge-success">منتشر شده</div></td>';
        } else {
            published_span = '<td id="span_publish_'+product_id+'"><div class="uk-badge sh-badge-warning">منتشر نشده</div></td>';
        }
    }
    return published_span
}


function transitions_product(product_id, trash_type) {
    var message_1 = "";
    var message_2 = "";
    if (trash_type == "True") {
        message_1 = 'آیا می خواهید به سطل زباله انتقال پیدا کند؟';
        message_2 = 'با موفقیت به سطل زباله ارسال شد.';
    } else {
        message_1 = "ایا می خواهید محصول بازگردانده شود؟";
        message_2 = "با موفقیت بازگردانده شد.";
    }
    UIkit.modal.confirm(message_1, function () {
        $.ajax({
            url: '/products/change_product_to_trash/',
            type: 'POST',
            data: {
                'product_id': product_id,
                'trash_type': trash_type,
                'csrfmiddlewaretoken': csrf_token
            },
            success: function (result) {
                if(result['message'] == 'success'){
                    $("#" + product_id).remove();
                    if(trash_type == "False"){
                        paginator(step);
                    }
                    uikitNotify(message_2, 'panda\&#32panda-checkmark2', 'success');
                }else {
                    notify_error(500, false)
                }
            },
            error: function (e) {
                notify_error(e, true)
            }
        });
    }, {labels: {'Ok': 'بلی', 'Cancel': 'خیر'}});
}

function delete_product(product_id) {
    UIkit.modal.confirm("آیا می خواهید محصول پاک شود؟", function () {
        $.ajax({
            url: '/products/delete/',
            type: 'POST',
            data: {
                'product_id': product_id,
                'csrfmiddlewaretoken': csrf_token
            },
            success: function (result) {
                uikitNotify('با موفقیت حذف شد', 'panda\&#32panda-cross', 'success');
                $("#" + product_id).remove();
            },
            error: function (e) {
                notify_error(e, true)
            }
        })
    }, {labels: {'Ok': 'بلی', 'Cancel': 'خیر'}});
}
