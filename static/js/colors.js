var csrf_token = "",
    colors = [],
    remove_colors = [];

function addColor(){
    var code = $('#selected_colors :selected').attr('data-code');
    var presentation = $('#selected_colors :selected').attr('data-presentation');

    var bool = checkColorNotRepeatedly(code);
    if(bool){
        colors.push({
            code: code,
            presentation:presentation
        });
        var html = '<a id="color_' + code + '" class="sh-btn-10 uk-button" style="margin-left:5px;"><i class="panda panda-cross" onclick="removeColor(\'' + code + '\')"></i>' + presentation + '</a>';
        $('#colors').append(html);

    }else {
        uikitNotify('رنگ تکراری می باشد.', 'panda\&#32panda-warning', 'warning');
    }
}
function checkColorNotRepeatedly(code){
    var bool = true;
    for (var item in colors) {
        if (colors[item]['code'] == code) {
            bool = false;
        }
    }
    return bool
}

function removeColor(color_code){
    for (var item in colors) {
        if (colors[item]['code'] == color_code) {
            var color_del = colors.splice(item, 1)[0];
            var bool = true;
            for (var rm in remove_colors) {
                if (remove_colors[rm]['code'] == color_del.code) {
                    bool = false;
                }
            }
            if(bool){
                remove_colors.push(color_del)
            }
        }
    }
    $('#color_' + color_code).remove();
}
