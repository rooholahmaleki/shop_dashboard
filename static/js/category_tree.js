var IdLastGroup = '',
    NameLastGroup = '',
    ListGroups = [],
    CategoryTree = '';

function create_default_category() {
    var select_option = '<option value="None">انتخاب کنید</option>';
    for (var key in CategoryTree) {
        select_option += '<option data-name="' + CategoryTree[key]['name'] + '" value="' + key + '">' + CategoryTree[key]['name'] + '</option>';
    }
    select_option = '<select style="width: 230px !important;" class="level_1 uk-margin-bottom kh" onchange="load_sub_group(1)">' + select_option + '</select>';
    $('#all_c_g').append(select_option);
    $('.sh-newpro-first').find('.uk-form-row ').removeClass('sh-added-newprod ');
    seletedBox('.level_1');
    // comboBox('.kh', 'kh');
}

function load_sub_group(level_class) {
    clean_groups(level_class);
    var parent = $('.level_' + level_class + ' :selected');
    IdLastGroup = parseInt(parent.val());
    NameLastGroup = parent.attr('data-name');
    var groups = getObjects(CategoryTree, 'id', parent.val());
    try {
        if (groups[0]['childs']) {
            level_class += 1;
            $('#add_group').attr('disabled', 'disabled');
            var sub_html = '<option value="None">انتخاب کنید</option>';
            for (var group in groups[0]['childs']) {
                sub_html += '<option data-name="' + groups[0]['childs'][group]['name'] + '" value="' + group + '">' + groups[0]['childs'][group]['name'] + '</option>';
            }
            sub_html = '<select class="level_' + level_class + ' uk-margin-large-right uk-margin-bottom"  style="width: 230px !important;" onchange="load_sub_group(' + level_class + ') ">' + sub_html + '</select>';
            $('#all_c_g').append(sub_html);
            seletedBox('.level_'+level_class);
        } else {
            $('.c-disabled').removeAttr('disabled');
        }
    } catch (e) {
        $('.c-disabled').attr('disabled', 'disabled');
    }
}

function clean_groups(counter) {
    $('.c-disabled').attr('disabled', 'disabled');
    while (true) {
        counter += 1;
        var class_name = $('.level_' + counter);
        if (class_name[0]) {
            class_name.remove();
        } else {
            break
        }
    }
}

function remove_group(id_group) {
    $('#' + id_group).remove();
    for (var item in ListGroups) {
        if (ListGroups[item]['id'] == id_group) {
            ListGroups.splice(item, 1)
        }
    }
}

function getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val));
        } else if (i == key && obj[i] == val || i == key && val == '') {
            objects.push(obj);
        } else if (obj[i] == val && key == '') {
            if (objects.lastIndexOf(obj) == -1) {
                objects.push(obj);
            }
        }
    }
    return objects;
}
