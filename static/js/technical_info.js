var csrf_token = '',
    id_technical_info = 1000,
    id_sub = 100000,
    technical_info_row = $('#technical_info_row').html(),
    title = $('#title').html();

function add_technical_info() {
    var html =
        '<div class="class_technical" id="id_technical_' + id_technical_info + '">' +
            title +
            '<a><i class="panda panda-cross close-technical-title" onclick="delete_technical_info(' + id_technical_info + ')"></i></a>' +
            '<div class="class_technical_sub" id="id_technical_sub_' + id_sub + '">' +
                 technical_info_row +
                '<a><i class="panda panda-cross close-technical-sub" onclick="delete_technical_sub(' + id_sub + ')"></i></a>' +
            '</div>' +
            '<a id="add_sub_' + id_technical_info + '" class="add_sub_form"><i class="panda panda-plus" onclick="add_sub_technical_info(' + id_technical_info + ')" ></i></a>' +
        '</div>' +
        '<hr id="hr_' + id_technical_info + '">';
    $("#technical_info").append(html);
    id_technical_info = id_technical_info + 1;
    id_sub = id_sub + 1;

    comboBox('.sample_form');
    comboBox('.combo-large', 'combo-large');
}

function add_sub_technical_info(id_technical_info) {
    var html =
        '<div class="class_technical_sub" id="id_technical_sub_' + id_sub + '">' +
             technical_info_row +
            '<a><i class="panda panda-cross close-technical-sub" onclick="delete_technical_sub(' + id_sub + ')"></i></a>' +
        '</div>';
    $("#id_technical_" + id_technical_info + " .add_sub_form").before(html);
    id_sub = id_sub + 1;

    comboBox('.sample_form');
}

function enter_technical_info() {
    var list_technical = [];
    var list_key_sub_title = [];
    var dict_error = {};
    $("#technical_info .class_technical").each(function () {
        var id = $(this).attr('id');
        var dict_technical = {
            'title': $(this).find('input[name="title"]').val(),
            'title_en': $(this).find('input[name="title_en"]').val(),
            'list_technical_sub': []
        };
        if(dict_technical['title'] != "" && dict_technical['title_en'] != ""){
            var list_technical_sub = [];
            $('#' + id + " .class_technical_sub").each(function () {
                var searchable = searchable_sub_technical($(this).find('input[name="searchable"]:checked')),
                    type_key = $(this).find('select[name="type_key"]').val(),
                    sub_title = $(this).find('input[name="sub_title"]').val(),
                    sub_title_en = $(this).find('input[name="sub_title_en"]').val(),
                    sub_description = $(this).find('input[name="sub_description"]').val(),
                    sub_description_en = $(this).find('input[name="sub_description_en"]').val();

                if(check_key_sub_title_en_no_repeat(sub_title_en, list_key_sub_title)){
                    list_key_sub_title.push(sub_title_en);
                    if(sub_title != "" && sub_title_en != "" && sub_description != "" && sub_description_en != ""){
                        list_technical_sub.push({
                            'searchable': searchable,
                            'type_key': type_key,
                            'title': sub_title,
                            'title_en': sub_title_en,
                            'description': sub_description,
                            'description_en': sub_description_en
                        })
                    }else{
                        dict_error['field'] = 'پر کردن تمامی فیلد ها الزامی می باشد';
                    }
                }else{
                    dict_error['key'] = 'کلید ها نباید همانند  هم باشند.';
                }
            });
            if(!$.isEmptyObject(list_technical_sub)){
                dict_technical['list_technical_sub'] = list_technical_sub;
                list_technical.push(dict_technical);
            }
        }else{
            dict_error['field'] = 'پر کردن تمامی فیلد ها الزامی می باشد';
        }
    });
    if(!$.isEmptyObject(dict_error)){
        for(var item in dict_error){
            uikitNotify(dict_error[item], 'panda\&#32panda-cross', 'danger');
        }
        list_technical = [];
        return list_technical
    }else{
        return list_technical;
    }
}

function check_key_sub_title_en_no_repeat(sub_title_en, list_key_sub_title){
    var bool = true;
    list_key_sub_title.forEach(function (element) {
        if(element == sub_title_en){
           bool = false
        }
    });
    return bool
}

function searchable_sub_technical(event){
    return event.length > 0;
}

function delete_technical_sub(id_technical_sub) {
    var technical_sub = $("#id_technical_sub_" + id_technical_sub);
    var count_child_father = technical_sub.parent().children('.class_technical_sub').length;
    if (count_child_father == 1) {
        var parent_id = technical_sub.parent().attr('id');
        parent_id = parent_id.split('_');
        parent_id = parent_id[parent_id.length - 1];
        delete_technical_info(parent_id);
    }
    technical_sub.remove();
}

function delete_technical_info(id_technical_info) {
    $("#id_technical_" + id_technical_info).remove();
    $("#hr_" + id_technical_info).remove();
}

function add_sample_form(event) {
    event.preventDefault();
    var sample_form = enter_technical_info();
    if(!$.isEmptyObject(sample_form)){
        var data = {
                'update': $('#update').val(),
                'id_sub_group': IdLastGroup,
                'change_filter_key': $('#change_filter_key').val(),
                'technical_info': JSON.stringify(sample_form),
                'csrfmiddlewaretoken': csrf_token
        };
        $.ajax({
            url: '/form/add/',
            type: 'POST',
            data: data,
            success: function (result) {
                if(result['message'] == 'repeat_error'){
                    uikitNotify('برای این زیر گروه قبلا فرم طراحی شده است.', 'panda\&#32panda-warning', 'warning');
                }else if(result['message'] == 'insert'){
                    uikitNotify('فرم ساخته شد.', 'panda\&#32panda-checkmark2', 'success');
                    window.location.href = '/form/edit/'+result['id']
                }else if(result['message'] == 'update'){
                    uikitNotify('فرم ویرایش شد', 'panda\&#32panda-checkmark2', 'success');
                    $('#change_filter_key').val('not_changed')
                }
            },
            error: function (e) {
                notify_error(e, true)
            }
        })
    }else {
        uikitNotify('فرم خود را طراحی کنید.', 'panda\&#32panda-warning', 'warning');
    }
}

function change_filter_key(){
    $('#change_filter_key').val('changed');
}
