var modal = UIkit.modal("#form_brand_modal",{bgclose:false, center:true}),
    csrf_token = '',
    list_category_group = [];

bind_input_en();

bind_input_fa();

function add_or_edit_brand() {
    var form_data = new FormData($('#form_add_brand')[0]);
    var fa_data = check_input_char_string($('#id_fa_name').val(), 'fa');
    var en_data = check_input_char_string($('#id_en_name').val(), 'en');
    if(check_list_category_group_not_empty() && fa_data && en_data){
        form_data.append('category_group', JSON.stringify(list_category_group));
        $.ajax({
            url: '/brand/add_brand/',
            type: 'POST',
            processData: false,
            contentType: false,
            data: form_data,
            dataType: 'json',
            success: function (data) {
                if (data['message'] == 'update' || data['message'] == 'insert') {
                    $('#brand_'+data['brand_id']).remove();

                    $("#brand tr:first").before(create_html_brand_to_show(data));

                    var count = 0;
                    $('#brand .count_brand').each(function () {
                        $(this).html(count+=1);
                    });
                    modal.hide();
                    if(data['message'] == 'update'){
                         uikitNotify('برند با موفقیت ویرایش شد.', 'panda\&#32panda-checkmark2', 'success');
                    }else {
                         uikitNotify('برند شما با موفقیت اضافه شد.', 'panda\&#32panda-checkmark2', 'success');
                    }

                } else if(data['message'] == 'form_error') {
                    for (var value in data['error']) {
                        uikitNotify(data['error'][value], 'panda\&#32panda-cross', 'danger');
                    }
                }else if(data['message'] == 'repeat_error'){
                    uikitNotify('این برند قبلا ثبت شده است.', 'panda\&#32panda-warning', 'warning');

                }else if(data['message'] == 'query_error'){
                    uikitNotify('لطفا از واژگان نامناسب استفاده نکنید.', 'panda\&#32panda-warning', 'warning');

                }else if(data['message'] == 'not_brand'){
                    uikitNotify('کلمه انتخابی شما برند نیست', 'panda\&#32panda-warning', 'warning');
                }
            },
            error: function (e) {
                notify_error(e, true);
                console.log(e, true)
            }
        });
    }
}

function check_input_char_string(string, language){
    var regex = '';
    var message = '';
    if(language == 'en'){
        regex = /^([A-Za-z0-9-!$%@#^&*()_+|~=`{}\[\]:";'<>?,.\/])/;
        message = "نام انگلیسی درست وارد نشده است."
    }else{
        regex = /^[\u0600-\u06FF\s0-9+]+$/;
        message = "نام فارسی درست وارد نشده است."
    }
    if (regex.test(String(string))) {
        return true;
    }
    uikitNotify(message, 'panda\&#32panda-warning', 'warning');
    return false
}

function check_list_category_group_not_empty(){
    if(list_category_group.length <= 0){
        uikitNotify('حداقل یک رسته و گروه مشخص کنید.', 'panda\&#32panda-warning', 'warning');
        return false
    }
    return true
}

function create_html_brand_to_show(data){
    var date = new Date();
    data['image'] = data['image']+"?"+date.getTime();
    var temp =
        '<tr id="brand_'+data['brand_id']+'">' +
        '<td class="count_brand">-1</td>' +
        '<td>' + data['fa_name'] + '</td>' +
        '<td>' + data['en_name'] + '</td>' +
        '<td><img style="height: 24px;" src="' + data['image'] + '"></td>' +
        '<td class="uk-float-left">' +
            '<button class="sh-btn-7 uk-button" onclick="set_data_to_modal_for_edit(\'' + data['brand_id'] + '\', \'brands\')"><i class="panda panda-pencil"></i>ویرایش</button>'+
            '<button class="sh-btn-6 uk-button" onclick="delete_brand(\'' + data['brand_id'] + '\', \'delete_brand\', \'None\')"><i class="panda panda-bin"></i>پاک کردن</button>' +
        '</td>' +
        '</tr>';
    return temp
}

function delete_brand(brand_id, type_action, user_id) {
    var message = "آیا می خواهید برند پاک شود؟";
    if(type_action == 'word_filtering'){
        message = "آیا می خواهید به واژگان نامناسب اضافه شود؟";
    }else if(type_action == 'brand_filtering'){
        message = 'مطمین هستید که برند نامعتبر می باشد؟';
    }else if(type_action == 'category_group'){
        message = 'آیا رسته و گروه انتخابی برای برند نیست؟';
    }
    UIkit.modal.confirm(message, function () {
        var data = {
                'brand_id': brand_id,
                'type_action': type_action,
                'user_id': user_id,
                'csrfmiddlewaretoken': csrf_token
        };
        $.ajax({
            url: '/brand/delete_brand/',
            type: 'POST',
            data: data,
            success: function (data) {
                if(data['message'] == 'query_error'){
                     notify_error(500, false)
                }
                else if(data['message'] == 'delete'){
                    $('#brand_' + brand_id).remove();
                    uikitNotify('برند انتخابی پاک شد.', 'panda\&#32panda-cross', 'success');
                }
                else if(data['message'] == 'update'){
                    $('#brand_' + brand_id).remove();
                    uikitNotify('رسته و گروه برای برند تایید نشد.', 'panda\&#32panda-cross', 'danger');
                }
            },
            error: function (e) {
                notify_error(e, true)
            }
        });
    }, {labels:{ 'Ok': 'بلی', 'Cancel': 'خیر'}});
}

function clear_modal_data(){
    list_category_group = [];
    $('#all_category_group').html('');
    $('#image').attr("src", '');
    $('#id_fa_name').val('');
    $('#id_en_name').val('');
    $('#id_brand_id').val('');
    $('#id_page_name').val('');
}

function set_data_to_modal_for_edit(brand_id, page_name){
    $.ajax({
        url: '/brand/find_brand_by_id/',
        type: 'POST',
        data: {
            'brand_id': brand_id,
            'csrfmiddlewaretoken': csrf_token
        },
        success: function (data) {
            if(data['message'] == 'query_error'){
                 notify_error(500, false)
            }else{
                var date = new Date();
                $('#id_fa_name').val(data['fa_name']);
                $('#id_en_name').val(data['en_name']);
                $('#id_brand_id').val(brand_id);
                $('#id_page_name').val(page_name);
                $('#image').attr('src', data['image']+"?"+date.getTime());
                var all_category_group = '';
                for(var item in data['category_group']){
                    var bg_color = "";
                    if(data['category_group'][item]['status']){
                        bg_color = 'background-color: #ffe5e4;';
                        delete data['category_group'][item]['status']
                    }
                    all_category_group += append_html_category_group_to_modal(
                        data['category_group'][item]['category_name'],
                        data['category_group'][item]['group_name'],
                        data['category_group'][item]['id'],
                        bg_color
                    );
                    list_category_group.push(data['category_group'][item])
                }
                $('#all_category_group').append(all_category_group);

                modal.show();
            }
        },
        error: function (e) {
            notify_error(e, true)
        }
    });
}

function delete_brand_word(word_id){
    UIkit.modal.confirm("آیا مطمین هستید؟", function () {
        var data = {
                'word_id': word_id,
                'csrfmiddlewaretoken': csrf_token
        };
        $.ajax({
            url: '/brand/delete_brand_word/',
            type: 'POST',
            data: data,
            success: function (data) {
                if(data['message'] == 'query_error'){
                     notify_error(500, false)
                }else{
                    $('#'+word_id).remove();
                     uikitNotify('از واژگان فیترینگ برند حذف شد.', 'panda\&#32panda-cross', 'success');
                }
            },
            error: function (e) {
                notify_error(e, true)
            }
        });
    }, {labels:{ 'Ok': 'بلی', 'Cancel': 'خیر'}});
}

function add_category_group_to_brand(){
    var selector_category = $('#id_category option:selected'),
        selector_group = $('#id_group option:selected'),
        category = selector_category.val(),
        group = selector_group.val(),
        category_name = selector_category.text(),
        group_name = selector_group.text(),
        dict_category_group_data = {
            'category': category,
            'category_name': category_name,
            'group': group,
            'group_name': group_name
        },
        id_category_group = add_category_group_to_list(dict_category_group_data);
    if(id_category_group){
        var html = append_html_category_group_to_modal(category_name, group_name, id_category_group, "");
        $('#all_category_group').append(html)
    }else{
       uikitNotify('تکراری می باشد.', 'panda\&#32panda-warning', 'warning');
    }
}

function append_html_category_group_to_modal(category_name, group_name, id_category_group, bg_color){
    var html =
        '<tr id="'+id_category_group+'">' +
            '<td><div style="'+bg_color+'">'+category_name+'</div></td>'+
            '<td><div style="'+bg_color+'">'+group_name+'</div></td>'+
            '<td><div style="'+bg_color+'"><a class="sh-btn-6 uk-button" onclick="delete_category_group(\''+id_category_group+'\')"><i class="panda panda-bin"></i>پاک کردن</a></div></td>'+
        '</tr>';
    return html
}

function delete_category_group(id_category_group){
    for(var i in list_category_group){
        if(list_category_group[i]['id'] == id_category_group){
            list_category_group.splice(i, 1);
            break;
        }
    }
    $('#'+id_category_group).remove();
}

function add_category_group_to_list(dict_category_group_data){
    var category = dict_category_group_data.category;
    var group = dict_category_group_data.group;
    category = (category.length < 2) ? "0"+category:category;
    group = (group.length < 2) ? "0"+group:group;
    var final_id = category+group;
    var bool = false;
    for(var item in list_category_group){
        if(list_category_group[item]['id'] == final_id){
            bool = true;
            break;
        }
    }
    if(bool) {
        return false
    }else{
        dict_category_group_data['id'] = final_id;
        list_category_group.push(dict_category_group_data);
        return final_id
    }
}

function check_input_char(event, language){
    var regex = '';
    if(language == 'en'){
        regex = /^([A-Za-z0-9-!$%@#^&*()_+|~=`{}\[\]:";'<>?,.\/])/;
    }else{
        regex = /^[\u0600-\u06FF\s0-9+]+$/;
    }
    var key = String.fromCharCode(event.which);
    if (event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39 || regex.test(key)) {
        return true;
    }
    return false
}

function bind_input_en(){
    $("#id_en_name").one("keypress", function (event) {
        if(check_input_char(event, 'en')){
            return true
        }
        uikitNotify('نام انگلیسی وارد کنید.', 'panda\&#32panda-warning', 'warning');
        return false;
    });
}

function bind_input_fa(){
    $("#id_fa_name").one("keypress", function (event) {
        if(check_input_char(event, 'fa')){
            return true
        }
        uikitNotify('نام فارسی وارد کنید', 'panda\&#32panda-warning', 'warning');
        return false;
    });
}

$("#id_en_name").blur(function(){
    bind_input_en()
});

$("#id_fa_name").blur(function(){
    bind_input_fa()
});

$('.cancel').on("click", function (e) {
    modal.hide();
});

modal.on({
    'hide.uk.modal': function(){
        clear_modal_data();
    }
});
