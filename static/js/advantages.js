var id_advantages = 1000,
    id_disadvantages = 100000,
    advantages_input = $('#advantages_input').html(),
    disadvantages_input = $('#disadvantages_input').html();

function addAdvantages(){
    var add_advantages = document.getElementById('add_advantages');
    $('#add_advantages').remove();
    var html =
        '<div style="display: inline-flex" id="id_advantages_' + id_advantages + '">' +
             advantages_input+
            '<a><i class="panda panda-cross" onclick="deleteAdvantages(' + id_advantages + ')"></i></a>' +
        '</div>';
    $('#advantages').append(html);
    $('#id_advantages_'+id_advantages).prepend(add_advantages);
    id_advantages += 1
}

function deleteAdvantages(id_advantages){
    var selector = "#id_advantages_" + id_advantages;
    var is_last_sub = $(selector).children('#add_advantages');
    if (is_last_sub) {
        var id_last = $("#id_advantages_" + id_advantages).parent().children("div :nth-last-child(2)").attr('id');
        $('#' + id_last).prepend(is_last_sub[0]);
    }
    $(selector).remove();
}

function addDisadvantages(){
    var add_disadvantages = document.getElementById('add_disadvantages');
    $('#add_disadvantages').remove();
    var html =
        '<div style="display: inline-flex" id="id_disadvantages_' + id_disadvantages + '">' +
             disadvantages_input+
            '<a><i class="panda panda-cross" onclick="deleteDisAdvantages(' + id_disadvantages + ')"></i></a>' +
        '</div>';
    $('#disadvantages').append(html);
    $('#id_disadvantages_'+id_disadvantages).prepend(add_disadvantages);
    id_disadvantages += 1
}

function deleteDisAdvantages(id_disadvantages){
    var selector = "#id_disadvantages_" + id_disadvantages;
    var is_last_sub = $(selector).children('#add_disadvantages');
    if (is_last_sub) {
        var id_last = $("#id_disadvantages_" + id_disadvantages).parent().children("div :nth-last-child(2)").attr('id');
        $('#' + id_last).prepend(is_last_sub[0]);
    }
    $(selector).remove();
}

function getAdvantages(){
    var list_advantages = [];
    $("#advantages .advantages").each(function () {
        if($(this).val()){
            list_advantages.push($(this).val())
        }
    });
    return list_advantages
}

function getDisAdvantages(){
    var list_disadvantages = [];
    $("#disadvantages .disadvantages").each(function () {
        if($(this).val()){
            list_disadvantages.push($(this).val())
        }
    });
    return list_disadvantages
}
