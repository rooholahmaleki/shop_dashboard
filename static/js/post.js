var csrf_token = "",
    reports =  [],
    reports_modal =  UIkit.modal('#show-reports',{center:true, bgclose:false}),
    message_modal =  UIkit.modal('#send-message',{center:true, bgclose:false});

function delete_post(post_id) {
    UIkit.modal.confirm("آیا می خواهید پست پاک شود؟", function () {
        $.ajax({
            url: "/post/delete_post/",
            type: "POST", // http method
            dataType: 'json',
            data: {
                'post_id': post_id,
                'csrfmiddlewaretoken': csrf_token
            },
            success: function (data) {
                if (data['message'] == 'error') {
                    uikitNotify('با عرض پوزش خطای سیستمی رخ داده است.', 'panda\&#32panda-cross', 'danger');
                } else {
                    $("#" + post_id).remove();
                    uikitNotify('باموفقیت پست پاک شد.', 'panda\&#32panda-cross', 'success');
                }
            },
            error: function () {
                uikitNotify('با عرض پوزش خطای سیستمی رخ داده است.', 'panda\&#32panda-cross', 'danger');
            }
        });
    } ,{labels:{ 'Ok': 'بلی', 'Cancel': 'خیر'}});
}

function check_post(post_id) {
    UIkit.modal.confirm("آیا پست بررسی شده است؟", function () {
        $.ajax({
            url: "/post/delete_report_post/",
            type: "POST", // http method
            dataType: 'json',
            data: {
                'post_id': post_id,
                'csrfmiddlewaretoken': csrf_token
            },
            success: function (data) {
                if (data['message'] == 'error') {
                    uikitNotify('با عرض پوزش خطای سیستمی رخ داده است.', 'panda\&#32panda-cross', 'danger');
                } else {
                    $("#" + post_id).remove();
                    uikitNotify('پست چک شد.', 'panda\&#32panda-checkmark2', 'success');
                }
            },
            error: function () {
                uikitNotify('با عرض پوزش خطای سیستمی رخ داده است.', 'panda\&#32panda-cross', 'danger');
            }
        });
    },{labels:{ 'Ok': 'بلی', 'Cancel': 'خیر'}});
}

function set_reasons_reports(_reports) {
    reports = JSON.parse(_reports);
    for(var key in reports){
        $('#reasons_'+key).html(reports[key])
    }
    reports_modal.show();
}

$('#show-reports').on({
    'hide.uk.modal': function(){
        for(var key in reports){
            $('#reasons_'+key).html(0)
        }
    }
});

function send_message() {
    var message = $('textarea#message-value').val();
    var user_id = $('#send-message #user_id').val();
    if (message.trim() == "") {
        uikitNotify('متن ارسالی شما نباید خالی باشد.', 'panda\&#32panda-warning', 'warning');
    } else {
        sendRequest(message, user_id);
    }
}

function sendRequest(message, user_id) {
    $.ajax({
        url: '/post/send_message/',
        type: 'POST',
        dataType: 'json',
        data: {
            message: message,
            user_id: user_id,
            'csrfmiddlewaretoken': csrf_token
        },
        success: function (data) {
            if (data['message'] == 'success') {
                message_modal.hide();
                $('textarea#message-value').val('');
                uikitNotify('پیام شما با موفقیت ارسال شد.', 'panda\&#32panda-checkmark2', 'success');
            } else {
                notify_error(500, false)
            }
        },
        error: function (e) {
            notify_error(e, true)
        }
    });
}

function set_data_user_to_modal_message(full_name, image, mongo_id){
    var html = '<img class="uk-border-circle uk-margin-small-left" src="'+image+'" width="32" height="32" alt="'+full_name+'">' +
                full_name;
    $('#send-message #data_user').html(html);
    $('#send-message #user_id').val(mongo_id);
    message_modal.show();
}
