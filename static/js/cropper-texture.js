(function () {
    var $image = $('.cropper-texture');
    var options = {
            aspectRatio: 2 / 2,
            preview: '.img-preview',
            cropBoxResizable:true,
            movable: true
    };

    $image.on().cropper(options);

    var $inputImage = $('#inputImageTexture'),
        URL = window.URL || window.webkitURL;

    $inputImage.change(function () {
        var files = this.files;
        var file = files[0];
        var blobURL;
        var reader = new FileReader();
        var image = new Image();
        reader.readAsDataURL(file);
        reader.onload = function (_file) {
            image.src = _file.target.result;              // url.createObjectURL(file);
            image.onload = function () {
                if (files && files.length) {
                    if (/^image\/\w+$/.test(file.type)) {
                        blobURL = URL.createObjectURL(file);
                        $image.one('built.cropper', function () {
                            URL.revokeObjectURL(blobURL); // Revoke when load complete
                        }).cropper('reset').cropper('replace', blobURL);
                        $inputImage.val('');
                        $('#image-main-i').hide()
                    }
                }
            }
        };
    });

}());
