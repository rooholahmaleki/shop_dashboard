/**
 * Created by rmaleki on 4/11/16.
 */
function aside_dynamic(id_clicked_tab, id_clicked_sub_tab, id_clicked_sub_sub_tab) {
    setTimeout(function () {
            var selector = "#" + id_clicked_tab;
            $(selector).click();
            $("#" + id_clicked_sub_tab).css("color", "#fff");
            $(selector).css("color", "#fff");
            if(id_clicked_sub_sub_tab){
                $('#'+id_clicked_sub_sub_tab).css("color", "#fff");
            }
        }, 0 );
};


function numberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function notify_error(error, bool) {
    if(bool){
        if(error.status == 403){
            if(error.responseText == "Forbidden"){
                uikitNotify('شما دسترسی به این عمل را ندارید.', 'panda\&#32panda-warning', 'warning');
            }else {
                uikitNotify('csrf_token', 'panda\&#32panda-warning', 'warning');
            }

        }else if(error.status == 500){
            uikitNotify('با عرض پوزش سیستم با مشکل رو به رو شده است.', 'panda\&#32panda-cross', 'danger');

        }else if(error.status == 404){
            uikitNotify('با عرض پوزش خطای سیستمی رخ داده است.', 'panda\&#32panda-cross', 'danger');
        }
    }else {
        if(error == 500){
            uikitNotify('با عرض پوزش سیستم با مشکل رو به رو شده است.', 'panda\&#32panda-cross', 'danger');
        }
    }
    return error
}
