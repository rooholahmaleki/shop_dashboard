$(".sh-asidebar").mCustomScrollbar({
    theme:"minimal-dark",
});

$('.tm-content').mCustomScrollbar({
    theme:"minimal-dark",
});
function uikitNotify(message, icon, status, timeout, pos){
    UIkit.notify({
        message : "<div class='uk-grid uk-grid-collapse uk-flex uk-flex-middle'><i class=" + icon + "></i><p>"+ message +"</p></div>",
        status  : typeof status == 'undefined'?'info':status,
        timeout : typeof timeout == 'undefined'?5000:timeout,
        pos     : typeof pos == 'undefined'?'bottom-left':pos
    });
}

function scrollCounter() {
    var windowHeight = window.innerHeight,
        footer = $('footer').innerHeight();

    $('.tm-content').css('height', windowHeight);
}

// $(document).ready(function() {
//     scrollCounter();
// });

// $(window).resize(function() {
//     scrollCounter();
// });

$.UIkit.langdirection = UIkit.$html.attr('dir') == "rtl" ? "left" : "right";

//function getRandomInt(min, max) {
//    return Math.floor(Math.random() * (max - min + 1)) + min;
//}

//setInterval(function(){
//    x = Math.floor(Math.random() * 100) + 1;
//    $('.uk-progress-bar').css('width', x + "%");
//    $('.sh-progress-status > p > span').text(x + "%");
//}, 1000);

window.formatPersian = false;

function startTime() {
    var x = persianDate().format("dddd , DD MMMM YYYY | H:mm:ss");
    $('#tm-date-and-time').text(x);

    t = setTimeout(function () {
        startTime();
    }, 1000);
}
startTime();

// Combo Box
    function comboBox(selector, customClass) {
        $(selector).selectbox({
            onOpen: function (inst) {
                $(this).next().children('.sbSelector').css('border-color', '#61beb3');
            },
            onClose: function (inst) {
                $(this).next().children('.sbSelector').css('border-color', '');
            },
            classToggle: 'sbToggle panda panda-bottom-2',
            classOptions: 'sbOptions uk-list sh-scroll-' + customClass,
            classHolder: typeof customClass == 'undefined'?'sbHolder':customClass
        });

        $(".sh-scroll-" + customClass).mCustomScrollbar();
    }

function seletedBox(selector) {
    $(selector).selectBox({
        menuTransition: 'fade',
        menuSpeed:'fast'
    });
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
