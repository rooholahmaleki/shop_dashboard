function add_image(id_image) {
    'use strict';
    $('#image_selected').val(id_image);
    $('.img-preview').html('');

    (function () {
        var $image = $('.cropper');
        var options = {
                aspectRatio: 378 / 419,
                preview: '.img-preview',
                zoomable: false,
                minCropBoxWidth: 200,
                minCropBoxHeight: 200,
                cropBoxResizable:true,
                dragCrop: false,
                background: false,
                movable: false,
                guides:false
        };

        $image.on().cropper(options);

        var $inputImage = $('#inputImage'),
            URL = window.URL || window.webkitURL;

        $inputImage.change(function () {
            var files = this.files;
            var file = files[0];
            readImage(file, files);
        });

        function readImage(file, files) {
            var blobURL;
            var reader = new FileReader();
            var image = new Image();
            reader.readAsDataURL(file);
            reader.onload = function (_file) {
                image.src = _file.target.result;              // url.createObjectURL(file);
                image.onload = function () {
                    // var w = this.width,
                    //     h = this.height;
                    // if (w >= 1512 && h >= 1676) {
                        if (files && files.length) {
                            if (/^image\/\w+$/.test(file.type)) {
                                blobURL = URL.createObjectURL(file);
                                $image.one('built.cropper', function () {
                                    URL.revokeObjectURL(blobURL); // Revoke when load complete
                                }).cropper('reset').cropper('replace', blobURL);
                                $inputImage.val('');
                                $('#image-main-i').hide()
                            } else {
                                $('#image-main-i').show();
                                uikitNotify('لطفا عکس انتخاب کنید.', 'panda\&#32panda-warning', 'warning');
                            }
                        }
                    // } else {
                    //     uikitNotify('عکس شما حداقل باید در اندازه 1676*1512 باشد.', 'panda\&#32panda-warning', 'warning');
                    // }
                };
            };

        }
    }());
}
