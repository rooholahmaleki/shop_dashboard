var csrf_token = '';

function technical_info_product() {
    var list_technical = [];
    var dict_error = {};
    $("#technical_info .class_technical").each(function () {
        var id = $(this).attr('id');
        var dict_technical = {
            'title': $(this).find('input[name="title"]').val(),
            'title_en': $(this).find('input[name="title_en"]').val(),
            'list_technical_sub': []
        };
        var list_technical_sub = [];
        $('#' + id + " .class_technical_sub").each(function () {
            var searchable = searchable_sub_technical($(this).find('input[name="searchable"]').val()),
                sub_title = $(this).find('input[name="sub_title"]').val(),
                sub_title_en = $(this).find('input[name="sub_title_en"]').val(),
                sub_description = $(this).find('input[name="sub_description"]').val(),
                type_key = $(this).find('input[name="type_key"]').val(),
                sub_description_en = $(this).find('input[name="sub_description_en"]').val();
                list_technical_sub.push({
                    'searchable': searchable,
                    'title': sub_title,
                    'title_en': sub_title_en,
                    'type_key': type_key,
                    'description': sub_description,
                    'description_en': sub_description_en
                });
        });
        if(!$.isEmptyObject(list_technical_sub)){
            dict_technical['list_technical_sub'] = list_technical_sub;
            list_technical.push(dict_technical);
        }

    });
    if(!$.isEmptyObject(dict_error)){
        for(var item in dict_error){
            uikitNotify(dict_error[item], 'panda\&#32panda-cross', 'danger');
        }
        return false
    }else{
        return list_technical;
    }
}

function searchable_sub_technical(bool){
    return bool == "True";
}
