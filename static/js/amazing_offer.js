var csrf_token = "";
function add_amazing_offer() {
    var formdata = new FormData($('#amazing_offer_form')[0]);
    $.ajax({
        url: '/amazing_offer/add/',
        type: 'POST',
        processData: false,
        contentType: false,
        data: formdata,
        dataType: 'json',
        success: function (result) {
            if (result['message'] == 'success') {
                if (result['value'] == 'insert') {
                    $('#id_update').val('yes');
                    uikitNotify('محصول با موفقیت اضافه شد.', 'panda\&#32panda-checkmark2', 'success');
                } else {
                    uikitNotify('محصول به روز رسانی شد.', 'panda\&#32panda-checkmark2', 'success');
                }

            } else if (result['message'] == 'form_error') {
                for (var value in result['value']) {
                    uikitNotify(result['value'][value], 'panda\&#32panda-cross', 'danger');
                }
                if ($.isEmptyObject(result['value'])) {
                    uikitNotify('عکس انتخاب نشده است.', 'panda\&#32panda-warning', 'warning');
                }

            } else if (result['message'] == 'repeat_error') {
                uikitNotify('این محصول پیشنهاد ویژه شده است', 'panda\&#32panda-warning', 'warning');

            } else if (result['message'] == 'query_error') {
                notify_error(500, false)

            } else if (result['message'] == 'image_error') {
                uikitNotify('عکس انتخاب نشده است', 'panda\&#32panda-warning', 'warning');
            }
        },
        error: function (e) {
            notify_error(e, true)
        }
    });
}

function search_product() {
    var product_id = $('#product_id').val();
    if (product_id != "") {
        $.ajax({
            url: "/products/find/one/",
            type: "POST", // http method
            dataType: 'json',
            data: {
                'product_id': product_id,
                'csrfmiddlewaretoken': csrf_token
            },
            success: function (data) {
                if (data['message'] == 'success') {
                    $('.sh-result-search').removeClass('uk-hidden');
                    var product = data['product'];
                    $('#product_name_fa').html(product['product_name_fa']);
                    $('#product_name_en').html(product['product_name_en']);
                    $('#product_image').attr('src', product['image']);
                    $('#id_product_id').val(product['product_id']);

                    if (data["price"] != "None") {
                        $('#product_customer_price').html(numberWithCommas(product['price']['customer_price']));
                        $('#product_buy').html(numberWithCommas(product['price']['buy']));
                        $('#product_sell').html(numberWithCommas(product['price']['sell']));
                    }
                } else {
                    $('.sh-result-search').addClass('uk-hidden');
                    uikitNotify('محصول مورد نظر یافت نشد.', 'panda\&#32panda-warning', 'warning');
                }
            },
            error: function () {
                uikitNotify('با عرض پوزش خطای سیستمی رخ داده است.', 'panda\&#32panda-cross', 'danger');
            }
        });
    } else {
        uikitNotify('کد محصول را وارد کنید.', 'panda\&#32panda-warning', 'warning');
    }
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#banner').attr('src', e.target.result);
            $('#banner').parent().children('i').addClass('uk-hidden');
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$("#id_image").change(function () {
    readURL(this);
});
