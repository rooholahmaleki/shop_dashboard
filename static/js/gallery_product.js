var list_gallery = [],
    id_gallery = 50000,
    id_image = 90000,
    modal_image_crop = UIkit.modal("#modal-image-crop",{bgclose:false, center:true}),
    modal_crop_thumbnail_150_170 = UIkit.modal("#modal-crop-thumbnail-150-170",{bgclose:false, center:true}),
    modal_crop_thumbnail_97_97 = UIkit.modal("#modal-crop-thumbnail-97-97",{bgclose:false, center:true}),
    len_gallery_count = "",
    add_color_image = $('#add_color_image').html(),
    error_gallery = [],
    list_select = [];


$('#add_gallery').on('click', function () {
    $(create_gallery_html(id_gallery)).insertBefore($('#add_gallery'));
    id_gallery += 1;
    id_image += 6;
    $('.select_color').select2({'width': '100%'});
});

function set_gallery() {
    list_select = [];
    list_gallery = [];
    error_gallery = [];
    create_gallery_to_save_db();
    if (gallery_no_has_error() && check_has_main_gallery()) {
        return list_gallery;
    }
    return false
}

function load_image_gallery(gallery) {
    gallery = JSON.stringify(gallery);
    $.ajax({
        data: {
            'gallery': gallery,
            'csrfmiddlewaretoken': getCookie('csrftoken')
        },
        url: '/products/load_gallery/',
        type: 'POST',
        success: function (result) {
            if(result['gallery']){
                append_galleries(result);
            }
        },
        error: function (e) {
                notify_error(e, true)
        }
    })
}

function append_galleries(result) {
    var gallery = result['gallery'],
        selector_gal = '#id_gallery_'+gallery['id'],
        list_image_to_show = "";

    list_image_to_show += append_image_from_gallery_to_list_image(gallery);
    list_image_to_show += append_default_to_list_image(5 - gallery['list_image'].length);
    var gallery_html = create_gallery_and_add_image(list_image_to_show, gallery);
    $("#div_add_gallery").parent().children('#div_add_gallery').before(gallery_html);
    $(selector_gal).find('select > option[value='+gallery['color']+']').attr('selected', 'selected');
    if(gallery['main_gallery'] == 'True'){
        $(selector_gal).find("input[type=radio]").prop('checked', true);
    }
    remove_spinner_gallery();
    $('.select_color').select2({'width': '100%'});
}

function append_image_from_gallery_to_list_image(gallery){
    var len_list_image = gallery['list_image'].length;
    var list_image_to_show = "";
     for(var i = 0 ; i < len_list_image ; i++){
        var image_id_id = gallery['list_image'][i]['id'],
            data_val_large = gallery['list_image'][i]['data_val_large'],
            data_val_150x170 = gallery['list_image'][i]['data_val_150x170'],
            data_val_97x97 = gallery['list_image'][i]['data_val_97x97'],
            image_small = gallery['list_image'][i]['small'],
            image_thumbnail_97x97 = gallery['list_image'][i]['thumbnail_97x97'],
            image_thumbnail_150x170 = gallery['list_image'][i]['thumbnail_150x170'];
        list_image_to_show +=
                '<div class="uk-width-1-3 gallery_image" data-id-image="'+ image_id_id +'" id="image_' + image_id_id + '">'+
                    '<input id="thumbnail_input_' + image_id_id + '" type="hidden" value="data:image/jpeg;base64,'+data_val_large+'">'+
                    '<a class="sh-delete-image" onclick="deleteImage(\'' + image_id_id + '\')">'+
                        '<i class="panda panda-cross"></i>'+
                    '</a>'+
                    '<a class="sh-primary-pic" onclick="add_image(\'' + image_id_id + '\')" href="#modal-image-crop" data-uk-modal="{center:true, bgclose:false}">'+
                        '<img data-val-large="file,'+data_val_large+'" id="main_image_' + image_id_id + '" src="'+image_small+'">'+
                        '<i id="i_378_' + image_id_id + '" class="panda panda-plus uk-hidden"><p id="p_378_' + image_id_id + '">عکس اصلی</p></i>'+
                    '</a>'+
                    '<a class="sh-thumbnail-170" onclick="add_thumbnail(\'' + image_id_id + '\',' + 170 + ')">'+
                        '<img data-val="file,'+data_val_150x170+'" id="image_thumbnail_150x170_' + image_id_id + '" src="'+image_thumbnail_150x170+'">'+
                        '<i id="i_th_170_' + image_id_id + '" class="panda panda-plus uk-hidden"><p id="p_th_170_' + image_id_id + '">thumbnail</p></i>'+
                    '</a>'+
                    '<a class="sh-thumbnail-97" onclick="add_thumbnail(\'' + image_id_id + '\',' + 97 + ')">'+
                        '<img data-val="file,'+data_val_97x97+'" id="image_thumbnail_97x97_' + image_id_id + '" src="'+image_thumbnail_97x97+'">'+
                        '<i id="i_th_97_' + image_id_id + '" class="panda panda-plus uk-hidden"><p id="p_th_97_' + image_id_id + '">thumbnail</p></i>'+
                    '</a>'+
                '</div>';

    }
    return list_image_to_show
}

function append_default_to_list_image(len_list_image){
    var list_image_to_show = "";
    for(var i = 0 ; i < len_list_image; i++){
        id_image += 1;
        list_image_to_show +=
                '<div class="uk-width-1-3 gallery_image" data-id-image="'+ id_image +'" id="image_' + id_image + '">'+
                    '<input id="thumbnail_input_' + id_image + '" type="hidden">'+
                    '<a class="sh-delete-image" onclick="deleteImage(\'' + id_image + '\')">'+
                        '<i class="panda panda-cross"></i>'+
                    '</a>'+
                    '<a class="sh-primary-pic" onclick="add_image(\'' + id_image + '\')" href="#modal-image-crop" data-uk-modal="{center:true, bgclose:false}">'+
                        '<img id="main_image_' + id_image + '" src="">'+
                        '<i id="i_378_' + id_image + '" class="panda panda-plus"><p id="p_378_' + id_image + '">عکس اصلی</p></i>'+
                    '</a>'+
                    '<a class="sh-thumbnail-170" onclick="add_thumbnail(\'' + id_image + '\',' + 170 + ')">'+
                        '<img id="image_thumbnail_150x170_' + id_image + '" src="">'+
                        '<i id="i_th_170_' + id_image + '" class="panda panda-plus"><p id="p_th_170_' + id_image + '">thumbnail</p></i>'+
                    '</a>'+
                    '<a class="sh-thumbnail-97" onclick="add_thumbnail(\'' + id_image + '\',' + 97 + ')">'+
                        '<img id="image_thumbnail_97x97_' + id_image + '" src="">'+
                        '<i id="i_th_97_' + id_image + '" class="panda panda-plus"><p id="p_th_97_' + id_image + '">thumbnail</p></i>'+
                    '</a>'+
                '</div>';
    }
    return list_image_to_show
}

function create_gallery_and_add_image(list_image_to_show, gallery){
    return '<div class="sh-select-color gallery" id="id_gallery_' + gallery['id'] + '">'+
                '<p>'+
                    '<input type="radio" name="main_gallery" onclick="change_main_gallery(\'' + gallery['id'] + '\', \'in_data_base\')">'+
                    add_color_image +
                '</p>'+
                '<a class="sh-delete-gallery" onclick="deleteGallery(\'' + gallery['id'] + '\', \'in_data_base\')">'+
                    '<i class="panda panda-cross"></i>'+
                '</a>'+
            '<div>'+
            '<div class="uk-grid sh-select-pictures" >'+
                list_image_to_show+
            '</div>';
}

function remove_spinner_gallery(){
    var count_gallery = $('.gallery').length;
    if(count_gallery == len_gallery_count+1){
        $('#spinner_gallery').remove();
    }
}

function change_main_gallery(gallery_id_selected, type_gallery) {
    if(type_gallery == 'in_data_base'){
        $.ajax({
            url: '/products/change_main_gallery/',
            type: 'POST',
            data: {
                'gallery_id': gallery_id_selected,
                'csrfmiddlewaretoken': getCookie('csrftoken')
            },
            dataType: 'json',
            success: function (message) {
                if(message == 'success'){
                    uikitNotify('گالری اصلی عوض شد.', 'panda\&#32panda-checkmark2', 'success');
                }else{
                    notify_error(500, false)
                }
            },
            error: function (e) {
                notify_error(e, true)
            }
        })
    }
}

function check_has_main_gallery(){
    if(list_gallery.length > 0){
        var bool = false;
        $("input[name='main_gallery']:checked").each(function(){
            bool = true;
        });
        if(bool){
            return true
        }else{
            uikitNotify("یکی از گالری ها باید به عنوان گالری اصلی انتخاب شود.", 'panda\&#32panda-warning', 'warning');
            return false;
        }
    }
    return true
}

function check_no_color_gallery_similar(select_color){
    if (select_color == "" || select_color == null || list_select.indexOf(select_color) > -1) {
        error_gallery.push("رنگ گالری ها نباید مثل هم باشند.");
        return true
    }
    return false
}

function check_is_main_gallery(main_gallery){
    if(main_gallery.length > 0){
        return 'True';
    }
    return 'False';
}

function check_has_gallery_image(list_image){
    if (list_image.length == 0) {
        error_gallery.push("گالری نباید بدون عکس باشد.");
        return false
    }
    return true
}

function add_image_to_list_image(id_gallery){
    error_gallery = [];
    var list_image = [];
    var bool = true;
    $("#" + id_gallery + " .gallery_image").each(function () {
        var main_image_id = $(this).attr('data-id-image');
        var category_img = [];
        var large_image = $("#main_image_" + main_image_id).attr('data-val-large');
        if (large_image != undefined && large_image != "") {
            var thumbnail_150x170 = $("#image_thumbnail_150x170_" + main_image_id).attr('data-val');
            var thumbnail_97x97 = $("#image_thumbnail_97x97_" + main_image_id).attr('data-val');
            if(check_image_has_thumbnail(thumbnail_150x170, thumbnail_97x97)){
                category_img.push(large_image.split(',')[1]);
                category_img.push(thumbnail_150x170.split(',')[1]);
                category_img.push(thumbnail_97x97.split(',')[1]);
                list_image.push(category_img);
            } else {
                bool = false;
                return false;
            }
        }
    });
    if(bool){
        return list_image
    }else{
        return false
    }
}

function check_image_has_thumbnail(thumbnail_150x170, thumbnail_97x97){
    if (thumbnail_150x170 != undefined && thumbnail_97x97 != undefined && thumbnail_150x170 != "" && thumbnail_97x97 != "") {
        return true
    } else {
        error_gallery.push("برای هر عکس باید thumbnail مشخص شود.");
        return false
    }
}

function create_gallery_to_save_db(){
    $(".gallery").each(function () {
        var list_image = [],
            id_gallery = $(this).attr('id'),
            select_color = $(this).find(':selected');
        if (check_no_color_gallery_similar(select_color.val())) {
                return false
        } else {
            list_select.push(select_color.val());
            var main_gallery = check_is_main_gallery($(this).find("input[type=radio]:checked"));
            var dict_image = {
                'color': select_color.val(),
                'main_gallery': main_gallery,
                'list_image': []
            };
            list_image = add_image_to_list_image(id_gallery);
            if (list_image && check_has_gallery_image(list_image)) {
                dict_image.list_image = list_image;
                list_gallery.push(dict_image);
            }
        }
    });

}

function deleteGallery(gallery_id_selected, type_gallery) {
    if (type_gallery == 'in_data_base') {
        var main_gallery = check_is_main_gallery($('#id_gallery_' + gallery_id_selected).find("input[type=radio]:checked"));
        if(main_gallery != 'True'){
            UIkit.modal.confirm("آیا می خواهید گالری پاک شود؟", function () {
                $.ajax({
                    url: '/products/delete_gallery/',
                    type: 'POST',
                    data: {
                        'gallery_id': gallery_id_selected,
                        'csrfmiddlewaretoken': getCookie('csrftoken')
                    },
                    dataType: 'json',
                    success: function (message) {
                        if (message == 'success') {
                            $('#id_gallery_' + gallery_id_selected).remove();
                            uikitNotify('گالری پاک شد.', 'panda\&#32panda-checkmark2', 'success');

                        } else if (message == 'empty_gallery') {
                            uikitNotify('محصول نمی تواند بدون گالری باشد.', 'panda\&#32panda-warning', 'warning');

                        } else {
                           notify_error(500, false)
                        }
                    },
                    error: function (e) {
                        notify_error(e, true)
                    }
                })
            });
        }else{
            uikitNotify('ابتدا گالری اصلی را تغییر دهید سپس این گالری را پاک کنید.', 'panda\&#32panda-warning', 'warning');
        }
    } else {
        $('#id_gallery_' + gallery_id_selected).remove();
    }
}

function deleteImage(id_image) {
    $('#change_gallery').val('change');
    main_image_clear(id_image);
    thumbnail_150x170_clear(id_image);
    thumbnail_97x97_clear(id_image);
}

function main_image_clear(id_image){
    $("#thumbnail_input_" + id_image).val('');
    var main_image_selector = "#main_image_"+id_image;
    $(main_image_selector).attr('src', '');
    $(main_image_selector).attr('data-val-large', '');
    $("#i_378_" + id_image).removeClass('uk-hidden');
}

function thumbnail_150x170_clear(id_image){
    var thumbnail_150x170_selector = '#image_thumbnail_150x170_'+id_image;
    $(thumbnail_150x170_selector).attr('src', '');
    $(thumbnail_150x170_selector).attr('data-val', '');
    $('#i_th_170_'+id_image).removeClass('uk-hidden');
}

function thumbnail_97x97_clear(id_image){
    var thumbnail_97x97_selector = '#image_thumbnail_97x97_'+id_image;
    $(thumbnail_97x97_selector).attr('src', '');
    $(thumbnail_97x97_selector).attr('data-val', '');
    $('#i_th_97_'+id_image).removeClass('uk-hidden');
}

function create_gallery_html(id_gallery){
    var image_html = append_default_to_list_image(5);
    return  '<div class="sh-select-color gallery" id="id_gallery_' + id_gallery + '">'+
                '<p>'+
                    '<input type="radio" name="main_gallery" onclick="change_main_gallery(' + id_gallery + ', \'new\')">'+
                    add_color_image +
                '</p>'+
                '<a class="sh-delete-gallery" onclick="deleteGallery(\'' + id_gallery + '\', \'new\')">'+
                    '<i class="panda panda-cross"></i>'+
                '</a>'+
                '<div>'+
                    '<div class="uk-grid sh-select-pictures" >'+
                        image_html+
                    '</div>'+
                '</div>'+
            '</div>';
}


function gallery_no_has_error(){
    if (error_gallery.length > 0) {
        for (var value in error_gallery) {
            uikitNotify(error_gallery[value], 'panda\&#32panda-cross', 'danger');
        }
        return false
    }
    return true
}

$("#saveimage").click(function () {
    try{
        $('#change_gallery').val('change');
        var image_large = $('.image-gallery > img').cropper('getCroppedCanvas').toDataURL('image/jpeg', 1),
            id_image_selected = $('#image_selected').val();
        $("#thumbnail_input_" + id_image_selected).val(image_large);
        var main_image_selector = "#main_image_"+id_image_selected;
        $(main_image_selector).attr('src', image_large);
        $(main_image_selector).attr('data-val-large', image_large);
        $("#i_378_" + id_image_selected).addClass('uk-hidden');
        modal_image_crop.hide();
    }
    catch(e){
        uikitNotify('لطفا عکس انتخاب کنید.', 'panda\&#32panda-warning', 'warning');
    }
});

$('#saveThumbnail_155_170').click(function () {
    try{
        $('#change_gallery').val('change');
        var id_image_selected = $('#image_selected').val(),
            image_thumbnail_155_170 = $('.image-thumbnail-150-170 > img').cropper('getCroppedCanvas').toDataURL('image/jpeg', 1),
            thumbnail_150x170_selector = '#image_thumbnail_150x170_'+id_image_selected;
        $(thumbnail_150x170_selector).attr('src', image_thumbnail_155_170);
        $(thumbnail_150x170_selector).attr('data-val', image_thumbnail_155_170);
        $('#i_th_170_'+id_image_selected).addClass('uk-hidden');
        modal_crop_thumbnail_150_170.hide();
    }
    catch(e){
        modal_crop_thumbnail_150_170.hide();
        uikitNotify('لطفا عکس انتخاب کنید.', 'panda\&#32panda-warning', 'warning');
    }

});

$('#saveThumbnail_97x97').click(function () {
    try{
        $('#change_gallery').val('change');
        var selected_image = $('#image_selected').val(),
            image_thumbnail_97x97 = $('.image-thumbnail-97-97 > img').cropper('getCroppedCanvas').toDataURL('image/jpeg', 1),
            thumbnail_97x97_selector = '#image_thumbnail_97x97_'+selected_image;
        $(thumbnail_97x97_selector).attr('src', image_thumbnail_97x97);
        $(thumbnail_97x97_selector).attr('data-val', image_thumbnail_97x97);
        $('#i_th_97_'+selected_image).addClass('uk-hidden');
        modal_crop_thumbnail_97_97.hide();
    }
    catch(e){
        modal_crop_thumbnail_97_97.hide();
        uikitNotify('لطفا عکس انتخاب کنید.', 'panda\&#32panda-warning', 'warning');
    }
});

$('body').on('change', '[name="color"]', function () {
    $('#change_gallery').val('change');
});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
