var list_message = {
    'national_code': "کشور سازنده این محصول تعیین نشده است.",
    'published': "این محصول منتشر شده است.",
    'ready_to_delete': "رسته این محصول پاک شده است.",
    'trash': "این محصول پاک شده است.",
    'gender': "جنسیت این محصول نامشخص می باشد.",
    'unit': "واحد محصول وارد نشده است.",
    'unit_type': "نوع واحد محصول وارد نشده است.",
    'product_name_fa': "نام فارسی این محصول وارد نشده است.",
    'brand': "برند این محصول مشخص نمی باشد.",
    'galleries': "این محصول هیچ عکسی ندارد",
    'filters': "فیلد های قابل جستجوی مشخصات فنی این محصول وارد نشده است."
};

function changePublishedProduct(product_id, status) {
    UIkit.modal.confirm("آیا می خواهید وضعیت انتشار محصول را تغییر دهید؟", function () {
        $.ajax({
            url: '/products/change_product_published/',
            type: 'POST',
            data: {
                'product_id': product_id,
                'status': status,
                'csrfmiddlewaretoken': csrf_token
            },
            success: function (result) {
                var errors = result['errors'];
                if(result['message'] == 'schema'){
                    for(var item in errors){
                        uikitNotify(list_message[item], 'panda\&#32panda-cross', 'danger');
                    }
                }else if(result['message'] == 'error'){
                    uikitNotify('سیستم با مشکل مواجه شده است.', 'panda\&#32panda-cross', 'danger');

                }else if(result['message'] == 'success'){
                     if(status){
                         $('.unpublished_product, .unpublished_product_'+product_id).removeClass('uk-hidden');
                         $('.published_product, .published_product_'+product_id).addClass('uk-hidden');
                         $('#span_publish_'+product_id).html('<div class="uk-badge sh-badge-success">منتشر شده</div>')
                     }else {
                         $('.published_product, .published_product_'+product_id).removeClass('uk-hidden');
                         $('.unpublished_product, .unpublished_product_'+product_id).addClass('uk-hidden');
                         $('#span_publish_'+product_id).html('<div class="uk-badge sh-badge-warning">منتشر نشده</div>');
                     }
                }
            },
            error: function (e) {
                notify_error(e, true)
            }
        })
    }, {labels: {'Ok': 'بلی', 'Cancel': 'خیر'}});
}
