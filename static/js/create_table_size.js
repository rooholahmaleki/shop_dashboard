var ClassCol = GetRandomInt(900000, 999999);

function AddRow() {
    var LengthTH = $("#tblData thead tr th").length - 1;
    var CountTd = GetHtmlTd(LengthTH);

    $("#tblData tbody").append(
        "<tr>" +
        CountTd +
        "<td><a class='btnDelete'><i class='panda panda-cross'></i></a></td>" +
        "</tr>"
    );
    Bind();
}

function AddCol() {
    var PresentationTh = $('#PresentationTh').val();
    var CodeTh = $('#CodeTh').val();
    if (PresentationTh != "" && CodeTh != "") {
        $('#PresentationTh').val('');
        $('#CodeTh').val('');
        $("#tblData thead tr #ActionData").before(
            '<th data-class="' + ClassCol + '" data-presentation="' + PresentationTh + '" data-code="' + CodeTh + '">' +
                '<span class="sh-colname-sizetab">'+PresentationTh +'</span>' +
                '<a class="btnDeleteCol sh-btn-6 uk-button" data-class="' + ClassCol + '">' +
                    '<i class="panda panda-cross"></i>' +
                '</a>' +
                '<a class="btnEditCol sh-btn-8 uk-button" data-class="' + ClassCol + '">' +
                    '<i class="panda panda-pencil"></i>' +
                '</a>' +
            '</th>'
        );
        var LengthTH = $("#tblData thead tr th").length;
        $("#tblData tbody tr").each(function () {
            var CountTd = $(this).children().length;
            for (var i = CountTd; i < LengthTH; i++) {
                $(this).find("td:last-child").before(
                    "<td data-class=''>" +
                    "<input class='uk-width-1-2' style='margin-left:5px;' name='code' placeholder='سایز' type='text'/>" +
                    "<input class='uk-width-1-2' name='presentation' placeholder='مقدار جهت نمایش' type='text'/>" +
                    "</td>"
                );
            }
        });
        ClassCol += 1;
        Bind();
    }
}

/**
 * @return {string}
 */
function GetHtmlTd(LengthTH) {
    var CountTd = '';
    for (var i = 0; i < LengthTH; i++) {
        CountTd +=
            "<td data-class=''>" +
            "<input class='uk-width-1-2' style='margin-left:5px;' name='code' placeholder='سایز' type='text'/>" +
            "<input class='uk-width-1-2' name='presentation' placeholder='مقدار جهت نمایش' type='text'/>" +
            "</td>";
    }
    return CountTd;
}

function DeleteRow() {
    var par = $(this).parent().parent();
    par.remove();
}

function DeleteCol() {
    var Class = $(this);
    Class.parent().remove();
    $("#tblData td").each(function () {
        if ($(this).attr('data-class') == Class.attr('data-class')) {
            $(this).remove()
        }
    })
}

function EditCol() {
    var Th = $(this).parent();
    Th.html(
        '<input type="text" placeholder="نام ستون" name="code" value="'+Th.attr('data-code')+'">' +
        '<input type="text" placeholder="نام جهت نمایش" name="presentation" value="'+Th.attr('data-presentation')+'">' +
        '<a class="btnSaveCol" data-class="'+Th.attr('data-class')+'">' +
            '<i class="panda panda-plus"></i>' +
        '</a>'
    );
    $(".btnSaveCol").unbind("click").bind("click", SaveCol);
}

function SaveCol() {
    var DataClass = $(this).attr('data-class');
    var Th = $(this).parent();
    var PresentationTh = Th.find('input[name="presentation"]').val();
    var CodeTh = Th.find('input[name="code"]').val();
    Th.attr('data-presentation', PresentationTh);
    Th.attr('data-code', CodeTh);
    Th.html(
        '<span class="sh-colname-sizetab">'+PresentationTh +'</span>' +
        '<a class="btnDeleteCol sh-btn-6 uk-button" data-class="' + DataClass + '">' +
            '<i class="panda panda-cross"></i>' +
        '</a>' +
        '<a class="btnEditCol sh-btn-8 uk-button" data-class="' + DataClass + '">' +
            '<i class="panda panda-pencil"></i>' +
        '</a>'
    );
    $(".btnDeleteCol").unbind("click").bind("click", DeleteCol);
    $(".btnEditCol").unbind("click").bind("click", EditCol);
}

function SetClassCol() {
    var ColLength = $("#tblData thead tr").children().length;
    for (var i = 1; i <= ColLength; i++) {
        var DataClass = $("#tblData thead tr th:nth-child(" + i + ")").attr('data-class');
        $("#tblData tbody tr").each(function () {
            $(this).find('td:nth-child(' + i + ')').attr('data-class', DataClass)
        })
    }
}

function Bind() {
    $(".btnDelete").unbind("click").bind("click", DeleteRow);
    $(".btnDeleteCol").unbind("click").bind("click", DeleteCol);
    $(".btnEditCol").unbind("click").bind("click", EditCol);
    SetClassCol();
}

function GetListSize() {
    var ListDefaultSize = [];
    var ListSize = [];
    var ListTh = [];
    $("#tblData tbody tr").each(function () {
        var list_size = [];
        var code = $(this).find("td:first-child input[name='code']").val();
        var presentation = $(this).find("td:first-child input[name='presentation']").val();
        if (code != "" && presentation != "") {
            var dict_default_size = {
                'code': code,
                'presentation': presentation
            };
            ListDefaultSize.push(dict_default_size)
        }

        $(this).find("td").each(function () {
            var dict_size = {};
            var code = $(this).find("input[name='code']").val();
            var presentation = $(this).find("input[name='presentation']").val();
            var id = $(this).attr('data-class');
            if (code != undefined && presentation != undefined) {
                dict_size = {
                    'code': code,
                    'presentation': presentation,
                    'id': id
                };
                list_size.push(dict_size);
            }
        });
        ListSize.push(list_size)
    });
    $("#tblData thead tr th").each(function () {
        var Id = $(this).attr('data-class');
        var PresentationTh = $(this).attr('data-presentation');
        var CodeTh = $(this).attr('data-code');
        if (Id != undefined) {
            var dict_th = {
                'presentation': PresentationTh,
                'code': CodeTh,
                'id': Id
            };
            ListTh.push(dict_th)
        }
    });

    return {
        'list_default_size': ListDefaultSize,
        'list_size': ListSize,
        'list_th': ListTh
    };
}

/**
 * @return {boolean}
 */
function CheckReturnSize(DictSize){
    return Boolean(DictSize['list_default_size'].length == DictSize['list_size'].length && DictSize['list_default_size'].length != 0);
}

/**
 * @return {boolean}
 */
function CheckDefaultSize(DictSize){
    var list_default_size = DictSize['list_default_size'];
    var list_code = [];
    var repeat = false;
    for(var item in list_default_size){
        var item = list_default_size[item]['code'];
        for(var code in list_code){
            if(item == list_code[code]){
                repeat = true
            }
        }
        if(!repeat){
            list_code.push(item)
        }
    }
    return repeat
}

/**
 * @return {number}
 */
function GetRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

$(function () {
    $(".btnDelete").unbind("click").bind("click", DeleteRow);
    $(".btnDeleteCol").unbind("click").bind("click", DeleteCol);
    $(".btnEditCol").unbind("click").bind("click", EditCol);
    $("#btnAddRow").unbind("click").bind("click", AddRow);
    $("#btnAddCol").unbind("click").bind("click", AddCol);
});
