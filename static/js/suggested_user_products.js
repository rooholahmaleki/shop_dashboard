var modal = UIkit.modal("#form_suggested_product_modal",{bgclose:false, center:true});

function set_data_to_modal_confirmed_suggested(mongo_id, product_name_en) {
    $('#id_suggested_id').val(mongo_id);
    $('#id_product_name_en').val(product_name_en);
    modal.show()
}

function delete_suggested_product(product_id) {
    UIkit.modal.confirm("آیا می خواهید محصول پاک شود؟", function () {
        $.ajax({
            url: '/products/suggested/delete/',
            type: 'POST',
            data: {
                'product_id': product_id,
                'csrfmiddlewaretoken': csrf_token
            },
            success: function (result) {
                uikitNotify('با موفقیت حذف شد', 'panda\&#32panda-cross', 'success');
                $("#product_" + product_id).remove();
            },   
            error: function (e) {
                notify_error(e, true)
            }
        })
    }, {labels: {'Ok': 'بلی', 'Cancel': 'خیر'}});
}

function confirmed_suggested_product() {
    var form_data = new FormData($('#form_add_suggested_product')[0]);
    var fa_data = check_input_char_string($('#id_product_name_fa').val(), 'fa');
    var en_data = check_input_char_string($('#id_product_name_en').val(), 'en');
    if (fa_data && en_data) {
        $.ajax({
            url: '/products/suggested/pending/confirmed/',
            type: 'POST',
            processData: false,
            contentType: false,
            data: form_data,
            dataType: 'json',
            success: function (result) {
                if (result['message'] == "success") {
                    uikitNotify('تایید شد.', 'panda\&#32panda-checkmark2', 'success');
                    $("#product_" + result['product_id']).remove();
                    modal.hide();
                } else if (result['message'] == "form_error") {
                    for (var value in result['error']) {
                        uikitNotify(result['error'][value], 'panda\&#32panda-cross', 'danger');
                    }
                }
                else {
                    notify_error(500, false)
                }
            },   
            error: function (e) {
                notify_error(e, true)
            }
        });
    }
}

function create_product(id_last_group, suggested_id){
    UIkit.modal.confirm("آیا می خواهید این محصول را ایجاد کنید؟", function () {
        $.ajax({
            url: '/products/create/product_id',
            type: 'POST',
            data: {
                'id_last_group': JSON.stringify(id_last_group),
                'csrfmiddlewaretoken': csrf_token
            },
            success: function (result) {
                if(result['message'] == 'success'){
                    window.location.href = "/products/create/" + result['product_id']+"?suggested_id="+suggested_id;
                }else{
                    notify_error(500, false)
                }
            },   
            error: function (e) {
                notify_error(e, true)
            }
        })
    }, {labels: {'Ok': 'بلی', 'Cancel': 'خیر'}});
}

function check_input_char_string(string, language) {
    var regex = '';
    var message = '';
    if (language == 'en') {
        regex = /^([A-Za-z0-9-!$%@#^&*()_+|~=`{}\[\]:";'<>?,.\/])/;
        message = "نام انگلیسی درست وارد نشده است."
    } else {
        regex = /^[\u0600-\u06FF\s0-9+]+$/;
        message = "نام فارسی درست وارد نشده است."
    }
    if (regex.test(String(string))) {
        return true;
    }
    uikitNotify(message, 'panda\&#32panda-cross', 'danger');
    return false
}
