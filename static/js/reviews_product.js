var csrf_token = '',
    id_review = 1000,
    id_sub = 100000;

function subReviewHtml(id_sub) {
    $("#review_change").val('change');
    return '<div id="id_review_sub_' + id_sub + '" class="sh-review-examin class_review_sub">' +
                '<a class="sh-photo-review" onclick="selectedImage(\'' + id_sub + '\')">' +
                '<input name="file-review" type="file" id="' + id_sub + '" class="uk-hidden" onchange="loadImageReview(event)">' +
                '<img id="img_' + id_sub + '" data-image="no_image">' +
                '<i class="panda panda-plus"></i>' +
                '</a>' +
                '<input class="uk-margin-large-right" name="title" value="" placeholder="درج عنوان">'+
                '<textarea class="uk-text-break uk-margin-large-right" name="description" placeholder="درج متن"></textarea>' +
                '<a class="panda panda-cross uk-margin-right" onclick="deleteSubReview(\'' + id_sub + '\')"></a>' +
            '</div>';
}

function addReview() {
    var html =
        '<div class="class_review" id="id_review_' + id_review + '">' +
            '<a><i class="panda panda-cross" onclick="deleteReview(' + id_review + ')"></i></a>' +
            '<input name="title" placeholder="عنوان کلی نقد و بررسی">'+
             subReviewHtml(id_sub) +
            '<a id="add_sub_' + id_review + '" class="add_sub"><i class="panda panda-plus" onclick="addSubReview(' + id_review + ')" ></i></a>' +
        '</div>' +
        '<hr id="hr_' + id_review + '">';

    $("#all_review").append(html);
    id_review = id_review + 1;
    id_sub = id_sub + 1;
    $('.sh-add-review').removeClass('uk-hidden');
}

function deleteReview(id_review) {
    $("#id_review_" + id_review).remove();
    $("#hr_" + id_review).remove();
    $("#review_change").val('change');
}

function addSubReview(id_review) {
    $("#add_sub_" + id_review).before(subReviewHtml(id_sub));
    id_sub = id_sub + 1;
}

function reviews_product() {
    var list_review = [];
    var dict_error = {};
    $("#all_review .class_review").each(function () {
        var id = $(this).attr('id');
        var dict_technical = {
            'title': $(this).find('input[name="title"]').val(),
            'review_sub_groups': []
        };
        if (dict_technical['title'] != ""){
            var review_sub_groups = [];
            $('#' + id + " .class_review_sub").each(function () {
                var image = $(this).find($('img')).attr('data-image'),
                    description = $(this).find('textarea[name="description"]').val(),
                    title = $(this).find('input[name="title"]').val();

                if (image != undefined || description != undefined){
                    review_sub_groups.push({
                        'description': description,
                        'title': title,
                        'image': ((image != "no_image") ? image : 'no_image')
                    })
                }
            });
            if (!$.isEmptyObject(review_sub_groups)){
                dict_technical['review_sub_groups'] = review_sub_groups;
                list_review.push(dict_technical);
            }
        } else {
            dict_error['title'] = "عنوان همه گروه های نقد و بررسی باید تعیین شوند."
        }
    });
    if(!$.isEmptyObject(dict_error)){
        for(var item in dict_error){
            uikitNotify(dict_error[item], 'panda\&#32panda-cross', 'danger');
        }
        return false
    } else {
        return list_review;
    }
}


function deleteSubReview(id_review_sub) {
    var review_sub = $("#id_review_sub_" + id_review_sub);
    var count_child_father = review_sub.parent().children('.class_review_sub').length;
    if (count_child_father == 1) {
        var parent_id = review_sub.parent().attr('id');
        parent_id = parent_id.split('_');
        parent_id = parent_id[parent_id.length - 1];
        deleteReview(parent_id);
    }
    review_sub.remove();
    $("#review_change").val('change');
}

function loadImageReview(e) {
    var input = e.target;
    var parent_img = $(input.parentElement);
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            parent_img.children('img').attr('src', e.target.result).addClass('uk-border-circle sh-add-piccircle');
            parent_img.children('img').attr('data-image', (e.target.result).split(',')[1]);
            parent_img.children('i').addClass('uk-hidden');
            parent_img.css('border', '0');
        };
        reader.readAsDataURL(input.files[0]);
        $("#review_change").val('change');
    }
}

function selectedImage(elemId) {
    document.getElementById(elemId).click();
}

function loadReviews() {
    $.ajax({
        url: '/products/load_review/',
        type: 'POST',
        data: {
            'csrfmiddlewaretoken': csrf_token
        },
        dataType: 'json',
        success: function (result) {
            var _reviews = result['reviews'];
            for(var i in _reviews){
                var review = _reviews[i];
                var sub_review_html = "";
                for(var j in _reviews[i]['review_sub_groups']){
                    var sub =_reviews[i]['review_sub_groups'][j];
                    var image = "";
                    if(sub['image'] != "no_image"){
                        image = '<img class="uk-border-circle sh-add-piccircle" id="img_' + sub['id'] + '" src="' + sub['image'] + '" data-image="' + sub['data_image'] + '">';
                    }else{
                        image = '<img id="img_' + sub['id'] + '" data-image="' + sub['data_image'] + '"><i class="panda panda-plus"></i>';
                    }
                    sub_review_html +=
                        '<div id="id_review_sub_' + sub['id'] + '" class="sh-review-examin class_review_sub">' +
                            '<a class="sh-photo-review" onclick="selectedImage(\'' + sub['id'] + '\')">' +
                                '<input name="file-review" type="file" id="' + sub['id'] + '" class="uk-hidden" onchange="loadImageReview(event)">' +
                                image+
                            '</a>' +
                            '<input class="uk-margin-large-right" name="title" value="'+sub['title']+'">'+
                            '<textarea class="uk-text-break uk-margin-large-right" name="description">'+sub['description']+'</textarea>' +
                            '<a class="panda panda-cross uk-margin-right" onclick="deleteSubReview(\'' + sub['id'] + '\')"></a>' +
                        '</div>';
                }
                var html =
                    '<div class="class_review" id="id_review_' + review['id'] + '">' +
                        '<a><i class="panda panda-cross" onclick="deleteReview(\'' + review['id'] + '\')"></i></a>' +
                        '<input name="title" value="'+review['title']+'">'+
                         sub_review_html+
                        '<a id="add_sub_' + review['id'] + '" class="add_sub"><i class="panda panda-plus" onclick="addSubReview(\'' + review['id'] + '\')" ></i></a>' +
                    '</div>' +
                    '<hr id="hr_' + review['id'] + '">';
                $("#all_review").append(html);
            }
            $("#spinner_reviews").remove();
            bindChange()
        },
        error: function (e) {
            notify_error(e, true)
        }
    })
}

function bindChange() {
    $('.class_review_sub textarea[name="description"]').keydown(function () {
        $("#review_change").val('change');
    });

    $('.class_review_sub input[name="title"]').keydown(function () {
        $("#review_change").val('change');
    });

    $('.class_review input[name="title"]').keydown(function () {
        $("#review_change").val('change');
    });
}
