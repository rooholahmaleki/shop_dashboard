var id_append_image = 0;
function add_thumbnail(id_image, size) {
    $('#image_selected').val(id_image);
    var $thumbnailImage = $('#thumbnail_input_'+id_image).val();
    if($thumbnailImage){
        if(size == 170){
            modal_crop_thumbnail_150_170.show()
        }else{
            modal_crop_thumbnail_97_97.show();
        }
        (function () {
            var $image,
                options;

            if(size == 170){
                $image = $('.cropper-thumbnail-150-170');
                options = {
                    aspectRatio: 31 / 34,
                    preview: '.img-preview',
                    zoomable: false,
                    autoCropArea:0.5,
                    minCropBoxWidth: 100,
                    minCropBoxHeight: 100,
                    cropBoxResizable:true,
                    dragCrop: false,
                    movable: false,
                    guides:false
                };
            }else{
                $image = $('.cropper-thumbnail-97-97');
                options = {
                    aspectRatio: 2 / 2,
                    preview: '.img-preview',
                    zoomable: false,
                    autoCropArea:0.3,
                    minCropBoxWidth: 50,
                    minCropBoxHeight: 50,
                    cropBoxResizable:true,
                    dragCrop: false,
                    movable: false,
                    guides:false
                };
            }
            $image.on().cropper(options);

            if (!$.isFunction(document.createElement('canvas').getContext)) {
                $('button[data-method="getCroppedCanvas"]').prop('disabled', true);
            }

            if (typeof document.createElement('cropper').style.transition === 'undefined') {
                $('button[data-method="rotate"]').prop('disabled', true);
                $('button[data-method="scale"]').prop('disabled', true);
            }

            var URL = window.URL || window.webkitURL;

            if(size == 170){
                if (URL) {
                    $('.image-thumbnail-150-170 > .panda.panda-warning').addClass('uk-hidden');
                    $image.one('built.cropper', function () {
                        URL.revokeObjectURL($thumbnailImage); // Revoke when load complete
                    }).cropper('reset').cropper('replace', $thumbnailImage);
                }
            }else if(size == 97){
                if (URL) {
                    $('.image-thumbnail-97-97 > .panda.panda-warning').addClass('uk-hidden');
                    $image.one('built.cropper', function () {
                        URL.revokeObjectURL($thumbnailImage); // Revoke when load complete
                    }).cropper('reset').cropper('replace', $thumbnailImage);
                }
            }

            $('.docs-options :checkbox').on('change', function () {
                var $this = $(this);
                var cropBoxData;
                var canvasData;
                if (!$image.data('cropper')) {
                    return;
                }

                options[$this.val()] = $this.prop('checked');

                cropBoxData = $image.cropper('getCropBoxData');
                canvasData = $image.cropper('getCanvasData');

                options.built = function () {
                    $image.cropper('setCropBoxData', cropBoxData);
                    $image.cropper('setCanvasData', canvasData);
                };

                $image.cropper('destroy').cropper(options);
            });
        }());
    }else{
        uikitNotify('ابتدا عکس را مشخص کنید', 'panda\&#32panda-warning', 'warning');
    }
}
