# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_userbox_group_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userbox',
            name='group_name',
            field=models.CharField(default=b'', max_length=24),
        ),
    ]
