# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='userbox',
            name='group_name',
            field=models.CharField(default='', max_length=24),
            preserve_default=False,
        ),
    ]
