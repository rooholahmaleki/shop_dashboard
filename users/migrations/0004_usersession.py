# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-30 12:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20160628_1001'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserSession',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=255)),
                ('phone_number', models.CharField(max_length=11)),
                ('mongo_id', models.CharField(max_length=24)),
            ],
        ),
    ]
