from django.db import models
from django.contrib.auth.models import User as DjangoUser


class UserBox(models.Model):
    user = models.OneToOneField(DjangoUser, related_name='userBox')
    mongo_id = models.CharField(max_length=24)
    group_name = models.CharField(max_length=24, default='')
    image_flag = models.BooleanField(default=False)

    def __unicode__(self):
        return self.user.username


class UsersMobile(DjangoUser):
    mongo_id = models.CharField(max_length=255)
