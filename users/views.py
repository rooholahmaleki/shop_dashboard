# coding: utf-8

from django.contrib import auth
from django.shortcuts import render, redirect
from django.conf import settings
from django.http import HttpResponseRedirect


def login(request):
    referer = request.META.get('HTTP_REFERER', '').rstrip('/')

    if 'next' in request.GET:
        redirect_url = request.GET['next']
    elif referer and not referer.endswith(settings.LOGIN_URL):
        redirect_url = referer
    else:
        redirect_url = '/'

    if request.user.is_authenticated():
        return HttpResponseRedirect(redirect_url)

    elif request.method == 'GET':
        return render(request, 'login.html', {'redirect_url': redirect_url})

    elif request.method == 'POST':
        user = auth.authenticate(
            username=request.POST.get('username', '').lower(),
            password=request.POST.get('password', '').encode('utf-8')
        )

        if user:
            auth.login(request, user)
            return redirect(home)
        else:
            return render(request, 'login.html', {
                'msg': 'نام کاربری یا گذرواژه را اشتباه وارد کرده اید!'
            })


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')


def home(request):
    return render(request, 'home.html')
