# coding: utf-8
from django import forms
from product.controller import CategoryController


class AddBrandForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(AddBrandForm, self).__init__(*args, **kwargs)

    fa_name = forms.CharField(
        label='نام فارسی',
        error_messages={'required': 'فیلد نام فارسی خالی می باشد.'},
        required=True,
        widget=forms.TextInput(attrs={'class': 'form_brand_data'})
    )
    en_name = forms.CharField(
        label='نام انگلیسی',
        error_messages={'required': 'فیلد نام انگلیسی خالی می باشد.'},
        required=True,
        widget=forms.TextInput(attrs={'class': 'form_brand_data'})
    )
    image = forms.FileField(
        label='لوگو',
        required=False,
        widget=forms.FileInput(attrs={
            'class': 'form_brand_data',
            'dispaly': 'none'
        })
    )
    brand_id = forms.CharField(
        widget=forms.HiddenInput(),
        required=False
    )
    page_name = forms.CharField(
        widget=forms.HiddenInput(),
        required=False
    )


class CategoryGroupForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(CategoryGroupForm, self).__init__(*args, **kwargs)
        self.fields['category'].choices = CategoryController().get_all_category_tuple()
        first_category_id = int(self.fields['category'].choices[0][0])
        self.fields['group'].choices = CategoryController().get_group_from_category_list(first_category_id)

    category = forms.ChoiceField(
        label='رسته',
        widget=forms.Select(attrs={'onchange': 'load_group()'}),
        error_messages={'required': 'رسته انتخاب نشده است.'},
        required=False
    )
    group = forms.ChoiceField(
        label='گروه',
        widget=forms.Select(attrs={}),
        error_messages={'required': 'گروه انتخاب نشده است.'},
        required=False
    )
