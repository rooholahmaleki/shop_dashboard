# coding: utf-8

import os
import logging
import shutil

import datetime
from PIL import Image
from pymongo import ReturnDocument

from forms import AddBrandForm
from bson.json_util import loads
from django.conf import settings
from django.shortcuts import render
from brand.forms import CategoryGroupForm
from core.db_mongo import get_next_sequence
from django.http import HttpResponse, JsonResponse
from django.views.decorators.http import require_POST
from core.db_mongo import cursor

logger = logging.getLogger(__name__)


def brands(request):
    criteria = {
        'status': True
    }
    args = {
        'all_brand': cursor.brand.find(criteria, {'_id': 0}).sort([("created_date", -1)]),
        'brand_form': AddBrandForm(initial={'page_name': 'brands'}),
        'category_form': CategoryGroupForm()
    }
    return render(request, 'brand/brands.html', args)


def add_or_edit_brand(request):
    response = {}
    brand_form = AddBrandForm(request.POST)
    if brand_form.is_valid():
        response = get_some_data_from_brand(request)
        criteria = {'$or': [{'fa_name': response['fa_name']}, {'en_name': response['en_name']}]}
        find_brand = cursor.brand.find_one(criteria)
        if request.POST['brand_id']:
            response['brand_id'] = int(request.POST['brand_id'])
            response['image'] = "/media/brand/" + str(response['brand_id']) + '.jpg'
            edit_brand(request, response, find_brand)
        else:
            add_brand(request, response, find_brand)

    else:
        response['message'] = 'form_error'
        response['error'] = brand_form.errors

    if "_id" in response:
        del response["_id"]
    return HttpResponse(
        JsonResponse(response),
        content_type='application/json')


def add_brand(request, response, find_brand):
    if not find_brand:
        response['brand_id'] = get_next_sequence('brand_id')
        response['image'] = "/media/brand/" + str(response['brand_id']) + '.jpg'
        response['created_date'] = datetime.datetime.now()
        query_send = cursor.brand.insert_one(response)
        if not query_send:
            response['message'] = 'query_error'
        else:
            if 'image' in request.FILES:
                save_image_brand(request.FILES['image'], response['brand_id'])
            else:
                shutil.copyfile(
                    settings.BASE_DIR + settings.DEFAULT_IMAGE_BRAND,
                    settings.BASE_DIR + "/media/brand/" + str(response['brand_id']) + '.jpg'
                )

            response['message'] = 'insert'
    else:
        response['message'] = 'repeat_error'


def edit_brand(request, response, find_brand):
    brand_id = response['brand_id']
    if find_brand:
        if find_brand['brand_id'] == brand_id:
            response['image'] = find_brand['image']
            if 'image' in request.FILES:
                save_image_brand(request.FILES['image'], brand_id)

            update_query = cursor.brand.update_one(
                {"brand_id": brand_id},
                {
                    '$set': response,
                    '$unset': {'reason_report': 1, 'user_id': 1}
                }).raw_result
            if not update_query:
                response['message'] = 'query_error'
            else:
                response['message'] = 'update'

        else:
            response['message'] = 'repeat_error'
    else:
        if 'image' in request.FILES:
            save_image_brand(request.FILES['image'], brand_id)

        update_query = cursor.brand.update_one(
            {"brand_id": brand_id},
            {
                '$set': response,
                '$unset': {'reason_report': 1, 'user_id': 1}
            }).raw_result
        if not update_query:
            response['message'] = 'query_error'


def get_some_data_from_brand(request):
    response = {
        'fa_name': request.POST['fa_name'].strip(),
        'en_name': request.POST['en_name'].strip(),
        'category_group': loads(request.POST['category_group']),
        'status': True
    }
    return response


def remove_image_brand(address_image):
    image = settings.BASE_DIR + address_image
    os.remove(image)


def save_image_brand(image_file, image_name):
    image_address_django = '/media' + settings.BRAND_IMAGE + str(image_name) + ".jpg"
    image = settings.MEDIA_ROOT + settings.BRAND_IMAGE + str(image_name) + ".jpg"
    img = Image.open(image_file)
    img = img.resize((38, 30), Image.ANTIALIAS)
    img.save(image, quality=100)
    return image_address_django


@require_POST
def delete_brand(request):
    response = {}
    data = request.POST
    brand_id = int(data['brand_id'])

    query_delete = cursor.brand.find_one_and_delete(
        {"brand_id": brand_id},
        return_document=ReturnDocument.BEFORE
    )
    if not query_delete:
        response['message'] = 'query_error'
    else:
        response['message'] = "delete"
        remove_image_brand(query_delete['image'])

    return HttpResponse(
        JsonResponse(response),
        content_type='application/json')


@require_POST
def find_brand_by_id(request):
    data = request.POST
    projection = {'user_id': 0}
    find_brand = cursor.brand.find_one({"brand_id": int(data['brand_id'])}, projection)
    if not find_brand:
        find_brand['message'] = 'query_error'
    else:
        find_brand['id'] = str(find_brand.pop('_id'))

    return HttpResponse(
        JsonResponse(find_brand),
        content_type='application/json')
