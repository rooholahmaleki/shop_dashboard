from django.conf.urls import url
from brand.views import brands, add_or_edit_brand, delete_brand, find_brand_by_id

urlpatterns = [
    url(r'^$', brands, name='brands'),
    url(r'^add_brand/$', add_or_edit_brand, name='add_or_edit_brand'),
    url(r'^delete_brand/$', delete_brand, name='delete_brand'),
    url(r'^find_brand_by_id/$', find_brand_by_id, name='find_brand_by_id'),
]
