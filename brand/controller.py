from django.conf import settings
from core.db_mongo import cursor


def get_all_brand_by_category_group(category_group):
    criteria = {
        'status': True,
        'category_group': {'$elemMatch': {"id": category_group}},
    }
    return cursor.brand.find(criteria, settings.BRAND_GENERAL_PROJECTION)
